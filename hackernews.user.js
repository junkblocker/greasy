// ==UserScript==
// @id             hacker_news
// @name           Hacker News Site Improvements
// @version        114
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Hacker News Site Improvements
// @icon           data:image/x-icon;base64,AAABAAEAAAAAAAEAIABRHQAAFgAAAIlQTkcNChoKAAAADUlIRFIAAAEAAAABAAgGAAAAXHKoZgAACkFpQ0NQSUNDIFByb2ZpbGUAAEgNnZZ3VFPZFofPvTe90BIiICX0GnoJINI7SBUEUYlJgFAChoQmdkQFRhQRKVZkVMABR4ciY0UUC4OCYtcJ8hBQxsFRREXl3YxrCe+tNfPemv3HWd/Z57fX2Wfvfde6AFD8ggTCdFgBgDShWBTu68FcEhPLxPcCGBABDlgBwOFmZgRH+EQC1Py9PZmZqEjGs/buLoBku9ssv1Amc9b/f5EiN0MkBgAKRdU2PH4mF+UClFOzxRky/wTK9JUpMoYxMhahCaKsIuPEr2z2p+Yru8mYlybkoRpZzhm8NJ6Mu1DemiXho4wEoVyYJeBno3wHZb1USZoA5fco09P4nEwAMBSZX8znJqFsiTJFFBnuifICAAiUxDm8cg6L+TlongB4pmfkigSJSWKmEdeYaeXoyGb68bNT+WIxK5TDTeGIeEzP9LQMjjAXgK9vlkUBJVltmWiR7a0c7e1Z1uZo+b/Z3x5+U/09yHr7VfEm7M+eQYyeWd9s7KwvvRYA9iRamx2zvpVVALRtBkDl4axP7yAA8gUAtN6c8x6GbF6SxOIMJwuL7OxscwGfay4r6Df7n4Jvyr+GOfeZy+77VjumFz+BI0kVM2VF5aanpktEzMwMDpfPZP33EP/jwDlpzcnDLJyfwBfxhehVUeiUCYSJaLuFPIFYkC5kCoR/1eF/GDYnBxl+nWsUaHVfAH2FOVC4SQfIbz0AQyMDJG4/egJ961sQMQrIvrxorZGvc48yev7n+h8LXIpu4UxBIlPm9gyPZHIloiwZo9+EbMECEpAHdKAKNIEuMAIsYA0cgDNwA94gAISASBADlgMuSAJpQASyQT7YAApBMdgBdoNqcADUgXrQBE6CNnAGXARXwA1wCwyAR0AKhsFLMAHegWkIgvAQFaJBqpAWpA+ZQtYQG1oIeUNBUDgUA8VDiZAQkkD50CaoGCqDqqFDUD30I3Qaughdg/qgB9AgNAb9AX2EEZgC02EN2AC2gNmwOxwIR8LL4ER4FZwHF8Db4Uq4Fj4Ot8IX4RvwACyFX8KTCEDICAPRRlgIG/FEQpBYJAERIWuRIqQCqUWakA6kG7mNSJFx5AMGh6FhmBgWxhnjh1mM4WJWYdZiSjDVmGOYVkwX5jZmEDOB+YKlYtWxplgnrD92CTYRm40txFZgj2BbsJexA9hh7DscDsfAGeIccH64GFwybjWuBLcP14y7gOvDDeEm8Xi8Kt4U74IPwXPwYnwhvgp/HH8e348fxr8nkAlaBGuCDyGWICRsJFQQGgjnCP2EEcI0UYGoT3QihhB5xFxiKbGO2EG8SRwmTpMUSYYkF1IkKZm0gVRJaiJdJj0mvSGTyTpkR3IYWUBeT64knyBfJQ+SP1CUKCYUT0ocRULZTjlKuUB5QHlDpVINqG7UWKqYup1aT71EfUp9L0eTM5fzl+PJrZOrkWuV65d7JU+U15d3l18unydfIX9K/qb8uAJRwUDBU4GjsFahRuG0wj2FSUWaopViiGKaYolig+I1xVElvJKBkrcST6lA6bDSJaUhGkLTpXnSuLRNtDraZdowHUc3pPvTk+nF9B/ovfQJZSVlW+Uo5RzlGuWzylIGwjBg+DNSGaWMk4y7jI/zNOa5z+PP2zavaV7/vCmV+SpuKnyVIpVmlQGVj6pMVW/VFNWdqm2qT9QwaiZqYWrZavvVLquNz6fPd57PnV80/+T8h+qwuol6uPpq9cPqPeqTGpoavhoZGlUalzTGNRmabprJmuWa5zTHtGhaC7UEWuVa57VeMJWZ7sxUZiWzizmhra7tpy3RPqTdqz2tY6izWGejTrPOE12SLls3Qbdct1N3Qk9LL1gvX69R76E+UZ+tn6S/R79bf8rA0CDaYItBm8GooYqhv2GeYaPhYyOqkavRKqNaozvGOGO2cYrxPuNbJrCJnUmSSY3JTVPY1N5UYLrPtM8Ma+ZoJjSrNbvHorDcWVmsRtagOcM8yHyjeZv5Kws9i1iLnRbdFl8s7SxTLessH1kpWQVYbbTqsPrD2sSaa11jfceGauNjs86m3ea1rakt33a/7X07ml2w3Ra7TrvP9g72Ivsm+zEHPYd4h70O99h0dii7hH3VEevo4bjO8YzjByd7J7HTSaffnVnOKc4NzqMLDBfwF9QtGHLRceG4HHKRLmQujF94cKHUVduV41rr+sxN143ndsRtxN3YPdn9uPsrD0sPkUeLx5Snk+cazwteiJevV5FXr7eS92Lvau+nPjo+iT6NPhO+dr6rfS/4Yf0C/Xb63fPX8Of61/tPBDgErAnoCqQERgRWBz4LMgkSBXUEw8EBwbuCHy/SXyRc1BYCQvxDdoU8CTUMXRX6cxguLDSsJux5uFV4fnh3BC1iRURDxLtIj8jSyEeLjRZLFndGyUfFRdVHTUV7RZdFS5dYLFmz5EaMWowgpj0WHxsVeyR2cqn30t1Lh+Ps4grj7i4zXJaz7NpyteWpy8+ukF/BWXEqHhsfHd8Q/4kTwqnlTK70X7l35QTXk7uH+5LnxivnjfFd+GX8kQSXhLKE0USXxF2JY0muSRVJ4wJPQbXgdbJf8oHkqZSQlKMpM6nRqc1phLT4tNNCJWGKsCtdMz0nvS/DNKMwQ7rKadXuVROiQNGRTChzWWa7mI7+TPVIjCSbJYNZC7Nqst5nR2WfylHMEeb05JrkbssdyfPJ+341ZjV3dWe+dv6G/ME17msOrYXWrlzbuU53XcG64fW+649tIG1I2fDLRsuNZRvfbore1FGgUbC+YGiz7+bGQrlCUeG9Lc5bDmzFbBVs7d1ms61q25ciXtH1YsviiuJPJdyS699ZfVf53cz2hO29pfal+3fgdgh33N3puvNYmWJZXtnQruBdreXM8qLyt7tX7L5WYVtxYA9pj2SPtDKosr1Kr2pH1afqpOqBGo+a5r3qe7ftndrH29e/321/0wGNA8UHPh4UHLx/yPdQa61BbcVh3OGsw8/rouq6v2d/X39E7Ujxkc9HhUelx8KPddU71Nc3qDeUNsKNksax43HHb/3g9UN7E6vpUDOjufgEOCE58eLH+B/vngw82XmKfarpJ/2f9rbQWopaodbc1om2pDZpe0x73+mA050dzh0tP5v/fPSM9pmas8pnS8+RzhWcmzmfd37yQsaF8YuJF4c6V3Q+urTk0p2usK7ey4GXr17xuXKp2737/FWXq2euOV07fZ19ve2G/Y3WHruell/sfmnpte9tvelws/2W462OvgV95/pd+y/e9rp95Y7/nRsDiwb67i6+e/9e3D3pfd790QepD14/zHo4/Wj9Y+zjoicKTyqeqj+t/dX412apvfTsoNdgz7OIZ4+GuEMv/5X5r0/DBc+pzytGtEbqR61Hz4z5jN16sfTF8MuMl9Pjhb8p/rb3ldGrn353+71nYsnE8GvR65k/St6ovjn61vZt52To5NN3ae+mp4req74/9oH9oftj9MeR6exP+E+Vn40/d3wJ/PJ4Jm1m5t/3hPP7MjpZfgAAEstJREFUeAHtnQuwVVUZx7/LQ14CIhmCmmamiaaYbzM1fKBIpuU4OppNk/ma8jWZVkr4qEwtMx0bS9NxYqwsEBADfKCokA98IgoGoZCAvAQEecltfddrwObewzl7feucvc76rRnlrn32+q9v/b57/3udvffau6HxfLlV1suZ0iA9pVEoEIBAvRNocANslEXSRoY2NJ4nC12lh9vUpt7HzfggAIH/E1jvDvpL2rlqz/9v4gcIQCAVAnrA79mGaX8q+WacEMgQcF/5mfZnmFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAhgAFkgFCFQEoEMICUss1YIZAh0C5Tp1qKQIcuIp26l9qjss9WrxD5cGllbepx77btRbpuZzMy5alcKWURwADKwtS807afERn8mkibtpW0an3f+dNFhvQVWf9R6/uk8MmAH4l8/Xr/kSrHa/YSmTfNXysRBb4CVJLouW+ITLq3khal9+21u8iBZ5Tep94/7dhV5JjLbEb5zN388VdIEgOoEJiMukZk7apKW7W+/4lXiTQknIajLxHpsm3rfMr9RHMy2mAWUW5/dbJfwr95OTO4ZLbI+NtzNm6hWa893Czg9BY+SGBT09H/UpuBjr9NRHNDqYgABlARruadx9xge/JuYKKzAJ36d+6RJwObtlm1TGTMrzbdRq0sAhhAWZgyO61YJDL2xsxGj2rvPUUOOM1DIMKmejVFp/8WRf/4NSeUiglgABUja27w+K0iy+blbb15u9RmAU1H/20251DplmXzRTQXlFwEMIBc2FwjvdasJwStSh93+Wr/U63Uiq3TWY/+F9vE+NAQrvt7kMQAPOCJXnZaMMNHYdO2J16dxhWBY39oc0PVwpkiT7scUHITwAByo3MNP1or8uBPfRQ2bdtnb5H9vrHptnqr6Um//hfZjOpBd/JUc0DJTQADyI2uueHkB0Rmv+SrsqF9vc8CjnNH/47dNow3709zXhV54a95W9OumQAG4Pur0LheZNiVviob2u+4j0i/kzfU6+mnLu7o/9Uf2Ixo2BUiyp7iRQAD8MLX3HjqOJFp4y2UPtao11nAce6ef735x7e89ZTI62N8VWjvCGAAVr8GlrOAnfqJ7HuSVWTF0OnS0x39v28Ty3DDGZdNRNGqYABWqZv1nMiL/7BSExk02E6rCEoDLhfpsLV/JK+MEJkx0V8HhSYCGIDlL8IId0XAamnvTvvVzyxga3f0P8rg6K/f+fXMP8WMAAZghtIJ6Tr0iffYKZ5YJ7OAAe6EnT5MxbdMuk/k3Sm+KrTfiAAGsBEMkx/1zjSr5cI77y+yzyCTsGomok/6OfJC/+7XrRF5yPDOS/+I6kIBA7BO45L/2t6bHvsswOro/+QdIotmWWcreT0MIMSvwFi3Os3qWX+7HCiy98AQUYbX7NbLHf0v8O9n9Qci//yFvw4KmxHAADZDYrBhxRK3Pt09M8CqxHpFQI/+W3X2pzDuZpHlC/x1UNiMAAawGRKjDbpEdelcG7HPHiyy1/E2WtVS6ba9O/qf79/bBwtFHv2Nvw4KLRLAAFrEYrBxzYe2J61imwUc727Wad/JH+To60RWLffXQaFFAhhAi1iMNj59l8h7b9mI7XqoSN/jbLRCq3R3R/8jzvPvZfE7IhPu9NdBoVUCGECraAw+0JuCRhjeuDLoZwZBVUHihJ+4o39H/45GuOcjrFvtr4NCqwQwgFbRGH0w+e8i77xoI/a5w0T2PMZGK5TKNn1EDv+ev/rcqSLP/tlfB4WSBDCAkngMPmxaLuzOhluVos8CrI7+w3/Mcl+r35kSOhhACThmH73xqMibj9vI7Xa4yBf622hZq/TYwR39z/FXnfkvkVdG+uugsEUCGMAWERntYLmEtaizgBPcYqh2HfyBWbLyj6auFTCAaqV31vNuubA7H2BRPn+EyO5HWSjZafTYSeTL3/XXm/KwyPQn/XVQKIsABlAWJqOddCnr+nU2Yl8r2BWBgXr038pvbI2NIsOdDqVqBDCAqqF2Hc2fJvLMn2x61BmAzgSKULbdWeSw7/hH8vz9InNe9tdBoWwCGEDZqIx2fOhat1zY3SVoUYpyLmCgu+7ve/TXx3uPHGxBBY0KCGAAFcAy2fV9XS78OxOppqsBelWglqXnLjZH/6f+YPuSlVoyiahvDKAWydKXWa5836bnWs8C9Lt/2/Z+Y1mzUmT09X4atM5FAAPIhc2z0UrD5cJ6Z6DeIViL8qldRQ79tn/Pj95i+6JV/4iSUcAAapVq/Rrw/rs2vddqFmBx9F+xWGTcTTYcUKmYAAZQMTKjBnoi0OoZd7pKcNeDjQIrU6bp6H92mTuX2G3ML+2enlSiGz5qmQAG0DKX6mzVtwvPn27T16AhNjrlqujbi9q0K3fvlvfTE6Ljb2v5M7ZWhQAGUBXMrXSiy4Wt3i6sTwza5aBWOjLe/OndRA45y190pLuZaS3Lff1B5lfAAPKzs2n50jCRt1+w0arW3YEWR3+9KWrSvTbjRiU3AQwgNzqjhrpcWJe+WhR9erC+SyBk0aP/QWf696AzH6u3KPlHk6wCBlCE1Oty4TcesYkk9LkAveLQpq1frDrjsXyPol80SbfGAIqS/qYHYDT6R6NvEgo1C+i1h8iBZ/jHaDXj8Y8keQUMoCi/Am9PFpn8gE00od4mpE8m9j3660xHZzyUQhDAAAqRhuYgRrpLaxbLhfc9SUTfLmxZttej/+n+ihz9/RkaKmAAhjC9pfSegKfdvQEWZZAzE8ui5xYaPH9ddIajMx1KYQh4ZrQw46ifQEZfa7NceN+TRXbsZ8Old1+RA07z02p6RLqxKflFRGtHAAMo2q+Brg94zL1WzLc0NIhYzQL0u7/v0b/prkd37Z9SKAIYQKHS0RzM2BvccmG3YtC39DtFZIcv+qn0cUf//T2P/mtXuXUPbmZDKRwBDKBwKXEBrVzqXoftFsn4lqZZgDt6+5Sm7/5uNuFT9EWpet8/pXAEMIDCpaQ5oPG32/zR7PdNkT575xulzh6+dGq+tp+0+tCZ2Vj3ABRKIQlgAIVMiwtKlwuPGuIfnc4C9Jl9eUrTd3/Po7/+8a8w+DqTJ37abJEABrBFRDXcYeI9Hz9J2DcE/Q6v9/BXUnTWoLMHn7Jsnjuh+VsfBdoGJoABBAbsJW+1XFjv3jv+yspC0RV/OnvwKaOuEVnjZjKUwhLAAAqbmubAdNGMvlXItxz8LZFtdihPpfee7sy/53f/BTPcOxDuLq8/9qoZAQygZugr6NjiXXn63P5jLi2vU33Dr+91/xFXieiz/imFJoABFDo9zcHpm4WnjvOP9CvninTuXlpHzxX4rvib/bLIC38r3Q+fFoIABlCINJQRhMVy4Y5dRY64oHRneq7Ad8VfU6zrS/fDp4UggAEUIg1lBPHOi265sMFRtf9F7jVerbzIQ9/we8jZZQRTYhd9s+/rY0rswEdFIoABFCkbW4pFv1f7Lhfu3ts90uuslnsacLn/W36GX9GyNlsLSQADKGRaWgnqvX+75cJ3tfJhBZuPvWzznbtuJ3L4OZtvr2TLy8NFZj5bSQv2rTEBDKDGCai4e32ZiL5Lz6foTT76MpGNy9HuCkH7Thtvqexnfbjpg26GQomKAAYQVbpcsEuN7q7b+JKgnhw86kI/EpPuE5k71U+D1lUngAFUHblBh2NvdPfXL/YT6jtARB/zpeVI98ffaQuXBz/es+X/r1tts26hZXW2BiSAAQSEG0xaV9jpO/V8it7m2/9id0Wgg7tB6BIfJZEn7hBZ/LafBq1rQgADqAl2g071nXpL5vgJ6SW/o50JdNs+v86q5e7ZBT/P356WNSWAAdQUv0fn+k493+XCHbqInOI5k3jkZpEPFnkMhKa1JIAB1JK+b9+T7hWZ96afis89/8sXiDzya7/+aV1TAhhATfF7dm61XDhvGA9fL7J6Rd7WtCsAAQygAEnwCkHfLvyfGtx8s2iWyITfe4VO49oTwABqnwP/CIa5BTzVLiMHi6xjuW+1sVv3hwFYE62F3vQn3AKcsdXr+d0pIs8OrV5/9BSMAAYQDG2VhS2WC5cb8nD3wBC99ZcSPQEMIPoUNg9g9kvuIRx/CT+aGRNFXh0Vvh96qAoBDKAqmKvUyYirwz+Gi7f7VimZ1ekGA6gO5+r0og/ifOqP4fp6bbTIWxPC6aNcdQIYQNWRB+5w9HVhrs03LffN+YKRwENGPj8BDCA/u2K2DPUyjufuF5nzajHHTFS5CWAAudEVuOG4m9xyYcP78/Xx3nrdn1J3BDCAukupG5AuF7Z4u/AnbCbcKbJw5ic1/q0jAhhAHSVzk6E84d4uvGT2JptyVfRef73nn1KXBDCAukyrG5QuF5423n90j90ismy+vw4KhSSAARQyLQUKagrP+C9QNsxDwQDMkSIIgXgIYADx5IpIIWBOAAMwR4ogBOIhgAHEkysihYA5AQzAHCmCEIiHAAYQT66IFALmBDAAc6QIQiAeAhhAPLkiUgiYE8AAzJEiCIF4CGAA8eSKSCFgTgADMEeKIATiIYABxJMrIoWAOQEMwBwpghCIhwAGEE+uiBQC5gQwAHOkCEIgHgIYQDy5IlIImBPAAMyRIgiBeAhgAPHkikghYE4AAzBHiiAE4iGAAcSTKyKFgDkBDMAcKYIQiIcABhBProgUAuYEMABzpAhCIB4CGEA8uSJSCJgTwADMkSIIgXgIYADx5IpIIWBOAAMwR4ogBOIhgAHEkysihYA5AQzAHCmCEIiHAAYQT66IFALmBDAAc6QIQiAeAhhAPLkiUgiYE8AAzJEiCIF4CGAA8eSKSCFgTgADMEeKIATiIYABxJMrIoWAOQEMwBwpghCIhwAGEE+uiBQC5gQwAHOkCEIgHgIYQDy5IlIImBPAAMyRIgiBeAhgAPHkikghYE4AAzBHiiAE4iGAAcSTKyKFgDkBDMAcKYIQiIcABhBProgUAuYEMABzpAhCIB4CGEA8uSJSCJgTwADMkSIIgXgIYADx5IpIIWBOAAMwR4ogBOIhgAHEkysihYA5gYbGc6XRXBVBCEAgCgLMAKJIE0FCIAwBDCAMV1QhEAUBDCCKNBEkBMIQwADCcEUVAlEQwACiSBNBQiAMAQwgDFdUIRAFAQwgijQRJATCEMAAwnBFFQJREMAAokgTQUIgDAEMIAxXVCEQBQEMIIo0ESQEwhDAAMJwRRUCURDAAKJIE0FCIAwBDCAMV1QhEAUBDCCKNBEkBMIQwADCcEUVAlEQwACiSBNBQiAMAQwgDFdUIRAFAQwgijQRJATCEMAAwnBFFQJREMAAokgTQUIgDAEMIAxXVCEQBQEMIIo0ESQEwhDAAMJwRRUCURDAAKJIE0FCIAwBDCAMV1QhEAUBDCCKNBEkBMIQwADCcEUVAlEQwACiSBNBQiAMAQwgDFdUIRAFAQwgijQRJATCEMAAwnBFFQJREMAAokgTQUIgDAEMIAxXVCEQBQEMIIo0ESQEwhDAAMJwRRUCURDAAKJIE0FCIAwBDCAMV1QhEAUBDCCKNBEkBMIQwADCcEUVAlEQwACiSBNBQiAMAQwgDFdUIRAFAQwgijQRJATCEMAAwnBFFQJREMAAokgTQUIgDAEMIAxXVCEQBQEMIIo0ESQEwhDAAMJwRRUCURDAAKJIE0FCIAwBDCAMV1QhEAUBDCCKNBEkBMIQwADCcEUVAlEQwACiSBNBQiAMgTbSEEYYVQhAoOAE3N++zgAWuf/WFzxUwoMABGwJ6N/8Ip0BDHU/LGEmYEsXNQgUlsDHs379mx/6P+CbiSzUnOR+AAAAAElFTkSuQmCC
// @include        http://hackerne.ws
// @include        http://hackerne.ws/
// @include        http://hackerne.ws/*
// @include        *://news.ycombinator.tld/*
// @license        MIT
// @require        https://code.jquery.com/jquery-3.6.0.min.js
// @require        https://github.com/eligrey/FileSaver.js/raw/master/dist/FileSaver.min.js
// @grant          GM_addStyle
// @grant          GM_deleteValue
// @grant          GM_getValue
// @grant          GM_listValues
// @grant          GM_log
// @grant          GM_openInTab
// @grant          GM_registerMenuCommand
// @grant          GM_setValue
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es2019, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */

console.log('hackernews.user.js starting');
this.$ = jQuery = jQuery.noConflict(true);
(function() {
    'use strict';

    // jQuery does not provide similar without a plugin
    // Usage: var results = $xp('/some/xpath');
    // or
    // Usage: $xp('/some/xpath').forEach() {}
    var $xp = typeof $xp !== 'undefined' ? $xp : function(exp, contextnode) {
        if (!contextnode || contextnode === '') contextnode = document;
        var i, l, arr = [],
            r = document.evaluate(exp, contextnode, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (i = 0, l = r.snapshotLength; i < l; i++) arr.push(r.snapshotItem(i));
        return arr;
    };

    var infiniteScrollDisabled = true;

    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    var delay = 0;

    function vacuumThrottle() {
        console.log("********** vacuuming **********");
        // This is as good a place as any to vacuum any stale 'delay' throttle values
        var opportunisticVacuum = GM_listValues();
        for (var x = 0; x < opportunisticVacuum.length; x++) {
            if (/^d-[0-9]+$/.exec(opportunisticVacuum[x])) {
                GM_deleteValue(opportunisticVacuum[x]);
                console.log("Deleted ", opportunisticVacuum[x]);
            }
        }
        console.log(GM_listValues());
        console.log("********** vacuumed **********");
    }

    function throttled() {
        var reg = /\?id=([0-9]+)/;
        var result = reg.exec(document.location.href);
        if (!result) return false;
        var pageid = result[1] + 0;
        var delayKey = 'd-' + pageid;
        try {
            if (document.body.firstElementChild.tagName.toLowerCase() != 'pre' && !/Sorry, we're not able to serve your requests this quickly/.exec(document.body.innerHTML)) {
                GM_deleteValue(delayKey);
                return false;
            }
        } catch (e) {
            console.log(e);
            return false;
        }

        console.log("throttled");
        delay = getValue(delayKey, pageid % 30000) + getRandomInt(30000);
        setValue(delayKey, delay);
        console.log("Retry page load in" + delay / (1000.0) + " seconds");
        setTimeout(function() {
            window.location.replace(window.location.href);
        }, delay, delay);
        return true;
    }

    function iCaseSort(a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    }
    var isGM = (typeof GM_getValue != 'undefined' && typeof GM_getValue('a', 'b') != 'undefined');

    // XXX: This may not be compatible with cooked getValue because of
    // 'undefined'/'null'
    var getRawValue = isGM ? GM_getValue : function(name, def) {
        var s;
        try {
            s = localStorage.getItem(name);
        } catch (e) {
            //
        }
        return (s == "undefined" || s == "null") ? def : s;
    };
    var setRawValue = function(name, value) {
        try {
            if (isGM) {
                GM_setValue(name, value);
            } else {
                localStorage.setItem(name, value);
            }
        } catch (e) {
            console.log("setRawValue did nothing because", e);
        }
    };

    var getValue = typeof getValue !== 'undefined' ? getValue : function(name, defaultValue) {
        var value = getRawValue(name);
        if (!value || typeof value != 'string' || value.length < 1) {
            try {
                setValue(name, defaultValue);
            } catch (e) {
                console.log(e);
            }
            return defaultValue;
        }

        var typ = value[0];
        var tvalue = value.substring(1);
        switch (typ) {
            case 'b':
                value = (tvalue == 'true');
                break;
            case 'n':
                value = Number(tvalue);
                break;
            case 'o': // object
                try {
                    value = JSON.parse(tvalue);
                } catch (e) {
                    console.log('ERROR: getValue(', name, ', ', defaultValue, ') could not parse stored value', tvalue, e);
                    console.log('Returning default value');
                    value = defaultValue;
                }
                break;
            case 's':
                value = tvalue;
                break;
            case 'f': // function
                try {
                    value = eval('(' + tvalue + ')');
                } catch (e) {
                    console.log('ERROR: getValue(', name, ', ', defaultValue, ') could not parse stored value', tvalue, e);
                    console.log('Returning default function');
                    value = defaultValue;
                }
                break;
            default:
                value = defaultValue;
                break;
        }
        return value;
    };

    var setValue = typeof setValue !== 'undefined' ? setValue : function(name, value) {
        var typ = (typeof value)[0];
        if (typ == 'o') {
            try {
                value = typ + JSON.stringify(value);
            } catch (e) {
                console.log(e);
                return;
            }
        } else if (typ == 'f') {
            try {
                value = typ + value.toString();
            } catch (e) {
                console.log(e);
                return;
            }
        } else if (/^[bsn]$/.exec(typ)) {
            value = typ + value;
        } else {
            throw "Invalid type passed to setValue(" + name + ", ...)";
        }
        setRawValue(name, value);
    };

    function getSystemTheme() {
        // Check if the --color-mode variable exists
        if (window.getComputedStyle(document.documentElement).getPropertyValue('--color-mode')) {
            return window.getComputedStyle(document.documentElement).getPropertyValue('--color-mode');
        } else {
            // Fallback method if --color-mode is not supported
            var body = document.body;
            if (body.classList.contains('dark-theme') || body.classList.contains('night-mode')) {
                return "dark";
            } else {
                return "light";
            }
        }
    }

    var darkMode = getValue('darkMode', getSystemTheme() == 'dark' ? true : false);
    var HIGHLIGHT_COLOR, CURSOR_ROW_COLOR, HN_DARK_ORANGE, HN_LIGHT_ORANGE;

    if (darkMode) {
        // HIGHLIGHT_COLOR = 'rgb(32, 40, 32)';
        // HIGHLIGHT_COLOR = 'rgb(48, 0, 0)';
        // HIGHLIGHT_COLOR = 'rgb(96, 32, 32)';
        // HIGHLIGHT_COLOR = 'rgb(16, 32, 48)';
        HIGHLIGHT_COLOR = 'rgb(48, 48, 48)';
        CURSOR_ROW_COLOR = 'rgb(66, 66, 66)';
        CURSOR_ROW_COLOR = 'rgb(80, 80, 80)';
        HN_DARK_ORANGE = '#ff9900';
        HN_LIGHT_ORANGE = '#ff5f00';
    } else {
        HIGHLIGHT_COLOR = 'rgb(221, 255, 221)';
        CURSOR_ROW_COLOR = 'rgb(255, 255, 192)';
        HN_DARK_ORANGE = '#ff5f00';
        HN_LIGHT_ORANGE = '#ff9900';
    }
    const ANIMATE = true;

    (function($) {
        $.fn.row_cur = function() {
            // Do your awesome plugin stuff here
            return this.each(function() {
                if ($(this).css('background-color') != CURSOR_ROW_COLOR) {
                    $(this).css('background-color', CURSOR_ROW_COLOR);
                }
            });
        };
        $.fn.row_uncur = function() {
            // Do your awesome plugin stuff here
            return this.each(function() {
                if ($(this).css('background-color') != HIGHLIGHT_COLOR) {
                    $(this).css('background-color', 'inherit');
                }
            });
        };
        $.fn.visible = function() {
            return this.css('visibility', 'visible');
        };
        $.fn.invisible = function() {
            return this.css('visibility', 'hidden');
        };
        $.fn.visibilityToggle = function() {
            return this.css('visibility', function(_, visibility) {
                return (visibility == 'visible') ? 'hidden' : 'visible';
            });
        };
    })($);

    var drop = typeof drop !== 'undefined' ? drop : function(e) {
        e.preventDefault();
        e.stopPropagation();
    };

    const entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        '\'': '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };

    function escapeHTML(string) {
        return String(string).replace(/[&<>"'/]/g, function(s) {
            return entityMap[s];
        });
    }

    const CACHE_PERIOD = 20 * 24 * 60 * 60 * 1000;
    const DEFAULT_CONFIG = {
        "nobles": [
            "_sdegutis",
            "0cachecoherency",
            "abd12",
            "aboodman",
            "alankay",
            "alankay1",
            "Animats",
            "antirez",
            "apenwarr",
            "Araq",
            "asadotzler",
            "audiosampling",
            "bagder",
            "bcantrill",
            "beej71",
            "beliu",
            "bigmac",
            "bradfitz",
            "brendangregg",
            "brl",
            "BruceM",
            "burntsushi",
            "callahad",
            "chuckharmston",
            "cmsj",
            "cortesi",
            "cperciva",
            "cpeterso",
            "cstrahan",
            "dang",
            "darnir",
            "darren0",
            "davidjgraph",
            "davidu",
            "dblohm7",
            "ddfisher",
            "decklebench",
            "def-",
            "dom96",
            "eastdakota",
            "efritz",
            "emlun",
            "Eva_Schweber",
            "f-",
            "fiddlosopher",
            "FiloSottile",
            "frioux",
            "ggreer",
            "gildas",
            "gnachman",
            "gorhill",
            "gowan",
            "grhmc",
            "groovecoder",
            "harlanhugh",
            "hisham_hm",
            "hk__2",
            "homakov",
            "hwayne",
            "i386",
            "inconshreveable",
            "jaas",
            "jbk",
            "jcsiracusa",
            "JeffreySnover",
            "jefftk",
            "jgrahamc",
            "jgruen",
            "jleader",
            "jlivingood",
            "josefbacik",
            "josh64",
            "jpgoldberg",
            "jqiu25",
            "junegunn",
            "justinmk",
            "jxpx777",
            "kalid",
            "khuey",
            "koala_man",
            "Kungsgeten",
            "leafo",
            "luckydude",
            "MagerValp",
            "malgorithms",
            "marksc",
            "martinvonz",
            "mawww",
            "mholt",
            "mikemcquaid",
            "mitchellh",
            "mjd",
            "mratsim",
            "mudler",
            "mvdan",
            "n3otec",
            "natfriedman",
            "neild",
            "nickcw",
            "niemeyer",
            "norberg",
            "norvig",
            "parrt",
            "pde3",
            "pg",
            "pgl",
            "PieterH",
            "piro_or",
            "pmeunier",
            "regnat",
            "rfjakob",
            "richhickey",
            "ridiculous_fish",
            "rlpb",
            "roustem",
            "rsc",
            "rsync",
            "sama",
            "sctb",
            "sdegutis",
            "Sesse__",
            "snowmaker",
            "SnowyOwl",
            "spooneybarger",
            "sporksmith",
            "SQLite",
            "sreitshamer",
            "stefantalpalaru",
            "stevedekorte",
            "steveklabnik",
            "swartzcr",
            "tabbott",
            "tectonic",
            "Thibaut",
            "thurston",
            "totallygamerjet",
            "tptacek",
            "treeform",
            "Tyriar",
            "unix-junkie",
            "ux",
            "wezfurlong",
            "williamstein",
            "xdma"
        ],
        "people": {
            "0cachecoherency": "ponylang",
            "0xggus": "Tor",
            "Aardappel": "TreeSheets",
            "Animats": "John Nagle (TCP)",
            "Araq": "Nim author",
            "Arathorn": "matrix",
            "BruceM": "Open Dylan",
            "CodeIsTheEnd": "jless",
            "DannyBee": "DannyBee",
            "Eva_Schweber": "1Password",
            "FiloSottile": "go/crypto",
            "JeffreySnover": "PowerShell",
            "JoshTriplett": "rust",
            "Kungsgeten": "org-brain",
            "MagerValp": "AutoDMG",
            "MaxGabriel": "Mercury Vault",
            "MrAlex94": "waterfox",
            "PieterH": "Pieter Hintjens",
            "Rinzler89": "Rinzler89",
            "SQLite": "SQLite/Fossil",
            "Sesse__": "plocate",
            "SnowyOwl": "ponylang",
            "Solder_Man": "Pockit",
            "StavrosK": "deadmansswitch",
            "Thibaut": "DevDocs",
            "Tyriar": "vscode",
            "YorickPeterse": "gitlab",
            "_sdegutis": "Mjolnir",
            "abd12": "dynamodb book",
            "aboodman": "greasemonkey",
            "agbell": "corecursive podcast",
            "akavel": "up - ultimate plumber",
            "alankay": "Alan Kay",
            "alankay1": "Alan Kay",
            "amedvednikov": "v",
            "andyk": "Andy Konwinski",
            "anigbrowl": "anigbrowl",
            "anslo": "slowroads.io",
            "antirez": "antirez/redis",
            "apaprocki": "apaprocki",
            "apenwarr": "tailscale",
            "asadotzler": "mozilla",
            "aseipp": "jujutsu",
            "audiosampling": "mynoise.net",
            "avar": "git",
            "avsm": "docker",
            "awesomekling": "ladybird",
            "bagder": "curl",
            "bcantrill": "dtrace",
            "beej71": "beej",
            "beliu": "Sourcegraph",
            "bfred_it": "GhostText",
            "bigmac": "docker security",
            "blantonl": "Police Scanners",
            "boudewijnrempt": "krita",
            "boulos": "Google Cloud",
            "bradfitz": "Brad Fitzpatrick/golang/camlistore/perkeep/lj",
            "brendangregg": "linux perf",
            "brl": "subgraphos",
            "brongondwana": "fastmail",
            "bryanmccann": "you.com",
            "bryphe": "onivim",
            "bthdonohue": "Instapaper",
            "burntsushi": "ripgrep",
            "bwoodruff": "1Password",
            "callahad": "mozilla",
            "chuckharmston": "mozilla",
            "cjbprime": "gittorrent/zoom",
            "cmsj": "Hammerspoon",
            "coderaiser": "Cloud Commander",
            "comex": "comex",
            "cortesi": "mitmproxy/modd/devd",
            "cperciva": "Colin Percival, crypto, Former FreeBSD security officer, tarsnap",
            "cpeterso": "mozilla",
            "crawshaw": "tailscale",
            "cstrahan": "nix",
            "cstross": "Charlie Stross",
            "cuddy": "sourcegraph",
            "dang": "hn - Daniel Gackle",
            "darnir": "wget",
            "darren0": "RancherOS",
            "david-giesberg": "Atlassian",
            "daviddahl": "encryptr",
            "davidjgraph": "draw.io",
            "davidkunz": "DevOnDuty Youtube",
            "davidu": "OpenDNS",
            "dblohm7": "mozilla",
            "dctoedt": "dctoedt",
            "ddebernardy": "ScrapingHub",
            "ddevault": "Drew DeVault",
            "ddfisher": "mypy",
            "decklebench": "ponylang",
            "def-": "nim advocate",
            "degio": "sysdig",
            "dhouston": "dropbox",
            "djk44": "cloudflare",
            "dnsmichi": "gitlab",
            "dom96": "nim",
            "dsumenkovic": "gitlab",
            "dteare": "1Password",
            "dystroy": "broot",
            "eastdakota": "CloudFlare CEO",
            "efritz": "sourcegraph",
            "emlun": "yubikey",
            "f-": "afl-fuzz",
            "feross": "WebTorrent",
            "fiddlosopher": "pandoc",
            "fogus": "clojure",
            "freediver": "kagi",
            "geerlingguy": "ansible things",
            "geofft": "geofft",
            "ggerganov": "gguf AI models",
            "ggreer": "the silver searcher",
            "gildas": "SingleFile",
            "gnachman": "iTerm2",
            "gorhill": "µBlock",
            "grellas": "grellas",
            "greysteil": "github",
            "grhmc": "NixOS",
            "groovecoder": "firefox",
            "grun": "icrecream python",
            "gumby": "LGPL, Cygnus",
            "haberman": "JITs",
            "hadley": "ggplot2/RStudio",
            "hannob": "irssi",
            "harlanhugh": "TheBrain",
            "harryh": "harryh",
            "herrington_d": "ast-grep",
            "hexomancer": "sioyek",
            "hidro": "materialistic",
            "hisham_hm": "htop",
            "hk__2": "homebrew",
            "hnlmorg": "murex",
            "homakov": "security",
            "hongbo_zhang": "Moonbit/BuckleScript",
            "hoten": "chrome",
            "hwayne": "TLA+",
            "i386": "jenkins",
            "idlewords": "Pinboard",
            "ignoramous": "RethinkDNS",
            "inconshreveable": "ngrok",
            "j4yav": "gitlab",
            "jaas": "letsencrypt",
            "james_cowling": "dropbox",
            "jamwt": "dropbox",
            "jbk": "VLC president",
            "jclulow": "illumos",
            "jcranmer": "jcranmer",
            "jcsiracusa": "Ars Technica (Old)",
            "jedberg": "reddit/netflix",
            "jefftk": "google ads",
            "jgrahamc": "Cloudflare",
            "jgruen": "mozilla",
            "jkbr": "HTTPie",
            "jkozera": "zeal",
            "jl-gitlab": "gitlab",
            "jlivingood": "comcast",
            "josefbacik": "btrfs?",
            "josh64": "Vienna News Reader",
            "jpgoldberg": "1Password",
            "jqiu25": "golang",
            "jquast": "pexpect",
            "junegunn": "vim-plug,fzf",
            "justinmk": "neovim",
            "jvns": "Julia Evans",
            "jxpx777": "1password",
            "kalid": "BetterExplained",
            "kasey_junk": "kasey_junk",
            "kazinator": "txr, pipe watch",
            "kdeldycke": "kdeldycke",
            "kepano": "obsidian",
            "khuey": "mozilla",
            "kickscondor": "fraidyc.at",
            "koala_man": "shellcheck",
            "koverstreet": "bcachefs",
            "kschw": "kschw",
            "larrik": "idiot",
            "latitude": "Hamachi",
            "leafo": "moonscript",
            "lewisjoe": "zoho word processor",
            "loganabbott": "sourceforge",
            "luckydude": "Larry McVoy BitKeeper",
            "luu": "Dan Luu",
            "lvh": "lvh",
            "malgorithms": "keybase",
            "marksc": "mozilla",
            "martanne": "vis editor",
            "martinvonz": "jujutsu dvcs git",
            "mattbierner": "vscode",
            "mattdesl": "wayfinder game",
            "mattgreenrocks": "watchexec",
            "mattip": "PyPy",
            "mawww": "kakoune editor",
            "mehalter": "AstroNvim",
            "metadaddy": "backblaze",
            "mhils": "another mitmproxy dev",
            "mholt": "caddy",
            "michaelmure": "git-bug",
            "mikemcquaid": "homebrew",
            "mitchellh": "Hashicorp",
            "mjd": "mjd",
            "moby": "github",
            "modeless": "James Darpinian",
            "mratsim": "nim",
            "mrborgen": "scr/imba",
            "mrmrs": "vimgifs",
            "mudler": "LocalAI",
            "munificent": "Google/Game/Dart/Interpreters",
            "mvdan": "mvdan",
            "n3otec": "ponylang",
            "nastevens": "streamexpect",
            "natfriedman": "CEO Github",
            "nathanwallace": "steampipe",
            "neild": "golang",
            "newhouseb": "Dropbox",
            "nickcw": "rclone (Nick Craig-Wood)",
            "niemeyer": "ubuntu",
            "norberg": "google",
            "norvig": "Peter Norvig",
            "nostrademons": "nostrademons",
            "nrp": "framework laptop",
            "obi1kenobi": "trustfall",
            "orf": "ptail",
            "orlp": "pdqsort",
            "pakled_engineer": "pakled_engineer",
            "parrt": "ANTLR",
            "patio11": "stripe",
            "pbsd": "pbsd",
            "pc": "stripe",
            "pde3": "EFF",
            "petdance": "ack",
            "pg": "Paul Graham",
            "pgl": "Peter Lowe's Blacklist",
            "phickey": "wasm",
            "phoerious": "KeePassXC",
            "piro_or": "TreeStyleTab",
            "pixelbeat__": "coreutils",
            "pmeunier": "pijul",
            "potatolicious": "potatolicious",
            "pushcx": "lobste.rs",
            "pvg": "pvg",
            "randomstring": "blekko",
            "raphlinus": "Xi Editor, Google",
            "rayiner": "rayiner",
            "regnat": "nixos",
            "rfjakob": "gocryptfs/encfs",
            "rhysd": "rhysd",
            "richhickey": "Clojure",
            "ridiculous_fish": "fish shell",
            "rlpb": "ddar",
            "roustem": "1Password",
            "rsc": "rsc/go",
            "rsync": "rsync.net",
            "saghul": "jitsi",
            "sama": "Sam Altman (YC)/hn",
            "sandyarmstrong": "M$",
            "scoopertrooper": "scoopertrooper",
            "scopatz42": "xonsh shell",
            "sctb": "hn",
            "sdegutis": "Mjolnir",
            "seanjregan": "jira/atlassian",
            "seanlevan": "boy genius",
            "seanmcb": "vscode",
            "sethvargo": "hashicorp",
            "shawndumas": "Google",
            "shykes": "docker",
            "simonw": "Simon Willison",
            "slg": "sane person",
            "slimsag": "sourcegraph",
            "snowmaker": "Jared (YC)",
            "soapdog": "mozilla",
            "somebee": "scr/imba",
            "spieglt": "FlyingCarpet",
            "spooneybarger": "ponylang",
            "sporksmith": "Tor",
            "sqs": "sourcegraph",
            "sreitshamer": "Arq",
            "stefantalpalaru": "nim goroutines",
            "stevedekorte": "Io",
            "steveklabnik": "rust",
            "stummjr": "ScrapingHub",
            "sujayakar": "dropbox",
            "swartzcr": "EFF",
            "syropian": "astralapp",
            "sytse": "gitlab",
            "tabbott": "mypy",
            "tarruda": "n/vim",
            "tectonic": "Huginn",
            "thurston": "Ragel",
            "tjdevries": "telescope.vim",
            "tlb": "Trevor (YC)",
            "totallygamerjet": "purego",
            "tptacek": "security",
            "treeform": "nim, pushbullet, hipmunk",
            "tribler": "tribler",
            "triska": "Prolog Guru (Markus)",
            "trotterdylan": "\"Grumpy\" go/python",
            "trueadm": "dominic/FB",
            "tstack": "lnav",
            "twp": "chezmoi",
            "tzs": "tzs",
            "unix-junkie": "nmap",
            "ux": "ffmpeg",
            "varjolintu": "KeePassXC",
            "vladstudio": "vladstudio",
            "wezfurlong": "wezterm",
            "williamstein": "cython",
            "woodruffw": "graphtage",
            "wscott": "BitKeeper",
            "xdma": "SubgraphOS",
            "xet7": "Wekan",
            "xiaq": "elvish",
            "yegg": "DuckDuckGo",
            "zik": "PicoC",
            "zyedidia": "micro editor",
            "zzzeek": "SQLAlchemy"
        },
        "sights": [
            "acolyer.org",
            "arstechnica.com",
            "axios.com",
            "developer.apple.com",
            "dropbox.tech",
            "edge.org",
            "golang.org",
            "gwern.net",
            "lwn.net",
            "motherjones.com",
            "nationalgeographic.com",
            "obdev.at",
            "propublica.org",
            "put.as",
            "reason.com",
            "samaltman.com",
            "sentinelone.com",
            "slatestarcodex.com",
            "tbray.org",
            "tesla.com",
            "time.com",
            "torrentfreak.com",
            "stevehanov.ca",
            "danluu.com",
            "dimitarsimeonov.com",
            "astralcodexten.substack.com",
            "scottaaronson.blog",
            "github.com/go-skynet"
        ]
    };
    const DEFAULT_PEOPLE = DEFAULT_CONFIG.people;
    const DEFAULT_SITES = DEFAULT_CONFIG.sights;
    const DEFAULT_NOBLES = DEFAULT_CONFIG.nobles;
    const MONO_FONT_FAMILY = '"JetBrains Mono NL", "JetBrains Mono", Menlo, Monaco, "Bitstream Vera Sans Mono", "Lucida Console", Consolas, Terminal, "Courier New", Courier, monospace';
    const SANS_SERIF_FONT_FAMILY = 'Lato, "Helvetica Neue", Helvetica, Verdana, Arial, FreeSans, "Luxi-sans", "Nimbus Sans L", sans-serif';
    // const SERIF_FONT_FAMILY = 'Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif';

    var FONT_FAMILY = MONO_FONT_FAMILY;
    var DIALOG_FONT_FAMILY = SANS_SERIF_FONT_FAMILY;

    // return: Whether elem is in array a
    var inArray = typeof inArray !== 'undefined' ? inArray : function(a, elem) {
        for (var i = 0, l = a.length; i < l; i++) {
            if (a[i] === elem) return true;
        }
        return false;
    };

    // requires: inArray
    var addUnique = typeof addUnique !== 'undefined' ? addUnique : function(a, elem) {
        if (!inArray(a, elem)) a.push(elem);
        return a;
    };

    function getViewportHeight() {
        var height = window.innerHeight; // Safari, Opera
        var mode = document.compatMode;

        if ((mode || !$.support.boxModel)) { // IE, Gecko
            height = (mode == 'CSS1Compat') ?
                document.documentElement.clientHeight : // Standards
                document.body.clientHeight; // Quirks
        }

        return height;
    }

    //
    //
    //      +------------+ <-- document top ----------------------------------
    //      |            |                            ^                   ^
    //      |            |                            |                   |
    //      |            |                    viewport.scrolled           |
    //      |            |                            |                   |
    //      |            |                            v                   |
    //  +--------------------+ <-- viewport.top == 0 -------------------- |
    //  |   |            |   |                                     ^   ^  |
    //  |   |            |   |                                     |   |  |
    //  |   |  +----+    |   | <---- bounds.topRelativeToViewport--+   |  +- $(el_.offset().top
    //  |   |  |    |    |   |                                         |
    //  |   |  |    |    |   |                                         |
    //  |   |  |    |    |   |                                         |
    //  |   |  |    |    |   |                                         |
    //  |   |  +----+    |   | <----- bounds.bottomRelativeToViewport -+
    //  |   |            |   |
    //  |   |            |   |
    //  +--------------------+ <--------- viewport.bottom
    //      |            |
    //      |            |
    //      |            |
    //      |            |
    //      +------------+ ---- document bottom
    //
    //
    //

    //
    // MODIFIED
    //
    // returns true if ALL OF AN ELEMENT is in the viewport vertically
    // requires getViewportHeight and $
    function elementCompletelyInViewport(el, padding) {
        padding = padding || 10;
        padding = padding >= 0 ? padding : 0 - padding;
        var viewport = {};
        viewport.top = 0;
        viewport.scrolled = (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
        viewport.bottom = getViewportHeight();
        var bounds = {};
        bounds.topRelativeToViewport = $(el).offset().top - viewport.scrolled;
        bounds.bottomRelativeToViewport = bounds.topRelativeToViewport + $(el).outerHeight();
        return (
            (bounds.topRelativeToViewport >= (viewport.top + padding) && bounds.bottomRelativeToViewport <= (viewport.bottom - padding))
        );
    }

    // *************************
    // scrollToElement(element) - MODIFIED with 10 padding
    // *************************
    function scrollToElement(theElement, padding) {
        padding = padding || 10;
        padding = 0 - padding;
        var selectedPosX = 0;
        var selectedPosY = padding;

        while (theElement !== null) {
            selectedPosX += theElement.offsetLeft;
            selectedPosY += theElement.offsetTop;
            theElement = theElement.offsetParent;
        }

        window.scrollTo(selectedPosX, selectedPosY);
    }

    function centerElementByScrolling(element) {
        $('html,body').animate({
            scrollTop: $(element).offset().top - Math.max(0, (getViewportHeight() - $(element).outerHeight()) / 2)
        }, 250);
    }

    function openInTab(url) {
        setTimeout(function() {
            if (!url) return;
            if (window && typeof window.open === 'function') {
                window.open(url);
            } else if (unsafeWindow && typeof unsafeWindow.open === 'function') {
                unsafeWindow.open(url);
            } else if (typeof GM_openInTab === 'function') {
                GM_openInTab(url);
            } else {
                window.location.href = url;
            }
        }, 500);
    }

    var config = getValue('config', {
        'people': DEFAULT_PEOPLE,
        'nobles': DEFAULT_NOBLES,
        'sights': DEFAULT_SITES,
    });

    var removeEventHandler = typeof removeEventHandler != 'undefined' ? removeEventHandler : function(target, eventName, eventHandler) {
        if (target.addEventListener) {
            target.removeEventListener(eventName, eventHandler, true);
        } else if (target.attachEvent) {
            target.detachEvent('on' + eventName, eventHandler);
        }
    };

    var addEventHandler = typeof addEventHandler != 'undefined' ? addEventListener : function(target, eventName, eventHandler, scope) {
        var f = scope ? function() {
            eventHandler.apply(scope, arguments);
        } : eventHandler;
        if (target.addEventListener) {
            target.addEventListener(eventName, f, true);
        } else if (target.attachEvent) {
            target.attachEvent('on' + eventName, f);
        }
        return f;
    };

    // Creates a new node with the given attributes and properties (be careful with XPCNativeWrapper limitations)
    function createNode(type, attributes, props) {
        var node = document.createElement(type);
        if (attributes) {
            for (var attr in attributes) {
                if (!attributes.hasOwnProperty(attr)) continue;
                node.setAttribute(attr, attributes[attr]);
            }
        }
        if (props) {
            for (var prop in props) {
                if (!props.hasOwnProperty(prop)) continue;
                if (prop in node) node[prop] = props[prop];
            }
        }
        return node;
    }

    function curriedConfigureScript() {
        return function() {
            configureScript(postDialogHide);
        };
    }

    function configureScript(postHideFunc) {
        if (top !== self || document.body.tagName.toLowerCase() === 'frameset') return;

        // Gets the layers
        var maskLayer = document.getElementById("hnmaskLayer");
        var dialogLayer = document.getElementById("hndialogLayer");
        // Checks the layers state
        // Creates the layers if they don't exist or displays them if they are hidden
        if ((maskLayer) && (dialogLayer)) {
            if ((maskLayer.style.display === "none") && (dialogLayer.style.display === "none")) {
                maskLayer.style.display = "";
                dialogLayer.style.display = "";
            }
            dialogLayer.focus();
        } else {
            createLayers();
        }

        function createLayers() {
            // Creates a layer to mask the page during configuration
            maskLayer = createNode("div", {
                id: "hnmaskLayer",
                title: "Click here to return to the page"
            });

            // Creates a layer for the configuration dialog
            dialogLayer = createNode("div", {
                id: "hndialogLayer"
            });

            dialogLayer.innerHTML = "<div id='hnconfTitle'>Hacker News</div>" +
                "    <ul>" +
                "        <li>Purge article/comments cache</li>" +
                "        <input type='button' id='hnPurgeArticleCacheButton' value='Purge' title='Purge article/comments cache'/>" +
                "        <li>Reset followed users list to default</li>" +
                "        <input type='button' id='hnDefaultUserFollowButton' value='Reset' title='Reset followed users list to default'/>" +
                "        <li>Reset followed sites list to default</li>" +
                "        <input type='button' id='hnDefaultSiteFollowButton' value='Reset' title='Reset followed sites list to default'/>" +
                "        <li>Save configuration to disk</li>" +
                "        <input type='button' id='hnSaveConfigurationButton' value='Save' title='Save configuration to disk'/>" +
                "        <li>Load saved configuration form disk</li>" +
                "        <input type='file' id='hnLoadConfigurationFile'>" +
                "        <li>Reset everything (USE WITH CAUTION)</li>" +
                "        <input type='button' id='hnResetEverythingButton' value='Reset Everything' title='Reset everything'/>" +
                "    </ul>" +
                "</div>" +
                "<div>" + genFollowDisplay() + "</div>";

            $(dialogLayer).find('a.user_follow').on('click', UserUnFollowFunction);

            // Appends the layers to the document
            $(document.body).append(maskLayer).append(dialogLayer);

            // Exits the configuration by hiding the layers
            // It is called by the Cancel button and the maskLayer event listeners
            // It is a nested function
            function hideLayers( /*_*/) {
                dialogLayer.style.display = "none";
                maskLayer.style.display = "none";
                postHideFunc();
            }

            // Adds the necessary event listeners
            $(maskLayer).click(hideLayers);
            $('#hnPurgeArticleCacheButton').click(makePurgeArticleCacheHandler());
            $('#hnDefaultUserFollowButton').click(makeDefaultUserFollowHandler());
            $('#hnDefaultSiteFollowButton').click(makeDefaultSiteFollowHandler());
            $('#hnSaveConfigurationButton').click(makeSaveConfigurationHandler());
            $('#hnResetEverythingButton').click(makeResetConfigHandler());
            $('#hnLoadConfigurationFile').change(makeLoadConfigurationFileHandler());

            addStyle(
                // Adds styles and classes for the configuration layers and its contents
                "#hnmaskLayer {\n" +
                "    background-color: black !important;\n" +
                "    opacity: 0.7 !important;\n" +
                "    z-index: 2147483645 !important;\n" +
                "    position: fixed !important;\n" +
                "    left: 0px !important;\n" +
                "    top: 0px !important;\n" +
                "    width: 100% !important;\n" +
                "    height: 100% !important;\n" +
                "\n}" +
                "#hndialogLayer {\n" +
                "    background-color: white !important;\n" +
                "    overflow: auto !important;\n" +
                "    padding: 20px !important;\n" +
                "    z-index: 2147483646 !important;\n" +
                "    outline: black solid thin !important;\n" +
                "    position: fixed !important;\n" +
                "    left: 30% !important;\n" +
                "    top: 7.5% !important;\n" +
                "    width: 40% !important;\n" +
                "    height: 85% !important;\n" +
                "    text-align: left !important;\n" +
                "    font-family: 'Times New Roman',Times,serif !important;\n" +
                "    text-shadow: 0 0 1px #ccc !important;\n" +
                "\n}" +
                "#hndialogLayer > * {\n" +
                "    margin: 20px 0px !important;\n" +
                "}\n" +
                "#hndialogLayer li {\n" +
                "    margin: 15pt 0px 7px !important;\n" +
                "    line-height: 1.5 !important;\n" +
                "    font-style: italic !important;\n" +
                "    color: #333 !important;\n" +
                "    display: list-item !important;\n" +
                "    list-style-type: none !important;\n" +
                "    font-size: 11pt !important;\n" +
                "    font-family: " + DIALOG_FONT_FAMILY + " !important;\n" +
                "    background: url(data:image/gif;base64,R0lGODlhAQACAIAAAMncpv///yH5BAAAAAAALAAAAAABAAIAAAICRAoAOw==) left bottom repeat-x !important;\n" +
                "    border: none !important;\n" +
                "}\n" +
                "#hndialogLayer input, #hndialogLayer select {\n" +
                "    vertical-align: bottom !important;\n" +
                "    margin-right: 0.5em !important;\n" +
                "    margin-bottom: 1em !important;\n" +
                "    color: #333 !important;\n" +
                "    background-color: " + HN_LIGHT_ORANGE + " !important;\n" +
                "    font-size: 10pt !important;\n" +
                "    line-height: 1.2 !important;\n" +
                "    border: 1px solid #333 !important;\n" +
                "    width: 100% !important;\n" +
                "}\n" +
                "#hnconfTitle {\n" +
                "    cursor: default !important;\n" +
                "    font-size: 14pt !important;\n" +
                "    line-height: 1.5 !important;\n" +
                "    font-weight: bold !important;\n" +
                "    text-align: center !important;\n" +
                "    color: #333 !important;\n" +
                "    margin: 20px !important;\n" +
                "    font-family: " + DIALOG_FONT_FAMILY + " !important;\n" +
                "}\n" +
                "#hnconfButDiv {\n" +
                "    text-align: center !important;\n" +
                "}\n" +
                "#hnconfButDiv input {\n" +
                "    margin: 5px !important;\n" +
                "}\n" +
                "#hndialogLayer ul {\n" +
                "    list-style-type: none !important;\n" +
                "    line-height: 1.5 !important;\n" +
                "    padding-left: 40px !important;\n" +
                "    padding-right: 40px !important;\n" +
                "    color: #333 !important;\n" +
                "    list-style-image: none !important;\n" +
                "    display: list-item !important;\n" +
                "    font-size: 11pt !important;\n" +
                "    font-family: " + DIALOG_FONT_FAMILY + " !important;\n" +
                "    " +
                "background: none !important;\n" +
                "    border: none !important;\n" +
                "}\n" +
                "#hndialogLayer em {\n" +
                "    font-weight: bold !important;\n" +
                "    font-style: normal !important;\n" +
                "    color: red !important;\n" +
                "}\n");
        }

        // Changes the enabled state of all input/select fields of the dialog layer. If newState is undefined or not boolean, it does nothing
        // It is a nested function
        function setDialogInputState(newState) {
            if (typeof (newState) !== "boolean") return;
            var allInputs = $xp(".//input|.//select", dialogLayer);
            allInputs.forEach(function(i) {
                i.disabled = !newState;
            });
        }

        function makeResetConfigHandler( /*_*/) {
            return function() {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);

                    setValue('config', {
                        'people': DEFAULT_PEOPLE,
                        'nobles': DEFAULT_NOBLES,
                        'sights': DEFAULT_SITES,
                    });

                    vacuumThrottle();

                    // Reloads page and script
                    window.location.reload();
                }, 0);
            };
        }

        function makePurgeArticleCacheHandler( /*_*/) {
            return function() {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);

                    setRawValue('HEADLINES_CACHE', JSON.stringify({}));
                    setRawValue('COMMENTS_CACHE', JSON.stringify({}));

                    vacuumThrottle();

                    // Reloads page and script
                    window.location.reload();
                }, 0);
            };
        }

        function makeDefaultUserFollowHandler( /*_*/) {
            return function() {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);
                    config.nobles = DEFAULT_NOBLES;
                    setValue('config', config);

                    // Reloads page and script
                    window.location.reload();
                }, 0);
            };
        }

        function makeDefaultSiteFollowHandler( /*_*/) {
            return function() {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);
                    config.sights = DEFAULT_SITES;
                    setValue('config', config);

                    // Reloads page and script
                    window.location.reload();
                }, 0);
            };
        }

        function makeSaveConfigurationHandler( /*_*/) {
            return function() {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);

                    DiskFile.saveAsFile(JSON.stringify(config), "HackerNews.json", "text/plain;charset=utf-8");

                    vacuumThrottle();

                    // Reloads page and script
                    window.location.reload();
                }, 0);
            };
        }

        function makeLoadConfigurationFileHandler( /*_*/) {
            return function(evt) {
                setTimeout(function() {
                    // Disables the input/select fields
                    setDialogInputState(false);

                    DiskFile.readFromFile(evt);

                    vacuumThrottle();
                }, 0);
            };
        }
    }

    var DiskFile = {
        /*
         * saveAsFile(text, filename, mimetype)
         *
         * Example: saveAsFile("Some content","filename.txt","text/plain;charset=utf-8");
         */
        saveAsFile: function(text, filename, mimetype) {
            try {
                var blob = new Blob([text], {
                    type: mimetype
                });
                /* global saveAs */
                saveAs(blob, filename);
            } catch (e) {
                window.open("data:" + mimetype + "," + encodeURIComponent(text), '_blank', '');
            }
        },
        readFromFile: function(e) {
            var file = e.target.files[0];
            if (!file) {
                return;
            }
            var reader = new FileReader();
            reader.onload = function(e) {
                var contents = e.target.result;
                try {
                    var value = JSON.parse(contents);
                    if (!value || !value.people || !value.nobles || !value.sights) throw 'Could not parse that JSON';
                    setValue('config', value);
                    // Reloads page and script
                    window.location.reload();
                } catch (ex) {
                    console.log('readFromFile', ex);
                    alert("Could not parse that JSON");
                }
            };
            reader.readAsText(file);
        },
    };

    var registerMenuCommand = typeof GM_registerMenuCommand !== 'undefined' ? GM_registerMenuCommand : function() { };

    function postDialogHide() {
        doLogo();
        highlightFollowedUsers();
    }

    // Registers the configuration menu command
    registerMenuCommand("Hacker News Configuration", curriedConfigureScript(), null, null, "H");
    registerMenuCommand((darkMode ? "Light" : "Dark") + " Mode", toggleDarkMode, null, null, "T");

    function toggleDarkMode() {
        setValue('darkMode', !darkMode);
        window.location.reload();
    }

    function doLogo() {
        try {
            var config_link = document.getElementById('ourlogo');
            if (!config_link) {
                var logo = $xp('/html/body/center/table/tbody/tr/td/table/tbody/tr/td/a')[0];
                if (!logo) return;
                config_link = document.createElement('span');
                config_link.id = 'ourlogo';
                config_link.style.color = HN_DARK_ORANGE;
                config_link.style.padding = '3px 5px 3px 5px';
                config_link.style.borderRadius = '3px';
                config_link.style.border = '1px dotted';
                config_link.style.cursor = 'pointer';
                config_link.setAttribute('alt', 'Configure Hacker News Improvements plugin');
                config_link.setAttribute('title', 'Configure Hacker News Improvements plugin');
                logo.parentNode.appendChild(config_link);
                $(config_link).click(curriedConfigureScript());
            }
            config_link.innerHTML = config.nobles.length + '/' + Object.keys(config.people).length + '/' + config.sights.length; // 'C';
        } catch (ce) {
            console.log(ce);
        }
    }

    function infinite_scroll() {
        if (!isPaginatedPage()) return;

        var $table;
        var $tablebg;
        var fetch;
        var loading = false;

        function more() {
            fetch = $('a[rel*="next"]').filter(function() {
                var nextext = $(this).text();
                if (nextext) {
                    return /^(More$|.* more comments...)/.exec(nextext);
                }
                return false;
            });

            var tr = fetch.closest('tr');
            if (!tr || !tr[0] || !tr[0].previousElementSibling || !/morespace/.exec(tr[0].previousElementSibling.className)) {
                tr = tr.closest('tr');
            }
            $tablebg = $(document.body).css('background-color');
            if (!$table) $table = tr.closest('table');
            tr.prev().remove();
            tr.remove();
        }

        more();

        $(window).scroll(function() {
            if (loading) return;
            if (infiniteScrollDisabled) return;
            if ($(window).scrollTop() + getViewportHeight() > $(document).height() - 100) {
                loading = true;
                var href = fetch.attr('href');
                if (typeof href === 'undefined') {
                    console.log("nothing more to fetch");
                    loading = false;
                    return;
                }
                console.log("fetching", href);
                $table.css('background-color', darkMode ? '#333' : '#DFD');
                $.get(href, function(data, status) {
                    var $newtable;
                    var $trs;
                    if (status == 'success') {
                        if (isMainPage()) {
                            $newtable = $('<div>').html(data).find('a[id^="up_"]').closest('table');
                            $trs = $newtable.find('tr');
                        } else if (isCommentsPage()) {
                            $newtable = $('<div>').html(data).find('table[id="hnmain"]');
                            $trs = $newtable.find('tr.athing');
                        }
                        $table.append($trs);

                        loading = false;
                        $table.css('background-color', $tablebg);
                        more();
                        work();
                    } else {
                        alert(status);
                    }
                });
            }
        });
    }

    // *************************
    // addStyle
    // *************************
    var addStyle = typeof addStyle != 'undefined' ? addStyle : function(css) {
        if (typeof GM_addStyle !== 'undefined') {
            GM_addStyle(css);
            return;
        }

        var heads = document.getElementsByTagName('head');
        var root = heads ? heads[0] : document.body;
        var style = document.createElement('style');
        try {
            style.innerHTML = css;
        } catch (x) {
            style.innerText = css;
        }
        style.type = 'text/css';
        root.appendChild(style);
    };

    var $morelinks_background_color = HN_DARK_ORANGE;
    var $morelinks_hover_color = HN_LIGHT_ORANGE;

    var $morelinks = {

        _menu_items: [
            ['/classic', 'Classic', 'classic'],
            ['/best', 'Highest voted recent links', 'best'],
            ['/active', 'Most active current discussions', 'active'],
            ['/bestcomments', 'Highest voted recent comments', 'best comments'],
            ['/highlights', 'Curated comments', 'curated comments'],
            ['/leaders', 'Users with most karma', 'best users'],
            ['/newest', 'Newest stories', 'newest stories'],
            ['/hidden', 'Hidden stories (for a week)', 'hidden stories'],
            ['/shownew', 'Newest Show HN', 'newest show'],
            ['/over?points=50', 'Posts with score over 50', 'Over 50'],
            ['/over?points=100', 'Posts with score over 100', 'Over 100'],
            ['/over?points=200', 'Posts with score over 200', 'Over 200'],
            ['/over?points=300', 'Posts with score over 300', 'Over 300'],
            ['/over?points=400', 'Posts with score over 400', 'Over 400'],
            ['/over?points=450', 'Posts with score over 450', 'Over 450'],
            ['/over?points=500', 'Posts with score over 500', 'Over 500'],
            ['/noobstories', 'Submissions from new accounts', 'noob stories'],
            ['/noobcomments', 'Comments from new accounts', 'noob comments'],
            ['https://hnprofile.com/', 'Find SMEs', 'SMEs'],
        ],

        _draw_css: function() {
            var css = '#morelinks_menu{position:absolute;display:inline;}' +
                '#morelinks_menu ul .item{display:none;}' +
                '#morelinks_menu ul .top{margin-bottom:4px;}' +
                '#morelinks_menu ul:hover .item{display:block;background-color:' + $morelinks_background_color + ';}' +
                '#morelinks_menu ul:hover .item a{padding:5px 10px;display:block;}' +
                '#morelinks_menu ul:hover .item a:hover{background-color:' + $morelinks_hover_color + '}' +
                '#morelinks_menu ul{width:100%;float:left;margin:0;padding:0 2px 2px 2px;list-style:none;}';
            return css;
        },

        init: function() {
            if (document.getElementById("morelinks_menu")) return;
            //var menu_links = $xp('/html/body/center/table/tbody/tr/td/table/tbody/tr/td[2]/span[@class="pagetop"]/a[@href="show"]');
            var menu_links = $xp('/html/body/center/table/tbody/tr/td/table/tbody/tr/td[2]/span[@class="pagetop"]/a[@href="submit"]');
            if (!menu_links) return;
            var submit_link_parent = menu_links[0].parentNode;
            var dd_link = '<style>' + this._draw_css() + '</style>' +
                ' | <div id="morelinks_menu"><ul>' +
                '<li class="top"><a href="/lists">other pages</a></li>';
            for (var i = 0, k = this._menu_items.length; i < k; i++) {
                dd_link += '<li class="item"><a href="' + this._menu_items[i][0] + '" title="' + this._menu_items[i][1] + '">' + this._menu_items[i][2] + '</a></li>';
            }
            dd_link += '</ul></div>';
            submit_link_parent.innerHTML += dd_link;
        }
    };

    function genFollowDisplay() {
        var html = '<table><thead><tr><th>User</th><th>Un/Follow</th><th>Notes</th></tr></thead>';
        if (config && config.people) {
            for (var human in config.people) {
                if (config.people.hasOwnProperty(human)) {
                    html += '<tr><td><a href="/user?id=' + human + '" target="_blank">' + human + '</td><td><a href="#" class="user_follow"' + (config.nobles.indexOf(human) != -1 ? ' title="Unfollow ' + human + '" username="' + human + '">-' : ' title="Follow"' + human + '" username="' + human + '">+') + '</a></td><td>' + escapeHTML(config.people[human]) + "</td></tr>\n";
                }
            }
        }
        html += '<tr><td colspan="2"><b>Following sites</b></td><td>' + config.sights.join(", ") + '</td></tr>';
        html += '</table>';
        return html;
    }

    function newTabifyLinks(doc) {
        doc = doc || document;
        if (!doc) return;
        $('a', doc).each(function() {
            if (
                /^[Aa]$/.exec(this.nodeName) && // for <a> only
                this.id === '' &&
                this.parentNode.className != 'pagetop' &&
                this.parentNode.parentNode.className != "pagetop" &&
                this.parentNode.className != "yclinks" &&
                this.innerText != "More" &&
                this.innerText != "link" &&
                this.innerText != "reply" &&
                !this.onClick && // no onClick defined
                this.href && // has a href
                !(/(?:^javascript:)/.exec(this.href))) { // not javascript:
                this.target = "_blank";
            }
        });
    }

    function initCache(a_name) {
        var now = Date.now();
        var the_cache;
        try {
            the_cache = getRawValue(a_name, '{}'); // needs a comma to test
            the_cache = JSON.parse(the_cache);
            if (!the_cache) return {};
            // rough purge of the_cache
            for (var cached in the_cache) {
                if (the_cache.hasOwnProperty(cached) && (now - the_cache[cached].t) >= CACHE_PERIOD) {
                    delete the_cache[cached];
                }
            }
        } catch (e) {
            the_cache = {};
        }
        return the_cache;
    }

    function persistCache(a_name, a_cache) {
        try {
            setRawValue(a_name, JSON.stringify(a_cache));
            return true;
        } catch (e) {
            console.log("Could not persist cache", a_name, e);
            return false;
        }
    }

    function getInnermostHovered() {
        var n = document.querySelector(":hover");
        var nn;
        while (n) {
            nn = n;
            n = nn.querySelector(":hover");
        }
        return nn;
    }

    function showParentOnlyOnDelay(on) {
        try {
            var on_now = getInnermostHovered();
            if (on != on_now) return;

            var viewport = {};
            var bounds = {};

            var $this = $(on);
            var p = on.id.replace(/^a_/, '');
            var pid = document.getElementById(p);
            var $pid = $(pid);
            var $pid_table = $pid.closest('table');
            var pid_container_row = $pid_table.closest('tr')[0];

            var hidden = (pid_container_row.style.display == 'none');
            if (hidden) {
                // note down current position because displaying the parent will make the document scroll
                viewport.top = 0;
                viewport.scrolled = (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
                viewport.bottom = getViewportHeight();
                bounds.topRelativeToViewport = $this.offset().top - viewport.scrolled;
                bounds.bottomRelativeToViewport = bounds.topRelativeToViewport + $this.outerHeight();
                // unhide the parent
                $(pid_container_row).fadeIn('fast');
            }
            // Make down voted content readable by setting the font to black
            var content_html = $(pid_container_row).find("td.default")[0].innerHTML.replace(/<span class="commtext c[0-9a-f0-9a-f]+">/ig, '<span class="commtext c00">');
            var $hover_container = $('#hover_container');
            // some original + our css rules make it difficult to use the right height
            $hover_container.hide().width($pid.width()).height($(pid).height()).css('top', $this.offset().top - 60 - $pid_table.height().toString() + 'px').css('left', $pid.find("div.comment").offset().left + 'px').html(content_html).css('padding', '10px 10px 20px 10px').css('background', darkMode ? 'black' : 'white').fadeIn('medium');
            if (hidden) {
                // now undo the scroll done by showing the hidden parent
                var selectedPosX = 0;
                viewport.new_scrolled = (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
                var selectedPosY = -bounds.topRelativeToViewport - viewport.scrolled + viewport.new_scrolled;

                while (on_now !== null) {
                    selectedPosX += on_now.offsetLeft;
                    selectedPosY += on_now.offsetTop;
                    on_now = on_now.offsetParent;
                }
                window.scrollTo(selectedPosX, selectedPosY);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function makeParentLink(comment, parentlevel, $hover_container) {
        var link = $('<a id="a_' + parentlevel + '" href="#">parent</a>');
        link.mouseenter(function(event) {
            drop(event);
            setTimeout(showParentOnlyOnDelay, 500, event.target);
            return false;
        });
        link.mouseout(function(event) {
            drop(event);
            try {
                $hover_container.fadeOut('medium').empty();
            } catch (e) {
                console.log(e);
            }
            return false;
        });
        link.click(function(event) {
            drop(event);
            try {
                var parental_unit = event.target.id.replace(/^a_/, '');
                scrollToElement(document.getElementById(parental_unit));
            } catch (e) {
                console.log(e);
            }
            return false;
        });
        var r = $(comment).find('u');
        if (r[0]) {
            r.last().after(link).after(" | ");
        } else {
            $(comment).find('font').last().append(link);
        }
    }

    // Hover on username to show info
    // uses global $hover_container
    var userinfo = {
        cache: {},
        popupInfo: function($this, data) {
            var html = data.html().replace(/([^"'])((http|ftp)s?:\/\/[^"' <]+)/g, '$1<a href="$2" target="_blank">$2</a>');
            $('#hover_container').hide().width('95%').height('auto').css('background', 'inherit').css('top', $this.offset().top + 20 + 'px').css('left', $this.offset().left + 'px').empty().html(html).css('padding', '10px 10px 20px 10px').css('color', darkMode ? 'white' : 'white').fadeIn('medium');
        },
        tooltip: function(on) {
            try {
                var on_now = getInnermostHovered();
                if (on != on_now) return;
                var $this = $(on);
                if ($this.parent().parent().hasClass('athing')) return false; // Don't popup for username in tooltip itself
                var alink = on.href;
                if (userinfo.cache[alink]) {
                    userinfo.popupInfo($this, userinfo.cache[alink]);
                } else {
                    console.log("fetching", alink);
                    $.get(alink, function(data, status) {
                        if (status != 'success') {
                            alert(status);
                        } else if (data != '') {
                            userinfo.cache[alink] = $(data).find('a[href*="submitted?id="]').closest('table');
                            userinfo.popupInfo($this, userinfo.cache[alink]);
                        } else {
                            alert('No data returned. Likely banned. Better pause!');
                        }
                    });
                }
            } catch (e) {
                console.log(e);
            }
        },
        init: function() {
            try {
                $(document).on('mouseenter', 'a.hnuser', function(event) {
                    drop(event);
                    if (/\/user\b/.exec(window.location.href)) return false;
                    setTimeout(userinfo.tooltip, 500, event.target);
                    return false;
                });
                $(document).on('mouseleave', '#hover_container', function(event) {
                    drop(event);
                    $(this).fadeOut('medium').empty();
                    return false;
                });
            } catch (e) {
                console.log(e);
            }
        }
    };

    function makeStoryUnFollowFunction(sid) {
        return function(event) {
            drop(event);
            setTimeout(function() {
                var comments_cache = initCache('COMMENTS_CACHE');
                if (comments_cache[sid]) {
                    delete comments_cache[sid];
                    persistCache('COMMENTS_CACHE', comments_cache);
                }
                $(event.target).hide();
                $(event.target.parentNode).find('a').css('background-color', 'inherit');
            }, 0);
            return false;
        };
    }

    function UserUnFollowFunction(event) {
        drop(event);
        var $target = $(event.target);
        var username = $target.attr('username');
        var label = $target.text();
        setTimeout(function(targ) {
            try {
                if (label == '+') {
                    var desc = prompt("Description for " + username + "?", config.people[username] || username);
                    if (desc) {
                        $(targ).text('-');
                        config.people[username] = desc;
                        addUnique(config.nobles, username);
                    } else {
                        delete config.people[username];
                        config.nobles = config.nobles.filter(function(a) {
                            return a != username;
                        });
                    }
                    config.nobles.sort(iCaseSort);
                    setValue('config', config);
                    DiskFile.saveAsFile(JSON.stringify(config), "HackerNews.json", "text/plain;charset=utf-8");
                    if ($('#hndialogLayer').invisible()) {
                        doLogo();
                        highlightFollowedUsers();
                    }
                } else if (label == '-') {
                    config.nobles = config.nobles.filter(function(a) {
                        return a != username;
                    });
                    config.nobles.sort(iCaseSort);
                    setValue('config', config);
                    $(targ).text('-');
                    DiskFile.saveAsFile(JSON.stringify(config), "HackerNews.json", "text/plain;charset=utf-8");
                    if ($('#hndialogLayer').invisible()) {
                        doLogo();
                        highlightFollowedUsers();
                    }
                }
            } catch (anything) {
                console.log(anything);
            }
            return false;
        }, 0, $target[0]);
        return false;
    }

    function makeSiteUnFollowFunction(sitename) {
        return function(event) {
            drop(event);
            setTimeout(function() {
                try {
                    var label = $(event.target).text();
                    if (sitename) {
                        if (label == '+') {
                            addUnique(config.sights, sitename);
                            setValue('config', config);
                            DiskFile.saveAsFile(JSON.stringify(config), "HackerNews.json", "text/plain;charset=utf-8");
                        } else {
                            config.sights = config.sights.filter(function(a) {
                                return a != sitename;
                            });
                            setValue('config', config);
                            DiskFile.saveAsFile(JSON.stringify(config), "HackerNews.json", "text/plain;charset=utf-8");
                        }
                        doLogo();
                        highlightFollowedSites();
                    }
                } catch (anything) {
                    console.log(anything);
                }
                return false;
            }, 0);
            return false;
        };
    }

    function makeFollowUserLink(follow_re, user_anode) {
        if (!follow_re || !user_anode) return;
        try {
            var $user = $(user_anode);
            var username = $user.text();
            if (username) {
                if (follow_re.exec(username)) {
                    $user.css({
                        'background-color': HN_DARK_ORANGE,
                        'color': darkMode ? 'white' : 'black',
                        'padding': '0 2px',
                        'border-radius': '3px',
                    });
                    $user.after($('<a href="#" class="user_follow" username="' + username + '" title="Unfollow ' + username + '">-</a>').on('click', UserUnFollowFunction));
                } else {
                    $user.css({
                        'background-color': 'inherit',
                        'color': 'inherit',
                        'padding': '0 2px',
                        'border-radius': '3px',
                    });
                    $user.after($('<a href="#" class="user_follow" username="' + username + '" title="Follow ' + username + '">+</a>').on('click', UserUnFollowFunction));
                }
                var desc = config.people[username];
                if (desc) {
                    var esc_desc = escapeHTML(desc);
                    $user.attr('alt', desc).attr('title', desc);
                    $user.after($('<span class="flair" alt="' + esc_desc + '" title="' + esc_desc + '">' + esc_desc + '</span>'));
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    // color people followed and make follow/unfollow links etc.
    function highlightFollowedUsers(doc) {
        try {
            doc = doc || document;
            var follow_re = new RegExp('^(' + config.nobles.join("|") + ')$');
            if (isMainPage()) {
                // color people followed and make follow/unfollow links etc.
                $('.user_follow, .flair', doc).remove();

                try {
                    $('.subtext a:nth-child(2)', doc).each(function() {
                        makeFollowUserLink(follow_re, this);
                    });
                } catch (it) {
                    console.log(it);
                }
            } else if (isCommentsPage()) {
                $('.user_follow, .flair', doc).remove();

                makeFollowUserLink(follow_re, $('table .subtext a', doc)[0]);
                try {
                    $('span.comhead > a.hnuser', doc).each(function() {
                        makeFollowUserLink(follow_re, this);
                    });
                } catch (a) {
                    console.log(a);
                }
            } else if (isLeadersPage()) {
                $('.user_follow, .flair').remove();
                doLeadersPage();
            } else if (isProfilePage()) {
                $('.user_follow, .flair').remove();
                doProfilePage();
            } else {
                console.log("Don't understand what kind of hn page this is");
            }
        } catch (e) {
            console.log(e);
        }
    }

    // color sites followed and make follow/unfollow links etc.
    function highlightFollowedSites(doc) {
        try {
            doc = doc || document;
            if (isMainPage() || isCommentsPage()) {
                highlightSites(doc);
            } else {
                console.log("Not a site highlightable hn page?");
            }
        } catch (e) {
            console.log(e);
        }
    }

    document.body.setAttribute('last_code', 0);

    function cookedKey(zEvent) {
        return "" +
            (zEvent.altKey ? "Alt " : "") +
            (zEvent.ctrlKey ? "Control " : "") +
            (zEvent.metaKey ? "Meta " : "") +
            (zEvent.shiftKey ? "Shift " : "") +
            // (["Control", "Shift", "Alt", "Meta"].includes(zEvent.key) ? "" : " ") +
            zEvent.key;
    }

    var main_nav = {

        row: 0,

        ncunh: function(e) {
            $(e).parent().row_uncur().prev().row_uncur();
        },

        nch: function(e) {
            var sw = $(e).parent().row_cur().prev().row_cur();
            centerElementByScrolling(sw[0]);
        },

        init: function() {
            if (!isMainPage()) return;
            main_nav.row = 0;

            try {
                var $second_row = $('tr td.subtext').parent().first();
                var $first_row = $second_row.prev();
                $second_row.row_cur();
                centerElementByScrolling(document.body.firstChild);
                $first_row.row_cur().focus();
            } catch (eh) {
                console.log(eh);
            }

            // keypress doesn't get arrow etc., must use keydown
            $(document).keydown(main_nav.main_page_keydown_handler);
        },

        main_page_keydown_handler: function(event) {
            try {
                var shouldBubble = false;
                /* Don't use shortcuts in various text fields */
                if ($("*:focus").is("input") || $("*:focus").is("textarea")) {
                    shouldBubble = true;
                    return shouldBubble;
                }
                var trows, story_href, comment_href;
                var code = cookedKey(event);
                switch (code) {
                    case 'Escape': // Hide tooltip
                        try {
                            $('#hover_container').hide();
                        } catch (esce) { //
                        }
                        return shouldBubble;
                    case 'F1': // help
                    case '?':
                    case 'Shift ?':
                    case 'h':
                    case 'Shift H':
                        drop(event);
                        if (document.body.getAttribute('last_code') != code) {
                            window.alert("h/? - Help\n\n" +
                                "i/I - Toggle infinite scroll\n" +
                                "j/J/Down - Next item\n" +
                                "k/K/Up - Previous item\n\n" +
                                "0 - First item on page\n$ - Last item on page\n\n" +
                                "o/O/Enter/Right - Open item and content in new tabs\n\n" +
                                "s/S/a/A - Open article/story only\n\n" +
                                "c/C - Open comments only\n\n" +
                                "u/U - Unfollow the story\n\n" +
                                "r/R - Reload\n\n" +
                                "t/T - Toggle Dark Mode\n\n" +
                                "n/N - Next unread/followed item\n" +
                                "p/P - Previous unread item"
                            );
                        }
                        document.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'i':
                    case 'I':
                    case 'Shift I':
                        drop(event);
                        infiniteScrollDisabled = !infiniteScrollDisabled;
                        if (infiniteScrollDisabled) {
                            notify("Infinite scrolling disabled");
                        } else {
                            notify("Infinite scrolling enabled");
                        }
                        shouldBubble = false;
                        return shouldBubble;
                    case 'ArrowDown': // scroll down
                    case 'j':
                    case 'J':
                    case 'Shift J':
                        drop(event);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        }
                        main_nav.row++;
                        if (main_nav.row >= trows.length) {
                            main_nav.row = 0;
                        }
                        main_nav.nch(trows[main_nav.row]);
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'ArrowUp': // scroll up
                    case 'k':
                    case 'K':
                    case 'Shift K':
                        drop(event);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        }
                        main_nav.row--;
                        if (main_nav.row < 0) {
                            main_nav.row = trows.length - 1;
                        }
                        main_nav.nch(trows[main_nav.row]);
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case '0': // top of page
                    case 'g':
                        drop(event);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        }
                        main_nav.row = 0;
                        main_nav.nch(trows[main_nav.row]);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'G': // bottom of page
                    case '$':
                    case 'Shift G':
                    case 'Shift $':
                        drop(event);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        }
                        main_nav.row = trows.length - 1;
                        main_nav.nch(trows[main_nav.row]);
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'ArrowRight': // open
                    case 'o':
                    case 'O':
                    case 'Shift O':
                    case 'Enter':
                        drop(event);
                        if (document.body.getAttribute('last_code') != code) {
                            document.body.setAttribute('last_code', code);

                            trows = $('tr td.subtext');
                            var $elem = $(trows[main_nav.row]);
                            var $comment = $elem.parent();
                            var $story = $comment.prev();
                            story_href = $story.find('td.title a').first().attr('href');
                            if (story_href) {
                                openInTab(story_href);
                                if (ANIMATE) $story.fadeOut('slow').fadeIn('slow');
                            }
                            try {
                                comment_href = $elem.find('a[href*="item?id="]').first().attr('href');
                                if (comment_href) {
                                    // Don't open Ask HN twice
                                    if (comment_href != story_href)
                                        openInTab(comment_href);
                                    if (ANIMATE) $comment.fadeOut('slow').fadeIn('slow');
                                } else {
                                    throw "Comments link not found.";
                                }
                            } catch (ec) {
                                console.log(ec);
                            }
                            // Only center on open if essential
                            if (!elementCompletelyInViewport(trows[main_nav.row])) centerElementByScrolling(trows[main_nav.row]);
                        }
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'a': // open article only
                    case 'A':
                    case 'Shift A':
                    case 's':
                    case 'S':
                    case 'Shift S':
                        drop(event);
                        if (document.body.getAttribute('last_code') != code) {
                            document.body.setAttribute('last_code', code);
                            trows = $('tr td.subtext');
                            $story = $(trows[main_nav.row]).parent().prev();
                            story_href = $story.find('td.title a').first().attr('href');
                            if (story_href) {
                                openInTab(story_href);
                                if (ANIMATE) $story.fadeOut('slow').fadeIn('slow');
                            }
                            // Only center on open if essential
                            if (!elementCompletelyInViewport(trows[main_nav.row])) centerElementByScrolling(trows[main_nav.row]);
                        }
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'ArrowLeft': // open comments only
                    case 'c':
                    case 'C':
                    case 'Shift C':
                        drop(event);
                        if (document.body.getAttribute('last_code') != code) {
                            document.body.setAttribute('last_code', code);
                            trows = $('tr td.subtext');
                            $elem = $(trows[main_nav.row]);
                            $comment = $elem.parent();
                            $story = $comment.prev();
                            try {
                                comment_href = $elem.find('a[href*="item?id="]').first().attr('href');
                                if (!comment_href) throw "Comments link not found.";

                                openInTab(comment_href);
                                if (ANIMATE) $comment.fadeOut('slow').fadeIn('slow');
                            } catch (ec) {
                                console.log(ec);
                            }
                            // Only center on open if essential
                            if (!elementCompletelyInViewport(trows[main_nav.row])) centerElementByScrolling(trows[main_nav.row]);
                        }
                        document.body.setAttribute('last_code', code);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'r': // reload
                    case 'R':
                    case 'Shift R':
                        document.body.setAttribute('last_code', code);
                        window.location.reload();
                        break;
                    case 't': // toggle dark mode
                    case 'T':
                    case 'Shift T':
                        drop(event);
                        toggleDarkMode();
                        break;
                    case 'n': // next unread or followed
                    case 'N':
                    case 'Shift N':
                        drop(event);
                        document.body.setAttribute('last_code', code);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        } else {
                            main_nav.row = 0;
                        }
                        var next_unread, l;
                        for (next_unread = main_nav.row + 1, l = trows.length; next_unread < l; next_unread++) {
                            if ($(trows[next_unread]).parent().prev().css('background-color') == HIGHLIGHT_COLOR ||
                                $(trows[next_unread]).find('a.highlight').length) {
                                main_nav.row = next_unread;
                                main_nav.nch(trows[main_nav.row]);
                                shouldBubble = false;
                                return shouldBubble;
                            }
                        }
                        main_nav.row = l - 1;
                        main_nav.nch(trows[main_nav.row]);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'p': // previous unread or followed
                    case 'P':
                    case 'Shift P':
                        drop(event);
                        document.body.setAttribute('last_code', code);
                        trows = $('tr td.subtext');
                        if (main_nav.row >= 0) {
                            main_nav.ncunh(trows[main_nav.row]);
                        } else {
                            main_nav.row = 0;
                        }
                        var prev_unread;
                        for (prev_unread = main_nav.row - 1; prev_unread >= 0; prev_unread--) {
                            if ($(trows[prev_unread]).parent().prev().css('background-color') == HIGHLIGHT_COLOR ||
                                $(trows[prev_unread]).find('a.highlight').length) {
                                main_nav.row = prev_unread;
                                main_nav.nch(trows[main_nav.row]);
                                shouldBubble = false;
                                return shouldBubble;
                            }
                        }
                        main_nav.row = 0;
                        main_nav.nch(trows[main_nav.row]);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'u': // unfollow story
                    case 'U':
                    case 'Shift U':
                        drop(event);
                        document.body.setAttribute('last_code', code);
                        trows = $('tr td.subtext');
                        $elem = $(trows[main_nav.row]);
                        var story_id = $elem.find('span.age a')[0].href.replace(/.*item\?id=(\d+).*/, '$1');
                        if (story_id && story_id !== 0) {
                            $elem.parent().find('a').removeClass('highlight').end().find('a.unfollow').remove();
                            setTimeout(function() {
                                var comments_cache = initCache('COMMENTS_CACHE');
                                if (comments_cache[story_id]) {
                                    delete comments_cache[story_id];
                                    persistCache('COMMENTS_CACHE', comments_cache);
                                }
                                shouldBubble = false;
                                return shouldBubble;
                            }, 0);
                        } else {
                            window.alert("Not a story");
                        }
                        shouldBubble = false;
                        return shouldBubble;
                    case 'Meta w': // Cmd+w
                    case 'Meta W':
                        document.body.setAttribute('last_code', code);
                        // Don't block browser function
                        shouldBubble = true;
                        return shouldBubble;
                    default:
                        document.body.setAttribute('last_code', code);
                        console.log('cooked code + [', code + ']');
                        break;
                }
                shouldBubble = true;
                return shouldBubble;
            } catch (e) {
                console.log("main_page_keydown_handler", e);
            }
        }
    };

    function addSearchFields() {
        var selector, cell;
        var items = [{
            'placeholder': 'hndex',
            'id': 'hndexsearch',
            'url': 'https://hndex.ml/',
            'param': 'q'
        },
        {
            'placeholder': 'Topic',
            'id': 'hnprofilesearch',
            'url': 'https://hnprofile.com/author_profiles',
            'param': 'search'
        },
        {
            'placeholder': 'Algolia',
            'id': 'algoliasearch',
            'url': 'https://hn.algolia.com/',
            'param': 'q'
        }
        ];
        try {
            items.forEach(function(i) {
                if (!document.getElementById(i.id)) {
                    // Grab the top bar
                    selector = selector || document.querySelector('td > table > tbody > tr');

                    if (selector) {
                        // Insert a new td between the submit link and the login link
                        cell = selector.insertCell(2);

                        // Inject the search box html into the header
                        cell.innerHTML = '<span id="' + i.id + '">' +
                            '  <form method="get" target="_blank" action="' + i.url + '" style="margin:0;"><input style="height:20px;" type="text" name="' + i.param + '" value="" size="15" placeholder="' + i.placeholder + '"/>' +
                            '</span>';
                    }
                }
            });
        } catch (se) {
            console.log(se);
        }
    }

    function highlightSites(doc) {
        try {
            doc = doc || document;
            var sights_re = new RegExp('^(' + config.sights.join("|") + ')$');
            $('span.sitestr', doc).each(function() {
                var site = this.innerText;
                var oldNode;
                if (sights_re.exec(site)) {
                    $(this).addClass('site-flair');
                    oldNode = $(this).closest('a').prev();
                    if (oldNode && oldNode.hasClass('unfollow')) {
                        oldNode[0].innerText = '-';
                    } else {
                        $(this.parentNode).before($('<a href="#" class="unfollow" title="Unfollow this site">-</a>').click(makeSiteUnFollowFunction(site))).before(' ');

                    }
                    this.highlighted = true;
                } else {
                    $(this).removeClass('site-flair');
                    oldNode = $(this).closest('a').prev();
                    if (oldNode && oldNode.hasClass('unfollow')) {
                        oldNode[0].innerText = '+';
                    } else {
                        $(this.parentNode).before($('<a href="#" class="unfollow" title="Follow this site">+</a>').click(makeSiteUnFollowFunction(site))).before(' ');
                    }
                }
            });
        } catch (ste) {
            console.log(ste);
        }
    }

    function betteridgeALink(link) {
        if (!link) return;
        if (/^(Do \S+ really) /.exec(link.innerHTML)) {
            $(link).addClass('betteridge-yes');
        } else if (/^(Is|Are|Does|Do(?!\s+not)|Has|Have|Did|Will|Can|Could|Should).*\?$/.exec(link.innerHTML)) {
            $(link).addClass('betteridge-no');
        }
    }

    // HN scores highlighter https://greasyfork.org/en/scripts/16552-hn-scores-highlighter START
    var scoresHighlighter = {
        scoreThresholds: [
            20,
            70,
            200,
            500,
            800
        ],
        defaultColor: darkMode ? '#666666' : '#CCCCCC',
        colors: [
            '#666666', // 20
            '#BD9910', // 70
            '#EA7C07', // 200
            '#FF0000', // 500
            darkMode ? "#ff0000" : "#0000FF", // 800
        ],
        parseScore: function(elem) {
            return parseInt(elem.innerHTML.split(" ")[0]);
        },
        getColorForScore: function(score) {
            var color = scoresHighlighter.defaultColor;
            for (var i = 0; i < scoresHighlighter.scoreThresholds.length; i++) {
                if (score >= scoresHighlighter.scoreThresholds[i]) {
                    color = scoresHighlighter.colors[i];
                }
            }
            return color;
        },
        highlight: function() {
            // scores highlighter
            var items = document.querySelectorAll(".score");
            for (var i = 0; i < items.length; i++) {
                items[i].style.fontWeight = 'bold';
                items[i].style.color = scoresHighlighter.getColorForScore(scoresHighlighter.parseScore(items[i]));
            }
        }
    };
    // HN scores highlighter https://greasyfork.org/en/scripts/16552-hn-scores-highlighter END

    function doMainPage(doc) {
        try {
            doc = doc || document;
            highlightSites(doc);

            var now = Date.now();
            var headlines_cache = initCache('HEADLINES_CACHE');
            var comments_cache = initCache('COMMENTS_CACHE');

            $('span.age a:last-child', doc).each(function() {
                if (this.processed) return;
                this.processed = true;
                var story_id = this.href.replace(/.*item\?id=(\d+).*/, '$1');
                if (!story_id) return;
                var $first_row = $(this).closest('tr').first().prev();
                var link = $first_row.find('.title a')[0];
                betteridgeALink(link);
                if (!headlines_cache[story_id]) {
                    $first_row.addClass('highlight');
                    headlines_cache[story_id] = {
                        'c': 0
                    };
                } else {
                    var $comments_count = $(this).closest('td').find('a').last();
                    var text = $comments_count.text();
                    if (comments_cache[story_id]) {
                        $comments_count.addClass('highlight');
                        $comments_count.after($('<a href="#" class="unfollow" title="Unfollow this story">-</a>').click(makeStoryUnFollowFunction(story_id))).after(' ');
                    }
                    var count = text.replace(/[^0-9]/g, '');
                    if (headlines_cache[story_id].c != count && text != 'discuss' && text != 'comments') {
                        var diff = count - headlines_cache[story_id].c;
                        diff = " (" + (diff > 0 ? "+" : "") + diff + ")";
                        $comments_count.after(diff);
                        headlines_cache[story_id].c = count;
                    }
                }
                headlines_cache[story_id].t = now;
            });
            highlightFollowedUsers(doc);

            persistCache('HEADLINES_CACHE', headlines_cache);
            scoresHighlighter.highlight();
        } catch (me) {
            console.log("doMainPage", me);
        }
    }

    function cycleCommentsMode() {
        try {
            if (!isCommentsPage()) return;

            var button = document.getElementById('cycleCommentsModeButton');
            if (button.value == 'Showing all comments (C)') {
                button.value = 'Showing all interesting comments (C)';
                button.style.backgroundColor = HN_LIGHT_ORANGE;
                $('.comment').each(function() {
                    if (isFlatCommentsPage()) {
                        if ($(this).find('a[rel*="nofollow"]:not(a[href*="reply?id="])').length !== 0 || $(this).find('a[target*="_blank"]').length !== 0 || $(this).parent().find('span.flair').length !== 0 || $(this).parent().find('pre').length !== 0 || $(this).parent('td').hasClass('newcomment') || $(this).text().match(/disclaimer|disclosure/i)) {
                            $(this).closest('tr').show();
                            $(this).closest('table').addClass('filtered').removeClass('default');
                        } else {
                            $(this).closest('tr').hide();
                        }
                    } else {
                        if ($(this).find('a[rel*="nofollow"]:not(a[href*="reply?id="])').length !== 0 || $(this).find('a[target*="_blank"]').length !== 0 || $(this).parent().find('span.flair').length !== 0 || $(this).parent().find('pre').length !== 0 || $(this).parent('td').hasClass('newcomment') || $(this).text().match(/disclaimer|disclosure/i)) {
                            $(this).closest('table').addClass('filtered').removeClass('default').closest('tr').show();
                        } else {
                            $(this).closest('table').closest('tr').hide();
                        }
                    }
                });
            } else if (button.value == 'Showing all interesting comments (C)') {
                button.value = 'Showing comments with links only (C)';
                button.style.backgroundColor = HN_LIGHT_ORANGE;
                $('.comment').each(function() {
                    if (isFlatCommentsPage()) {
                        if ($(this).find('a[rel*="nofollow"]:not(a[href*="reply?id="])').length === 0 && $(this).find('a[target*="_blank"]').length === 0) {
                            $(this).closest('tr').hide();
                        } else {
                            $(this).closest('tr').show();
                            $(this).closest('table').addClass('filtered').removeClass('default');
                        }
                    } else {
                        if ($(this).find('a[rel*="nofollow"]:not(a[href*="reply?id="])').length === 0 && $(this).find('a[target*="_blank"]').length === 0) {
                            $(this).closest('table').closest('tr').hide();
                        } else {
                            $(this).closest('table').addClass('filtered').removeClass('default').closest('tr').show();
                        }
                    }
                });
            } else if (button.value == 'Showing comments with links only (C)') {
                button.value = 'Showing comments with known people only (C)';
                button.style.backgroundColor = HN_LIGHT_ORANGE;
                $('.comment').each(function() {
                    if (isFlatCommentsPage()) {
                        if ($(this).parent().find('span.flair').length == 0) {
                            $(this).closest('tr').hide();
                        } else {
                            $(this).closest('tr').show();
                            $(this).closest('table').addClass('filtered').removeClass('default');
                        }
                    } else {
                        if ($(this).parent().find('span.flair').length == 0) {
                            $(this).closest('table').closest('tr').hide();
                        } else {
                            $(this).closest('table').addClass('filtered').removeClass('default').closest('tr').show();
                        }
                    }
                });
            } else if (button.value == 'Showing comments with known people only (C)') {
                button.value = 'Showing unread comments only (C)';
                button.style.backgroundColor = HN_LIGHT_ORANGE;
                $('.comment').each(function() {
                    if (isFlatCommentsPage()) {
                        if (!$(this).parent('td').hasClass('newcomment')) {
                            $(this).closest('tr').hide();
                        } else {
                            $(this).closest('tr').show();
                            $(this).closest('table').addClass('filtered').removeClass('default');
                        }
                    } else {
                        if (!$(this).parent('td').hasClass('newcomment')) {
                            $(this).closest('table').closest('tr').hide();
                        } else {
                            $(this).closest('table').addClass('filtered').removeClass('default').closest('tr').show();
                        }
                    }
                });
            } else if (button.value == 'Showing unread comments only (C)') {
                button.value = 'Showing all comments (C)';
                button.style.backgroundColor = 'transparent';
                $('.comment').each(function() {
                    if (isFlatCommentsPage()) {
                        $(this).closest('tr').show();
                        $(this).closest('table').addClass('default').removeClass('filtered');
                    } else {
                        $(this).closest('table').addClass('default').removeClass('filtered').closest('tr').show();
                    }
                });
            }
        } catch (cp) {
            console.log(cp);
        }
    }

    function makeCommentsPageKeydownHandler(story_id) {
        return function(event) {
            var shouldBubble;
            try {
                /* Don't use shortcuts in various text fields */
                if ($("*:focus").is("input") || $("*:focus").is("textarea")) {
                    shouldBubble = true;
                    return shouldBubble;
                }
                var code = cookedKey(event);
                switch (code) {
                    case 'Escape': // Escape
                        try {
                            $('#hover_container').hide();
                        } catch (esce) { //
                        }
                        return false;
                    case 'F1': // help
                    case '?':
                    case 'Shift ?':
                    case 'h':
                    case 'Shift H':
                        drop(event);
                        window.alert("h/?/F1 - Help\n\n" +
                            "c/C - Cycle comments type\n\n" +
                            "g/0 - Go to the top of the page\n\n" +
                            "G/$ - Go to the bottom of the page\n\n" +
                            "r/R - Reload this page\n\n" +
                            "t/T - Toggle Dark Mode\n\n" +
                            "u/U - Unfollow this story\n\n" +
                            "Esc - Dismiss any tooltip popup");
                        return false;
                    case 'c': // cycle mode
                    case 'C':
                    case 'Shift C':
                        drop(event);
                        cycleCommentsMode();
                        return false;
                    case 'g': // top of the page
                    case '0':
                        drop(event);
                        scrollToElement(document.body);
                        shouldBubble = false;
                        return shouldBubble;
                    case 'G': // bottom of page
                    case '$':
                    case 'Shift G':
                    case 'Shift $':
                        drop(event);
                        scrollToElement(document.forms[document.forms.length - 1]);//$('hover_container'));
                        shouldBubble = false;
                        return shouldBubble;
                    case 'r': // reload page
                    case 'R':
                    case 'Shift R':
                        window.location.reload();
                        break;
                    case 't': // toggle dark mode
                    case 'T':
                    case 'Shift T':
                        drop(event);
                        toggleDarkMode();
                        break;
                    case 'u': // unfollow this story
                    case 'U':
                    case 'Shift U':
                        drop(event);
                        if (story_id !== 0) {
                            setTimeout(function() {
                                var comments_cache = initCache('COMMENTS_CACHE');
                                if (comments_cache[story_id]) {
                                    delete comments_cache[story_id];
                                    persistCache('COMMENTS_CACHE', comments_cache);
                                }
                                window.alert("Unfollowed");
                                return false;
                            }, 0);
                        } else {
                            window.alert("Not a story");
                        }
                        return false;
                    case 'Cmd w':
                        // Don't block browser function
                        return true;
                    default:
                        console.log(code);
                        break;
                }
            } catch (e) {
                console.log("Comments page keydown handler", e);
            }
            return true;
        };
    }

    function doCommentsPage(doc) {
        try {
            doc = doc || document;
            addSearchFields(doc);
            highlightSites(doc);
            // BUG: The insertion in autopager continued pages is weird. Each comment
            // seems to be counting for one page. The result is that parent navigation
            // does not work for continued pages. To make it work as it is now, the
            // parentstack mechanism would need to be persisted (in the page?).
            //
            // An alternate is to rescan from the top every time. WE ARE CURRENTLY USING
            // THIS.
            if (!document.getElementById('hover_container')) {
                $('body').append('<div id="hover_container" style="position:absolute;left:0px;top:0px;display:none;width:100px;height:100px;border-radius:10px;z-index:100;background: rgba(240,240,240,0.95);color:white !important;border: 1px solid #ccc;box-shadow:3px 3px 5px rgba(0,0,0,0.3);padding:10px 10px 20px 10px;"></div>');
            }
            var comments_cache = initCache('COMMENTS_CACHE');
            var $hover_container = $('#hover_container');

            var x;
            var oldlevel = -1;
            var parentstack = ['comment_none'];
            var story_id = window.location.href.replace(/^.*item\?id=(\d+).*$/, '$1');
            if (!/^\d+$/.exec(story_id)) {
                // Not a story but other comments like page like somebody's comments history
                story_id = 0;
            }
            if (story_id) {
                var link = $('tr.athing td.title a')[0];
                betteridgeALink(link);
                if (!comments_cache[story_id]) {
                    comments_cache[story_id] = {};
                    comments_cache[story_id].c = {};
                }
                comments_cache[story_id].t = Date.now();
                if (!document.getElementById(story_id + '_unfollow') && !/threads\?id=/.exec(document.location.href)) {
                    $('td.title', doc).append(' ').append($('<a href="#" id="' + story_id + '_unfollow" title="Unfollow this story">-</a>').click(makeStoryUnFollowFunction(story_id)).css('color', HN_LIGHT_ORANGE));
                }
            }

            var debug_levels = false;
            var repeated;

            $('td.default', doc).each(function( /*_, _*/) {
                // The following can be reduced like so using jQuery
                // .closest("table").closest("tr")
                // .not(".level_0")
                var hn_comment_id;
                try {
                    // hn_comment_id = $(this).find('span.comhead a').last().attr('href').replace(/^.*item\?id=(\d+).*/, '$1');
                    hn_comment_id = $(this).find('a').filter(function() {
                        return /item\?id=/.exec(this.href);
                    }).first().attr('href').replace(/^.*item\?id=(\d+).*/, '$1');
                } catch (e) {
                    console.log("Could not determine comment id. Probably a deleted comment.");
                }
                if (!hn_comment_id) {
                    try {
                        // hn_comment_id = $(this).find('span.comhead a').last().attr('href').replace(/^.*item\?id=(\d+).*/, '$1');
                        hn_comment_id = $(this).find('a').filter(function() {
                            return /reply\?id=/.exec(this.href);
                        }).last().attr('href').replace(/^.*reply\?id=(\d+).*/, '$1');
                    } catch (e) {
                        console.log("Could not determine comment id. Probably a deleted comment.");
                    }
                }
                if (debug_levels) console.log("comment_id", hn_comment_id);
                if (!(/^[0-9]+$/).exec(hn_comment_id)) {
                    console.log("Could not determine good comment id");
                    return;
                }
                if (story_id && !comments_cache[story_id].c[hn_comment_id]) {
                    $(this).addClass('newcomment');
                    comments_cache[story_id].c[hn_comment_id] = 1;
                }
                var new_id = 'comment_' + hn_comment_id;
                // Is it already processed in a previous pass? If yes then we only let
                // the stack level calculation code work and not create any links etc.
                repeated = document.getElementById(new_id);
                $(this).attr('id', new_id);
                var level;
                try {
                    level = $(this.previousElementSibling.previousElementSibling.firstChild).attr('width') / 40;
                    if (level > oldlevel) {
                        if (debug_levels) console.log(oldlevel, "v", level);
                        if (level && !repeated) {
                            makeParentLink(this, parentstack[level], $hover_container);
                        }
                        for (x = oldlevel; x < level; x++) {
                            parentstack.push(new_id);
                        }
                    } else if (level < oldlevel) {
                        if (debug_levels) console.log(oldlevel, "^", level);
                        for (x = level; x <= oldlevel; x++) {
                            parentstack.pop();
                        }
                        parentstack.push(new_id);
                        if (level && !repeated) {
                            makeParentLink(this, parentstack[level], $hover_container);
                        }
                    } else {
                        if (debug_levels) console.log("=", level);
                        parentstack.pop();
                        parentstack.push(new_id);
                        if (level && !repeated) {
                            makeParentLink(this, parentstack[level], $hover_container);
                        }
                    }
                    if (debug_levels) console.log("Stack at", hn_comment_id, "=", parentstack);
                    oldlevel = level;
                } catch (nolevel) {
                    // e.g. /bestcomments has no threading. it is just individual comments
                }
            });

            // Distinguish OP comments
            try {
                var op = $('table .subtext', doc).find('a').eq(0).text();
                // https://news.ycombinator.com/threads?id=<user>
                if (!op || op === '') {
                    var reg = /threads.id=([a-zA-Z0-9_]+)/;
                    var result = reg.exec(window.location.href);
                    if (result) op = result.match[1];
                }

                $('.comhead a').filter(function() {
                    return $(this).html() == op;
                }, doc).closest('td').addClass('op');
            } catch (e) {
                console.log("Couldn't do the op color change thing", e);
            }
            highlightFollowedUsers(doc);
            persistCache('COMMENTS_CACHE', comments_cache);
            if (!document.getElementById('cycleCommentsModeButton')) {
                var $insert_where;
                if (isFlatCommentsPage()) {
                    $insert_where = $('.comment:first').closest('table');
                } else {
                    $insert_where = $('.comment:first').closest('table').closest('tbody').closest('table');
                }
                $insert_where.before($("<center>" +
                    "<input type='button' id='cycleCommentsModeButton' value='Showing all comments (C)' alt='Press C' title='Press C' class='big-button'/>" +
                    "</center>"));
                $("#cycleCommentsModeButton").click(cycleCommentsMode);

                // keypress doesn't get arrow etc., must use keydown
                $(document).keydown(makeCommentsPageKeydownHandler(story_id));
            }
        } catch (cp) {
            console.log(cp);
        }
    }

    function doLeadersPage() {
        try {
            var follow_re = new RegExp('^(' + config.nobles.join("|") + ')$');
            $('a.hnuser').each(function() {
                try {
                    makeFollowUserLink(follow_re, this);
                } catch (e1) {
                    console.log("Inner", e1);
                }
            });
        } catch (e) {
            console.log(e);
        }
    }

    function doProfilePage() {
        try {
            var follow_re = new RegExp('^(' + config.nobles.join("|") + ')$');
            var $u_node = $('a.hnuser');
            // wrap in a dummy span to help makeFollowUserLink which appends
            $u_node.html('<span>' + $u_node.html() + '</span>');
            makeFollowUserLink(follow_re, $u_node.first()[0]);
            var $about = $u_node.closest('tr').next().next().next();
            $about.html($about.html().replace(/([^"'])((http|ftp)s?:\/\/[^"' <]+)/g, '$1<a href="$2">$2</a>')).find('a').attr('target', '_blank');
        } catch (e) {
            console.log(e);
        }
    }

    function notify(msg) {
        // Show the message
        $('#notification_container').html(msg).fadeIn('slow');

        // Hide the message after 3 seconds
        setTimeout(function() {
            $('#notification_container').fadeOut('slow');
        }, 3000);
    }

    function doOneTime() {
        try {
            if (document.done_one_time) return;
            document.done_one_time = true;
            $('body').append('<div id="hover_container" style="position:absolute;left:0px;top:0px;display:none;width:100px;height:100px;border-radius:10px;z-index:100;background: rgba(240,240,240,0.95);color:white !important;border: 1px solid #ccc;box-shadow:3px 3px 5px rgba(0,0,0,0.3);padding:10px 10px 20px 10px;"></div>');
            $('body').append('<div id="notification_container" style="position:fixed;top:1em;right:1em;display:none;text-align:center;width:auto;height:1em;border-radius:10px;z-index:100;background: rgba(24,24,24,0.95);color:white !important;border: 1px solid #ccc;box-shadow:3px 3px 5px rgba(0,0,0,0.3);;padding:10px 10px 20px 10px;"></div>');

            $('span.pagetop font', document).attr('color', (darkMode ? 'white' : 'black'));

            doLogo();
            addSearchFields();
            infinite_scroll();
            userinfo.init();
            main_nav.init();

            addStyle(
                "body {\n" +
                "    text-rendering: optimizeLegibility;\n" +
                "}\n" +
                "body, table {\n" +
                "    background-color: " + (darkMode ? "rgb(16, 20, 23)" : "#fcfcfc") + ";\n" +
                "    font-family: " + FONT_FAMILY + " !important;\n" +
                "    font-size: 11pt;\n" +
                "    line-height: 1.5;\n" +
                "}\n" +
                "div.toptext {\n" +
                "    color: " + (darkMode ? "white" : "black") + ";\n" +
                "}\n" +
                ".filtered {\n" +
                "    background-color: " + (darkMode ? "#303030" : "#ffeecc") + " !important;\n" +
                "}\n" +
                "pre, code {\n" +
                "    font-family: Menlo, Monaco, \"Bitstream Vera Sans Mono\", \"Lucida Console\", Consolas, Terminal, \"Courier New\", Courier, monospace;\n" +
                "    background-color: " + (darkMode ? "rgba(255, 255, 255, 0.02)" : "rgba(0, 0, 0, 0.02)") + ";\n" +
                "}\n" +
                "table {\n" +
                "    width: 100%;\n" +
                "}\n" +
                ".comhead, .comment, .subtext, a:link, a:hover, a:visited, input {\n" +
                "    font-family: " + FONT_FAMILY + " !important;\n" +
                "    font-size: 11pt;\n" +
                "    line-height: 1.5;\n" +
                "}\n" +
                "         a:link,          a:hover,          a:visited, " +
                ".subline a:link, .subline a:hover, .subline a:visited " +
                ".comment a:link, .comment a:hover, .comment a:visited " +
                ".pagetop a:link, .pagetop a:hover, .pagetop a:visited {\n" +
                "    color: " + (darkMode ? "rgb(142, 193, 217)" : "black") + ";\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                ".title > .comhead, .title > a:link, .title > a:hover, .title > a:visited {\n" +
                "    font-family: " + FONT_FAMILY + " !important;\n" +
                "    font-size: 11pt;\n" +
                "    color: " + (darkMode ? "#c0c0c0" : "rgb(10, 10, 10)") + ";\n" +
                "}\n" +
                ".site-highlight, a.site-highlight:link, a.site-highlight:hover,  a.site-highlight:visited {\n" +
                "    background-color: " + HN_LIGHT_ORANGE + ";\n" +
                "    color: " + (darkMode ? "white" : "black") + ";\n" +
                "}\n" +
                ".highlight, a.highlight:link, a.highlight:hover,  a.highlight:visited {\n" +
                "    background-color: " + HIGHLIGHT_COLOR + ";\n" +
                "}\n" +
                "a.highlight:link, a.highlight:hover,  a.highlight:visited {\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                "a.site-highlight:link, a.site-highlight:hover,  a.site-highlight:visited,\n" +
                "a.follow:link, a.follow:hover,  a.follow:visited,\n" +
                "a.unfollow:link, a.unfollow:hover,  a.unfollow:visited {\n" +
                "    color: " + HN_LIGHT_ORANGE + ";\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                "td.default {\n" +
                "    background-color: " + (darkMode ? "#222222" : "#f9f9f9") + ";\n" +
                "    padding: 7px;\n" +
                "    border-radius: 7px;\n" +
                "    border: 1px solid #f0f0f0;\n" +
                "    width: 100%;\n" +
                "}\n" +
                "td.default i {\n" +
                "    background-color: " + (darkMode ? "#202020" : "#e9e9e9") + ";\n" +
                "}\n" +
                "td.op {\n" +
                "    border-color: " + HN_LIGHT_ORANGE + ";\n" +
                "}\n" +
                ".commtext, .subline, .subtext {\n" +
                "    color: " + (darkMode ? "white" : "black") + ";\n" +
                "    background-color: inherit;\n" +
                "}\n" +
                "td.default.newcomment, td.default.newcomment .commtext {\n" +
                "    background-color: " + HIGHLIGHT_COLOR + ";\n" +
                "}\n" +
                "a.user_follow {\n" +
                "    color: " + HN_DARK_ORANGE + ";\n" +
                "    padding: 0px 3px 0px 3px;\n" +
                "    border-radius: 3px;\n" +
                "}\n" +
                ".comment a[rel*=\"nofollow\"]:not(a[href*=\"reply?id=\"]) {\n" +
                "    color: " + HN_DARK_ORANGE + ";\n" +
                "    background-color: inherit;\n" +
                "}\n" +
                ".comment a[target*=\"_blank\"] {\n" +
                "    color: " + HN_DARK_ORANGE + ";\n" +
                "}\n" +
                "a.togg:hover, a.user_follow:hover, .comment a[rel*=\"nofollow\"]:hover {\n" +
                "    border-radius: 3px;\n" +
                "    background-color: " + HN_DARK_ORANGE + ";\n" +
                "    color: " + (darkMode ? "black" : "white") + ";\n" +
                "    text-decoration: none;\n" +
                "}\n" +
                ".flair, .site-flair {\n" +
                "    font-size: smaller;\n" +
                "    display: inline-block;\n" +
                "    color: " + HN_DARK_ORANGE + ";\n" +
                "    background-color: " + (darkMode ? "#111111" : "#fefefe") + ";\n" +
                "    margin: 0px 5px -5px 5px;\n" +
                "    max-width: 10em;\n" +
                "    overflow: hidden;\n" +
                "    text-overflow: ellipsis;\n" +
                "    white-space: nowrap;\n" +
                "    border: 1px dotted " + HN_DARK_ORANGE + ";\n" +
                "    border-radius: 3px;\n" +
                "    padding: 1px 3px 1px 3px;\n" +
                "}\n" +
                ".topsel a:link, .topsel a:visited {\n" +
                "    background-color: " + HN_LIGHT_ORANGE + ";\n" +
                "}\n" +
                "/* Provide a way to read downvoted text by hovering on it */\n" +
                "span.comment font:hover, span.dead:hover {\n" +
                (darkMode ?
                    "    color: white;\n" +
                    "}\n" +
                    ".newcomment .commtext {\n" +
                    "    color: white;\n" +
                    "    background-color: #333333;\n"
                    :
                    "    color: black;\n") +
                "}\n" +
                ".togg {\n" +
                "    float: left;\n" +
                "    margin-right: 5px;\n" +
                "}\n" +
                ".votearrow {\n" +
                "    margin-bottom: 10px;\n" +
                "}\n" +
                "a.betteridge-no:after {\n" +
                "    color: " + (darkMode ? "#f99" : "#600") + ";\n" +
                "    font-size: 50%;\n" +
                "    text-transform: uppercase;\n" +
                "    letter-spacing: 1px;\n" +
                "    background-color: " + (darkMode ? "black" : "white") + ";\n" +
                "    content: ' Probably not ';\n" +
                " }\n" +
                "a.betteridge-yes:after {\n" +
                "    color: " + (darkMode ? "#006" : "#600") + ";\n" +
                "    font-size: 50%;\n" +
                "    text-transform: uppercase;\n" +
                "    letter-spacing: 1px;\n" +
                "    background-color: " + (darkMode ? "black" : "white") + ";\n" +
                "    content: ' Probably ';\n" +
                " }\n" +
                ".container {\n" +
                "   margin: 0 auto;\n" +
                "   display: inline-block;\n" +
                "}\n" +
                "nav {\n" +
                " display: inline-block;\n" +
                "   word-wrap:break-word !important;\n" +
                "}\n" +
                "nav ul {\n" +
                "   padding: 0;\n" +
                "   margin: 0;\n" +
                "   list-style: none;\n" +
                "   position: relative;\n" +
                "}\n" +
                "nav ul li {\n" + // used by infinite page scroll?
                "   display: inline-block;\n" +
                "    background-color: " + (darkMode ? "red" : "yellow") + ";\n" +
                "}\n" +
                "nav a {\n" +
                "   display: block;\n" +
                "   padding: 0 10px; \n" +
                "   color: " + (darkMode ? "black" : "white") + ";\n" +
                "   text-decoration:none;\n" +
                "}\n" +
                "/* Hide Dropdowns by Default */\n" +
                "nav ul ul {\n" +
                "   display: none;\n" +
                "   position: absolute; \n" +
                //"   top: 60px; /* the height of the main nav */\n" +
                "}\n" +
                "/* Display Dropdowns on Hover */\n" +
                "nav ul li:hover > ul {\n" +
                "   display:inherit;\n" +
                "}\n" +
                "/* First Tier Dropdown */\n" +
                "nav ul ul li {\n" +
                "   width: 270px;\n" +
                "   float: none;\n" +
                "   display: list-item;\n" +
                "   position: relative;\n" +
                "}\n" +
                "/* Second, Third and more Tiers    */\n" +
                "nav ul ul ul li {\n" +
                "   position: relative;\n" +
                "   top:-60px; \n" +
                "   left: 270px;\n" +
                "}\n" +
                "/* Change this in order to change the Dropdown symbol */\n" +
                "li > a:after { content:  ''}\n" +
                "li > a:only-child:after { content: ''; }\n" +
                ".big-button {\n" +
                "    padding: 5pt 10pt 5pt 10pt;\n" +
                "    border-radius: 5pt;\n" +
                "}\n" +
                ".pagetop a:visited, .pagetop a:link, .pagetop a:hover {\n" +
                "    color: " +
                (darkMode ? "white" : "black") +
                "\n" +
                "}" +
                "#cycleCommentsModeButton {\n" +
                "    background-color: grey;\n" +
                "    color: white;\n" +
                "}" +
                ".highlight {\n" +
                "    background-color: " + HIGHLIGHT_COLOR + ";\n" +
                "}",
            );
            $('table table').attr('cellpadding', '5');
            $('img[width=18][height=18]').hide();
            if (document.getElementById("morelinks_menu")) return;
            $morelinks.init();
        } catch (e) {
            console.log(e);
        }
    }

    function isMainPage() {
        var url = window.location.href;

        return (
            /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)(#|$|\/?$|\?$)/.exec(url) ||
            /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)\/from\?site=/.exec(url) ||
            /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)\/(active|ask|asknew|best|classic|front|from|hidden|invited|newest|news\d*|noobstories|over|pool|show|shownew|x)(#|$|\/|\?)/.exec(url) ||
            /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)\/submitted\?id=.+/.exec(url)
        );
    }

    function isPaginatedPage() {
        return (isMainPage() || isCommentsPage() || /(ycombinator\.com|hackerne\.ws)\/((best|new)comments|threads)/.exec(window.location.href));
    }

    function isCommentsPage() {
        return /(ycombinator\.com|hackerne\.ws)\/item|threads|(best|new|noob)comments|highlights/.exec(
            window.location.href);
    }

    function isFlatCommentsPage() {
        return /\b(new|best)comments\b/.exec(document.location.href);
    }

    function isLeadersPage() {
        return /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)\/leaders(\/|\?|#|$)/.exec(
            window.location.href);
    }

    function isProfilePage() {
        return /^https?:\/\/(news\.ycombinator\.com|hackerne\.ws)\/user\?id=.+(\/|\?|#|$)/.exec(
            window.location.href);
    }

    function work(doc) {
        try {
            doc = doc || document;
            newTabifyLinks(doc);

            if (isMainPage()) {
                doMainPage(doc);
            } else if (isCommentsPage()) {
                doCommentsPage(doc); // no doc here since we scan every time
                $('table:first', doc).width('100%');
            } else if (isLeadersPage()) {
                doLeadersPage();
            } else if (isProfilePage()) {
                doProfilePage();
            } else {
                console.log("Unrecognized hackernews page type");
            }
        } catch (e) {
            console.log(e);
        }
    }

    function page_insert_event_handler(e) {
        work(e.target);
    }

    // NOTE: Using weAutoPagerize
    window.addEventListener("AutoPatchWork.DOMNodeInserted", page_insert_event_handler, false);
    window.addEventListener("AutoPagerize_DOMNodeInserted", page_insert_event_handler, false);
    window.addEventListener("AutoPagerAfterInsert", page_insert_event_handler, false);
    if (!throttled()) {
        doOneTime();
        work();
    }
})();
console.log("hackernews.user.js ended");
// vim: set et fdm=indent fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=0 nowrap :
