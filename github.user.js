// ==UserScript==
// @id          github
// @name        GitHub Enhancements
// @namespace   junkblocker
// @author      Manpreet Singh <junkblocker@yahoo.com>
// @description GitHub Enhancements
// @icon        https://github.com/favicon.ico
// @include     https://github.tld/*
// @match       https://github.tld/*
// @version     10
// @grant       none
// @run-at      document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

'use strict';

// Default fallback: duckduckgo -> bing -> algolia -> github -> google | ZipRecruiterWiki
console.log("github.user.js starting");
try {
    (function() {
        ['container', 'container-lg', 'container-xl'].forEach(function(name) {
            try {
                var container = document.getElementsByClassName(name);
                for (var i = 0, l = container.length; i < l; i++) {
                    container[i].style.width = '95%';
                    container[i].style.maxWidth = '95%';
                }
            } catch (e) {
                console.log(e);
            }

        });

        if (!(/^https:\/\/(gist\.)?github\.[a-z0-9.]+\/([^/]+\/[^/]+\/)?search/).test(document.location.href)) return;

        var original_url = window.location.href;
        var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
            var f = scope ? function() {
                eventHandler.apply(scope, arguments);
            } : eventHandler;
            if (target.addEventListener) {
                target.addEventListener(eventName, f, true);
            } else if (target.attachEvent) {
                target.attachEvent('on' + eventName, f);
            }
            return f;
        };

        function gourl(url, aftertime) {
            setTimeout(function() {
                if (window.location.href != original_url) {
                    console.log("User moved away from", original_url, "to", window.location.href);
                    return; // user navigated away in a hurry
                }
                window.location.replace(url);
            }, aftertime || 50);
        }

        function query_s() {
            var ret = '';
            try {
                if (document.forms.search_form && document.forms.search_form.q) {
                    ret = document.forms.search_form.q.value;
                    search_kind = '';
                } else if (/https:\/\/github.com\/([^/]+\/[^/]+\/)?search\?.*?q=([^&;]+)/.test(document.location.href)) {
                    ret = RegExp.$2;
                }
                return ret;
            } catch (e) {
                return '';
            }
        }

        function duck_s() {
            return 'https://duckduckgo.com/html/?q=' + query_s() + '&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        }

        function google_s(kind) {
            if (kind && kind === 'pics') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=isch' + '&&tbs=li:1';
            } else if (kind && kind === 'video') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=vid' + '&&tbs=li:1';
            } else {
                return 'https://www.google.com/search?q=' + query_s();
            }
        }

        function spage_s() {
            return 'https://startpage.com/do/search/?query=' + query_s() + '&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        }

        function github_s() {
            return 'https://github.com/search?q=' + query_s();
        }

        function github_kind_s(kind) {
            kind = kind || 'Repositories';
            if (/github.com\/([a-zA-Z0-9._-]+\/[a-zA-Z0-9._-]+\/search)/.test(document.location.href)) {
                return 'https://github.com/' + RegExp.$1 + '?q=' + query_s() + '&type=' + kind;
            } else {
                return 'https://github.com/search?q=' + query_s() + '&type=' + kind;
            }
        }

        function isRepoSearch() {
            return /github.com\/([a-zA-Z0-9._-]+\/[a-zA-Z0-9._-]+)\/search/.test(document.location.href);
        }

        function zrw_s() {
            return 'https://wiki.zr.org/_search?patterns=' + query_s() + '&search=Search';
        }

        function algolia_s() {
            return 'https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=' + query_s() + '&sort=byPopularity&type=all';
        }

        function bing_s() {
            return 'https://www.bing.com/search?q=' + query_s() + '&pc=MOZIo';
        }

        function makeButton(label, query_s_func) {
            var button_id = 'my_' + label;
            if (document.getElementById(button_id)) return;
            var button = document.createElement('button');
            button.id = 'my_' + label;
            button.textContent = label;
            addEventHandler(button, 'click', function() {
                window.location.replace(query_s_func());
                return false;
            });
            return button;
        }

        function work(after) {
            var search_kind;
            after = after || 0;

            var results = document.getElementsByClassName('blankslate')[0];
            if (results) {
                if (/We couldn.t find any repositories matching/.test(results.innerHTML)) {
                    console.log('Should trigger a code search');
                    gourl(github_kind_s('code'));
                    return;
                } else if (/We couldn.t find any code matching/.test(results.innerHTML) || /type=code/.test(document.location.href)) {
                    console.log('Should trigger a Commits search');
                    gourl(github_kind_s('Commits'));
                    return;
                } else if (/We couldn.t find any commits matching/.test(results.innerHTML)) {
                    console.log('Should trigger an Issues search');
                    gourl(github_kind_s('Issues'));
                    return;
                } else if (/We couldn.t find any issues matching/.test(results.innerHTML)) {
                    console.log('Should trigger a Discussions search');
                    gourl(github_kind_s('Discussions'));
                    return;
                } else if (/We couldn.t find any discussions matching/.test(results.innerHTML)) {
                    console.log('Should trigger a Wiki search');
                    gourl(github_kind_s('Wikis'));
                    return;
                } else if (/We couldn.t find any wiki pages matching/.test(results.innerHTML)) {
                    if (isRepoSearch()) {
                        // Exhausted our options
                        console.log('Should trigger a general github search');
                        gourl('https://github.com/search?langOverride=&q=' + query_s());
                    } else {
                        console.log('Should trigger a Users search');
                        gourl(github_kind_s('Users'));
                    }
                    return;
                } else if (/We couldn.t find any users matching/.test(results.innerHTML)) {
                    if (isRepoSearch()) {
                        console.log('Should trigger a general github search');
                        gourl('https://github.com/search?langOverride=&q=' + query_s());
                    } else {
                        console.log("Redirecting to algolia search");
                        gourl(algolia_s());
                    }
                    return;
                }
            } else {
                results = document.forms.search_form;
                if (results) {
                    if (!document.getElementById('junkblocker_search_div')) {
                        var div = document.createElement('div');
                        div.id = 'junkblocker_search_div';
                        div.setAttribute('align', 'center');

                        div.appendChild(makeButton('Algolia', algolia_s));
                        div.appendChild(makeButton('Bing', bing_s));
                        div.appendChild(makeButton('DuckDuckGo', duck_s(search_kind)));
                        div.appendChild(makeButton('Google', google_s(search_kind)));
                        div.appendChild(makeButton('ZRW', zrw_s(search_kind)));

                        results.parentNode.insertBefore(div, results);
                    }
                    return;
                }
            }
            // force it to rerun in case of AJAX crap
            setTimeout(work, after + 1000);
        }
        setTimeout(work, 2000);
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("github.user.js ended ");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 nowrap :
