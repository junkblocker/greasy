// ==UserScript==
// @id             imdb
// @name           imdb enhancements
// @version        6
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    imdb enhancements
// @include        *://*.imdb.tld/*
// @require        https://html2canvas.hertzen.com/dist/html2canvas.js
// @require        https://raw.githubusercontent.com/eligrey/FileSaver.js/master/src/FileSaver.js
// @grant          GM_setClipboard
// @run-at         document-end
// @icon           data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAQCAYAAAB3AH1ZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABgdJREFUeNokVVtvXFcZXXufc+aWudszY8eO7ZjGaRIFCvSWcGtUISFKVbW0D7xFfeAFiR8ASFxUCREJ+oKE0pcqFFEpKiCoEGnVi0JV6ENERIRpQ5P4Eo89Hntsz/1c9oV1jmd0dM7e++zvrG9961tb9LZ/i2xmDmbwzg+VXjk7Cgu+NcKRiISwgIW0xgoYA2GEssI6sNqxEL6AdVwIKwGjjXKNNVIqaaQWBmklYa2BRgBAaDCGq6wbuX5KVs6/ns2dveqPtuFGZhJGffwVJ7r6Ume7iZRTZswBtAngmAoURjCICCANazJQdsiAiiGzcAhGC8V3oxgdwWYQGQkJwbkebJSCcTwoAiJYqCjOaIAptL7a319/c+TWx64J3/oBgvd/tLc1QF4uoXaiChQbgNYwrTXImRl+0AHCCKP1JnIzXMtmgaHGYPMO8o1ZLldIwgET5baDHXQ3NlFeOg/p1XFwbxudwToymQy8dB2tlSb21u5U07X+h7L4zZ+Kzu3TLXTbDTiLqC5k8K/lPC6/OsT8dAHPPFPA1avb2OlozM0BL35nFq/9bhcrGwHKkxrPPXsK7/xtHRutEZkJiCuN576Vw0MPZvGL3/Sw0gzxvedLePxcGyurI/z3ZgcTU1Wk0xKpaBOp/Jk7ro3q9yGdRrZImo5I/Oe2wiuvXkclX8O580/h5y9/QFojeJksHvnCY3jp5Ws4GHXgIY8LTzyGX77yb7R3tnD4k7jyehXX/niRwP+E26t38fVzL+IznRz6nX34eoDQj1CfqHCch6vdTakjSkYaDIMxMFAoZFwGymBupoIMhbO4UIAQVUxWS7hxq4VK7QgpL2BxrsR1jdqUJLU1XPrJRZw9tYjO3h6Wb+3h1Jk6pMzhV5ev48yXP8T3f7yB+aUHsHQsC7/Xh6bMQ6pbRtQTVc8aeiygAMdJJkJIxI+UDbyUgfIN/vHRJkY+x9Q2ZYaIAKk0CAr9geMTKBeyyQ7PyzO4YheMcOLksST09Rt3cWclQJrPivqKv2E0HGktNUr2E4UqAohMsohEyRG0UigVi6iWJ/H+ux8hn/cwMTmBMFRJpwh2hvB8dIe78EOb7LPsGh2w8zh84dkv4XMnSocxnTTGERtcx8C5njCglbRspVDzCiwcRcUzO2ND3lwMfB/TMzUcpwr7wQALszNoTM2iz64gegZhySyz5T4hYvZcMmfgxazyudPuYRxbAXlzlEfWQkIkh5ogjQilI6sDTeSGVxhFSGIwAO0Emh4T0lDcbAaN2XyyUmf26SMK8V8rwyzpSdJDNp3jPUwYMAmZMsk6KzlwDotpIoGANY5L4KU8DHomJQUczQSgQgmCAkEd9j2NI6JpOCxUhtkdrcsEQHUigGvHSUyrIqSpAQ85fHy3hb1+PzGpcEhmXAZDiLEvE4OK54dkUwQOkyVwCiPk0KVdUnGKLRLXrohRP67jCNtbEYJhEbsHEXKbeyiXTyYAcvlJrK19it2ej6EpYbcbsRy7+NmlN5Msi9kiJhoONprd5P1Lv34Lq/ebJMZDucTYgYDi5eXJHO1TLL99cn2ypI71dRnaH2JrZxp//jtQKzt48uEi/nK9hUKhgEfP1vDeB6t49JEGPrnt46C7j2987Sj+eXOMZneHmqLTsYyfPW3xxVNF/PVdCpMkFHICN5abePj0Ap58vIfB3iaKpQmksm2sr+bXxM33nn4jFd76dq1eRa8boMc+m58/zuy7ZGEL88fmMAp87O/2MdWglbZbKBYrSKVcBtikJqaRSqd4NuiEgf1BD+12F9PTR+kPLobDAdfzCMYK97fuo16fRI7l2WgfwK1c+L3Yar6Nzsq1746bVy7PLRyH7w9Yez/WIeufQXyexSKJvcLqeJK1451V5mHEOfawTE5IipKCNE6GK/F8wAOMb2kLN95rPaRc3l0f92ichaknLs4sXrgiNu69Bk9ZrP7vjT8ovfogazxme3Bb7FDwBKRRRluH32S/Sm0OP0ypS5+vGemY+LCLz+Kk8SxHbO3YqmIzi70g4h66v6Hjyf397axX/vzy0kPPvxAM2vi/AAMASJUzNk9kVxUAAAAASUVORK5CYII=
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es2019, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log('imdb.user.js starting');

var one, three, four;

// ========== FEATURE 1 - Fix Movie URL ==========
// tt\d{7} is the correct movie code format and imdb barfs if it doesn't
// not follow that format because preceeding 0s are removed.
// So add those automatically if coming from a URL with those stripped.
//
if (/^(https?:\/\/([a-zA-Z0-9_]+\.)?imdb\.com\/title\/tt)([0-9]{1,6})\b(.*$)/.test(window.location.href)) {
    one = RegExp.$1;
    three = RegExp.$3;
    four = RegExp.$4;
    // https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
    if (!String.prototype.padStart) {
        String.prototype.padStart = function padStart(targetLength, padString) {
            targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
            padString = String(typeof padString !== 'undefined' ? padString : ' ');
            if (this.length >= targetLength) {
                return String(this);
            }
            targetLength = targetLength - this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + String(this);
        };
    }
    var newurl = one + three.padStart(7, '0') + four;
    window.location.replace(newurl);
}
// ========== Feature 2 ==========
// Show full generes list on top. Clicking it copies them to the clipboard.
//
var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
    var f;
    try {
        f = scope ? function() {
            eventHandler.apply(scope, arguments);
        } : eventHandler;
        if (target.addEventListener) {
            target.addEventListener(eventName, f, true);
        } else if (target.attachEvent) {
            target.attachEvent('on' + eventName, f);
        }
    } catch (e) {
        console.log(e);
    }
    return f;
};

setTimeout(function() {
    document.querySelectorAll('td').forEach(function(i) {
        if (i.innerHTML == "Genres") {
            try {
                var text = "[" + i.nextElementSibling.innerText.replace(/([a-z])\s+([A-Z])/g, '$1,$2', '') + "]"
                GM_setClipboard(text);
                alert("Copied " + text + " to clipboard");
            } catch (e) {
                console.log(e);
            }
        }
    }, 1000);
});
console.log('imdb.user.js ended');
// vim: set et fdm=indent fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
