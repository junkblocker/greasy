// ==UserScript==
// @name         blah
// @namespace    http://tampermonkey.net/
// @version      7
// @description  try to take over the world!
// @author       You
// @match        *
// @include      *
// @grant        none
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es2019, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */
/* eslint-disable no-prototype-builtins */

// NOTE echo 'thing' DENC on the command line to get stuff rot47 | rev'ed

(function() {
    'use strict';

    var href = window.location.href;

    function reverseString(str) {
        return str.split('').reverse().join('');
    }

    function rot47(a, b) {
        return ++b ?
            String.fromCharCode(((a = a.charCodeAt() + 47), a > 126 ? a - 94 : a)) :
            a.replace(/[^ ]/g, rot47);
    }

    function denc(something) {
        return reverseString(rot47(something));
    }

    // @match       https://*.signin.aws.amazon.com/oauth*
    // @match       https://signin.aws.amazon.com/signin*
    // @match       https://aws.amazon.com/console/
    // @match       https://signin.aws.amazon.com/saml
    if (/^https:\/\/[^\/]*\.signin\.aws\.amazon\.com\/oauth.*/.test(href) ||
        /^https:\/\/signin\.aws\.amazon\.com\/signin.*/.test(href) ||
        /^https:\/\/aws\.amazon\.com\/console/.test(href) ||
        /^https:\/\/signin\.aws\.amazon\.com\/smal$/.test(href)) {
        if (/saml$/.test(window.location.href)) {
            if (/did not include a SAML/.test(document.body.innerHTML)) {
                window.location.href = 'https://accounts.google.com/o/saml2/initsso?idpid=C0' + '1pghowc&spid=5785537' + '01671&forceauthn=false';
            }
        } else if (/Session Expired|Amazon Web Services Sign-In|AWS Signin|AWS Management Console/.test(window.document.title)) {
            window.location.href = 'https://accounts.google.com/o/saml2/initsso?idpid=C' + '01pghowc&spid=578' + '553701671&forceauthn=false';
        }
        return;
    }
    athis = denc('>@4]C6E:FC46CA:K]HHH');
    console.log(athis);
    if (athis == window.location.host) { //.com debug
        window.zr = window.zr || {};
        window.zr.debug_default_on = true;

        var scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        athat = denc('D;]C688F365]CK^CK^D;^');
        scriptElement.src = athat;
        document.body.appendChild(scriptElement);
        return;
    }
    var wikr = 'SXZ]WuaTpbTnY]l6E2EDY]<423==24^-a9EF2@^-8C@]-CK]-:<:H^-^-iDAEE9';
    console.log(wikr, denc(wikr));
    athis = new RegExp(denc(wikr));
    var godr = 'SXY]WuaTpbTY]l6E2EDnY]<423==24^-a9EF2@^-8C@]-CK]-4@5@8^-^-iDAEE9';
    athat = new RegExp(denc(godr));
    console.log(godr, denc(godr));
    if (athis.test(href) || athat.test(href)) {
        console.log("Reloading");
        // Autoload wiki pages when browser is re/started and old url + oauth is incompatible
        athis = "^8C@]CK]:<:H^^iDAEE9";
        console.log(athis);
        athat = denc(athis);
        console.log(athat);
        var wikipage = athat + decodeURIComponent(RegExp.$1);

        console.log('Sign In');
        console.log(athis);
        if (/Sign In/.test(document.body.innerText)) {
            String.prototype.hashCode = function() {
                var hash = 0,
                    i, chr;
                if (this.length === 0) return hash;
                for (i = 0; i < this.length; i++) {
                    chr = this.charCodeAt(i);
                    hash = ((hash << 5) - hash) + chr;
                    hash |= 0; // Convert to 32bit integer
                }
                return hash;
            };
            var delay = wikipage.hashCode() % 30000 + 1000;
            if (delay < 0) delay = -delay;
            setTimeout(function() {
                window.location.replace(wikipage);
            }, delay);
            return;
        }
    }
    // godoc redirection from IDE when not an oauth issue
    var athis = denc('SXY]8C@]-CK]-@8^-WG65]-@8]-8<A^-^-iDAEE9/');
    console.log(athis);
    var athat = denc('8<A^8C@]CK]4@5@8^^iDAEE9');
    console.log(athat);
    var r = RegExp(athis);
    console.log(r);
    if (r.test(href)) {
        window.location.replace(athat) + RegExp.$1;
        return;
    }
})();
