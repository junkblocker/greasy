﻿// ==UserScript==
// @id             Highlight_Non_HTTPS_Elements
// @name           Highlight Non HTTPS Elements
// @version        3
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Highlight non-HTTPS elements in the page
// @icon           data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAA4WlDQ1BzUkdCAAAYlWNgYDzNAARMDgwMuXklRUHuTgoRkVEKDEggMbm4gAE3YGRg+HYNRDIwXNYNLGHlx6MWG+AsAloIpD8AsUg6mM3IAmInQdgSIHZ5SUEJkK0DYicXFIHYQBcz8BSFBDkD2T5AtkI6EjsJiZ2SWpwMZOcA2fEIv+XPZ2Cw+MLAwDwRIZY0jYFhezsDg8QdhJjKQgYG/lYGhm2XEWKf/cH+ZRQ7VJJaUQIS8dN3ZChILEoESzODAjQtjYHh03IGBt5IBgbhCwwMXNEQd4ABazEwoEkMJ0IAAHLYNoSjH0ezAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAH9ElEQVRYhaWXS4wcRxnHf1XV857ZmZ2d8c4+7H14bONNYhvH8cZKDE6UOAmOhEhkBAIRDsgHhJQLEnBAQQghhMQJDsABcYAcEgkOJDwilATFSoQck1iOjdfx2mvH3l3v07szO9PT3VUfh1k7Edq110pJ3aWuavX3q//3qGrFBtpzzxw5ttxo/KTRCorWWi0izok1TkCcQ8QhAog4BSel6b5y/PTpixv5trrd5OHDuzLaZn/trPuac06JCCKCE1k13H5GoH0TRCBybt73w6feP3P+xJ0A9O3mwrp5KfD9r4dBS9kwxEYhURThwggbRVhrcdbhnEWJ44HhCnu3dmOQrlTce2VXtdp/JwCz3sTBfbueV4rvbO7MKmstfhAiTnDOtRVYVQERYgqOPnwPD44MsL2/zFJ9hanFesZ4an8q1/niwsKCvSuA4eHhvBb7p/7OTPqrj+xmqKeTi9fm8MOIKHIoQBCcEzTC/movB+4dQmuNUorhni4uTc1xo9HaYpDG9OzC8XVlXmvQBv7R6bmF4udHBkglYvQVO3h8z1a29RZJeopkXIMIBuGRe7fwyN4qSqnVgBLinmZ05xZEBIv0rGd8XQC/1fpyNuGx4vvtIAN2DlY4sn8H1Z4iuwe60UqxdVMnD++ukkol2prIx9dKw0cEjKLjbgGUdW6k2YqYmJy/5WcF5DMpjhwY4cDIFqq9BR4c2Uzc81DtBFjNiHZW5NMJFEImlTx89Oj6sbYWgIjIWT+MuDS1QBhENwdBhI50gq5CjmcP7mL7YGU1kd2qUnKr7yvlqRSz2Mj2XL+yc/huABB4K7KOZhgyu1j7P7p2n00l0VqDgBMIw5BWq50pIpBNJ9nW24WAbCmU6ncFoBRnYsaggHQqjnWWVhDitwLCMGoXodXLOku93sSJcGVmkdPj15hbXCaMLP39PZRLxflqJfujH3zjyd+vZctba9CY+ClDKOV8WnXkUizVmyQTcRIxj6YfsFwLyBdyOOc48d/LXPhojumlFWxkGeot8e6lGVqiuLESsq3SWSoXM8f+efL8uTUXu54yfeWu6f1be8rdpRwnxq7iB4543LCpkMEPLPPLdRZrPjU/xK36RRmNiXkkUwly2SxGaY7uHWCuXqfSVXjjhd/99dENKfDNw6N/f+vs5czrZy5hrbSr/GpwjV39mFlpTSzmEbXC9rMI4qDpBFB0pNP0lDqYml9iz7a+PWuudK3B0+OTk0EQpsMowolFxLG643Bz0wHBGEO50o1aZWqXZ0sURjTqTZKeJptNoo3CiVvcMIBWxBKepu0h9QnjHzelFJlMklwuQyweb0OodolGHC6ydHekaQURg91FVvzgz2vZWtMFYeRmMokYSnGrEn7CNEopNhdzvHDsCFfna7wes1y4Ms18rUnUrlsoBZVChhtLddKpOB8tLL6yYYCG77/YkU097xmjwsjeWrFWilQ8RqUzzdCmTkr5LN3FDvZW+3DOUW80mZpbYmp+meuLdYa6i2jl2NJbuvbG8Tff2TDA+en5E3uH+17NJuJP34h8PKP53H1DPHVghGuzi7x1apx4zPtECgkijnw2Qz6bYcdADyLCmfFrFDLJqzMLS0/88m8XWhsGANAi392UTx/qyqWyD40M8Mzj+whCyxv/GWNTPsv+e4faPhdBnLC03KCYz4JuY1lxdOXSZHKp9577/h/PrmtnvYl3L02O1RrhswnPayegc5w8N8Hc0grLKwGzi7X2acg6ZuZvtCvkKow4R7MZ0FXIoUStuxHdVgGAC1Mzr+3btnn+7MT10sj4FB+MT+IctAh5++wEoKmUOphdWEYbw9WZJXZt68MY3T49ibvd5+8MAGCUfr8ZhI+9/OYpUI6aH3JtoU4YOSZm6+RzaUpdnXTkcox/eJqDV6/zpUf3YoxGa42z9sqnAoicvB339GN+EGKM4qPZGsV8hj33bOXb33qOmFJEQYugWWNmYoyYdogTYkZjrePy9cV/fCqAmh+8Wswmfmi0UtYJ1UqBTCrOF/cO0hh/FwSsCFFkKXckgXZgRpHj4tQ8r/17bM3ov9lu+19w8537tw28EzNqFNr1wBhNImHIJuJEztFdyDK6c4D+SvFW4Vpe8Vmq+cQ7S43ywPbLfiuctZGdAZlu+M1z42MX3/npb//w3kYA2NFXORqPm5fScQ+tVfsgYhR9xRwHRgbY2l9un4gB6xz1FZ9UMoHWmkDHqewcpVZbIZvLErQCpq/PsLm/h0ajObkhgM/09W03CcZwQjoeQ5u2CofuGyTmGdKpOMVshlIhS7MVkoh7xOMxAKwoXjs1iR8ZtNb09pQ5dHAUrTXpdGZDLuDQ4GDyRszWWqH10jEDSuFHEeJgbmmFwDo6M0n2DFd4Yt8OesoFUAqlQUQxNnmDD66uoJShOtzvmn6gPc9w4IHP3jkIf/Oz7+WrW6sv//wXv/ImpmfwjIcVx9RCHT+IUEpjtKLWCmg0Q8qdOZy1tKyj3gjoKmSpVgpYZYjne9mz+x6dTqW4+R95W4Bjx47FBgcG//Lh+YmDrVZE3BisCEZrhro7mVyoEUSOrmyKJ++v8tB9w4BirtbkX2cu01fuIlnsJttZltEd3SqZTAMKJ0Kr5TM7t3h7gNFq+cfHj588eH78MkFkSXoe1jmMNsQ8w1ClSEc6wZEHdtBTyqOUlkXfuSmbNl848jT9fX0Y48FqtomIhFGozp2/yNiHFyl25vkfbhHvPFQ2rOsAAAAASUVORK5CYII=
// @include        http://*
// @include        https://*
// @grant          none
// @run-at         document-end
// ==/UserScript==

// From original scripts by Adam Katz and Tod Beardsley

try {
  console.log("Highlight_Non_HTTPS_Elements.user.js starting");
} catch (safe_wrap_top) {};
try {
  (function() {

    var ssl_bg = "#cfb"; // green background for SSL-protected password fields
    var plain_bg = "#fcb"; // red background for non-SSL-protected password fields

    var rel_ssl = '',
      rel_plain = '';
    var pw_field = 'input[type="password"]';

    function addStyle(content) {
      var style = document.createElement("style");
      style.type = "text/css";
      style.appendChild(document.createTextNode(content));
      var head = document.getElementsByTagName("head");
      head && head[0] ? head = head[0] : head = document.body;
      head.appendChild(style);
    }

    function pwcss(sel, rgb) {
      return sel + pw_field + ' { color:#000; background:' + rgb + '!important }\n';
    }

    if (location.protocol == "https:") {
      rel_ssl = pw_field + ", ";
    } else {
      rel_plain = pw_field + ", ";
    }

    addStyle(
      pwcss(rel_ssl + 'form[action^="https://"] ', ssl_bg) +
      pwcss(rel_plain + 'form[action^="http://"] ', plain_bg)
    );

    var forms = document.getElementsByTagName("form");

    // For each form, on each password field, note the domain it submits to
    // (unless it's the same domain as the current page).  TODO: strip subdomains?
    for (var f = 0, fl = forms.length; f < fl; f++) {
      var target;
      if (!forms[f].action || !forms[f].action.match) {
        // defaults for forms without actions -> assume JavaScript
        target = [(location.protocol == "https:"), "javascript"];
      } else {
        target = forms[f].action.match(/^http(s?):..([^\/]+)/i);
      }

      var pws = document.evaluate("//input[@type='password']", forms[f], null,
        XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);

      if (!pws || !target || !target[2]) {
        continue;
      }

      // Report when domain doesn't match
      var is_secure = " will be sent to <" + target[2] + ">";
      if (location.host == target[2]) {
        is_secure = "";
      }

      if (target[2].match(/^javascript(?![^:])/)) {
        is_secure = "UNKNOWN SECURITY, password to be sent via " + target[2];
      } else if (target[1]) {
        is_secure = "SSL secured password" + is_secure;
      } else {
        is_secure = "INSECURE password" + is_secure;
      }

      for (var p = 0, pl = pws.snapshotLength; p < pl; p++) {
        var field = pws.snapshotItem(p);

        // target is SSL, same host, and already has a rollover title -> never mind
        if (target[1] && target[2] == location.host && field.title) {
          continue;
        }

        // rollover text gets security notice plus previous title on newline
        field.title = is_secure + (field.title ? "\n" + field.title : "");
      }
    }

    if (location.protocol == 'https') {
      window.setTimeout(
        function nonHTTPS() {
          srcElements = document.evaluate('//*[translate(@src, "HTP", "htp")]', document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
            //console.log(srcElements.snapshotLength + " elements found.");

            for (var i = 0; i < srcElements.snapshotLength; i++) {
              thisSrc = srcElements.snapshotItem(i);
              //console.log("Element: " + thisSrc.src + "has: " + thisSrc.src.search('http://'));
                if (thisSrc.src.search('http://') == 0) {
                  thisSrc.title = thisSrc.nodeName + ": comes from " + thisSrc.src;
                  thisSrc.style.border = "medium dotted red";
                  thisSrc.style.backgroundColor = "black";
                  thisSrc.style.color = "green";
                  thisSrc.style.zIndex = 65536;
                  thisSrc.style.visibility = "visible";
                  if (thisSrc.offsetWidth < 10) {
                    thisSrc.style.width = "10px";
                  }
                  if (thisSrc.offsetHeight < 10) {
                    thisSrc.style.height = "10px";
                  }
                }
            }
            return false;
        }, 300);
    }
  })();
} catch (safe_wrap_bottom_1) {
  try {
    console.log(safe_wrap_bottom_1);
  } catch (safe_wrap_bottom_2) {}
};
try {
  console.log("Highlight_Non_HTTPS_Elements.user.js ended");
} catch (safe_wrap_bottom_3) {};
// vim: set et fdm=indent fenc=utf-8 ff=unix ft=javascript sts=0 sw=2 ts=2 tw=79 nowrap :
