// ==UserScript==
// @id          lobste_rs_site_improvements
// @name        lobste.rs site improvements
// @namespace   junkblocker
// @description lobste.rs site improvements
// @version     6
// @author      Manpreet Singh <junkblocker@yahoo.com>
// @icon        https://lobste.rs/favicon.ico
// @require     https://code.jquery.com/jquery-3.7.1.min.js
// @include     *://lobste.rs*
// @grant       none
// @run-at      document-end
// ==/UserScript==

// lobste.rs source code is at https://github.com/jcs/lobsters

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */
'use strict';

console.log('lobste.rs.user.js starting');

const HIGHLIGHT_COLOR = 'rgb(221, 255, 221)';

function getInnermostHovered() {
    var n = document.querySelector(":hover");
    var nn;
    while (n) {
        nn = n;
        n = nn.querySelector(":hover");
    }
    return nn;
}

function delayedTooltip(on) {
    try {
        if (on != getInnermostHovered()) return;

        var $this = $(on);
        $.get($this.attr('href'), function(data, status) {
            if (status != 'success') {
                alert(status);
                return;
            }

            var content_html = $(data).find('div.box').html();
            if (!content_html) return;

            $hover_container.hide().width('800px').height('auto').css('top', $this.offset().top + 16 + 'px').css('left', $this.offset().left + 16 + 'px').html(content_html).css('padding', '10px 10px 20px 10px').fadeIn('medium').find('a').attr('target', '_blank');
        });
    } catch (e) {
        console.log(e);
    }
}

function doOneTime() {
    try {
        // Send you to login page for lobste.rs
        var loginLink = document.querySelector('.headerlinks a[href="/login"]');
        if (loginLink && loginLink.innerText == 'Login' && !/\/login/.test(location.href)) {
            location.href = 'https://lobste.rs/login';
        }
        $hover_container = $('<div id="hover_container" style="position:absolute;left:0px;top:0px;display:none;width:100px;height:100px;border-radius:10px;z-index:100;background: rgba(240,240,240,0.95);border: 1px solid #ccc;box-shadow:3px 3px 5px rgba(0,0,0,0.3);padding:10px 10px 20px 10px;"></div>');
        $('body').append($hover_container);
        $(document).on('mouseleave', '#hover_container', function() {
            $hover_container.hide();
            return true;
        });
        $(document).on('keydown', function(event) {
            try {
                var code = event.which || event.keyCode;
                if (code == 27) { // Escape key
                    if ($hover_container.is(':visible')) $hover_container.hide();
                }
            } catch (kde) {
                console.log(kde);
            }
            return true;
        });

        $(document).on('mouseenter', 'a[href*="/u/"]', function() {
            setTimeout(delayedTooltip, 1000, this);
            return true;
        });
    } catch (c) {

    }
}

function work() {
    // Open links and comments in new tabs
    $('a.u-url, span.comments_label > a, a.domain').each(function() {
        $(this).attr('target', '_blank');
        // Optionally, add rel="noopener noreferrer" for security
        $(this).attr('rel', 'noopener noreferrer');
    });
    // set background of unread comments
    $('span.comment_unread').closest('div.details').attr('style', 'background: ' + HIGHLIGHT_COLOR);
}

doOneTime();
work();

// Use MutationObserver to check for new links added dynamically
var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        // Check if any new nodes were added
        if (mutation.addedNodes.length > 0) {
            // Run the function again to catch new links
            work();
        }
    });
});

// Observe the document for changes
observer.observe(document, {
    childList: true,
    subtree: true
});

console.log('lobste.rs.user.js ended');
// vim: set et fdm=indent fenc=utf-8 ff=unix ft=javascript sts=0 sw=2 ts=2 tw=79 nowrap :
