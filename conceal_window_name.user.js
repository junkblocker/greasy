// ==UserScript==
// @name        Conceal window.name
// @description Intercepts read access to window.name property.
// @namespace   localhost
// @match       *://*
// @exclude     *.google.tld
// @run-at      document-start
// @version     2
// @grant       none
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log('conceal_window_name.user.js starting');
try {
    (function() {
        var _window = {
            name: window.name
        };
        Object.defineProperty(window, 'name', {
            get: function() {
                //No CAPTCHA reCAPTCHA
                if (/^https:\/\/www\.google\.com\/recaptcha\/api2\/(?:anchor|frame)\?.+$/.test(window.location.href) && /^I[0-1]_[1-9][0-9]+$/.test(_window.name)) {
                    return _window.name;
                } else {
                    if (_window.name != '') {
                        console.warn('Intercepted read access to window.name "' + _window.name + '" from ' + window.location);
                    }
                    return '';
                }
            }
        });
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("conceal_window_name.user.js ended");
