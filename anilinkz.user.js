// ==UserScript==
// @id             anilinkz
// @name           anilinkz fixup
// @version        6
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    anilinkz.com fixup
// @icon           https://aniwatcher.com/favicon.png
// @include        *://anilinkz.tld/*
// @include        *://aniwatcher.tld/*
// @grant          GM_addStyle
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log('anilinkz.user.js starting');
try {
    (function() {
        var doVisualUpdates = true;

        var hidden;
        if (typeof window.document.hidden !== 'undefined') {
            hidden = 'hidden';
        } else if (typeof window.document.msHidden !== 'undefined') {
            hidden = 'msHidden';
        } else if (typeof window.document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
        }

        if (hidden) {
            doVisualUpdates = !window.document[hidden];

            document.addEventListener('visibilitychange', function() {
                doVisualUpdates = !window.document[hidden];
                if (doVisualUpdates) {
                    setTimeout(blockAds, 100, 100);
                }
            });
        }

        var PreferredSourcePicker = {
            scores: {
                // Has HTML5 player
                'myStream': 10,
                'UpCr2': 10,
                'UpCr4': 10,
                '4up': 10,
                'MixD': 9,
                'AuEn': 9, // Can't start at arbitrary position
                'rVid': 9, // takes forever to start
                'Unli': 9,
                'v44': 9, // fast
                'VidCr2': 10,
                'VidCr4': 10,
                'vStream': 9, // Seekable, fast
                'vNest': 9, // ad free, slow
                'STape': 9,
                'sMoe': 8, // ad free, slow

                'eStream': 7, // seekable, fast
                'PlayP': 6, // Flash Only :(
                'PlayP2': 6, // Flash Only :(
                'Daily': 6,
                'Vous': 6,
                'gDrive': 5,
                'Pbb': 4,
                'ArkVid': 5, // slow
                'Nova': 5,
                'vZoo': 4, // Pause control broken
                'yUp': 0,
                'Dood': 0,
                'AuEn2': -1,
                'oLoad': -1,
                'vDrive': -1,

                // Not working with uBlock origin anymore
                'Baka': -1,
            },

            score: function(src_name) {
                return (typeof PreferredSourcePicker.scores[src_name] == 'undefined') ? 0 : PreferredSourcePicker.scores[src_name];
            },

            run: function() {
                var picked_src;
                var picked_index;
                var picked_url;
                var looking_at;
                var not_already_picked = !(/(\?src=|#selected)/.test(location.href));
                var links = document.querySelectorAll('#sources a');
                var link_src_names = [];
                for (var i = 0, l = links.length; i < l; i++) {
                    links[i].href += '#selected';
                    looking_at = link_src_names[i] = links[i].innerText || links[i].innerHTML;
                    links[i].innerText = looking_at + "\n" + PreferredSourcePicker.score(looking_at);
                    if (not_already_picked) {
                        if (!picked_src || (PreferredSourcePicker.score(looking_at) > PreferredSourcePicker.score(picked_src))) {
                            picked_src = looking_at;
                            picked_index = i;
                            picked_url = links[i].href;
                        }
                    }
                }
                if (links && links.length) {
                    var last_link = links[links.length - 1];
                    var reselect = document.createElement('a');
                    reselect.text = "Reselect\n-";
                    reselect.href = last_link.href.replace(/\?.*$/, '');
                    last_link.parentNode.insertBefore(reselect, last_link.parentNode.lastChild);
                    if (not_already_picked && picked_index !== 0) {
                        window.location.replace(picked_url);
                    }
                }
            }
        };

        PreferredSourcePicker.run();
        var ad_selector = '.sa, .ads, #tago, #tagoan, #hloading, #ifr_adid, #aad.ad, .jhasvdjhas a, .jhasvdjhas, .vjs-center-ad, .vjs-top-ad';

        var addStyle = typeof addStyle != 'undefined' ? addStyle : function(css) {
            if (typeof GM_addStyle !== 'undefined') {
                GM_addStyle(css);
                return;
            }

            var heads = document.getElementsByTagName('head');
            var root = heads ? heads[0] : document.body;
            var style = document.createElement('style');
            try {
                style.innerHTML = css;
            } catch (x) {
                style.innerText = css;
            }
            style.type = 'text/css';
            root.appendChild(style);
        };

        addStyle(
            '    body {\n' +
            '      margin: 0px;\n' +
            '      font-size: 13px;\n' +
            '    }\n' +
            '    .kwarta, .la {\n' +
            '      background-color: rgb(63, 63, 63);\n' +
            '    }\n' +
            '    #topnav, #wrap {\n' +
            '      width: auto;\n' +
            '    }\n' +
            '    a:link {\n' +
            '      text-decoration: underline;\n' +
            '    }\n' +
            '    a:visited {\n' +
            '      color: yellow;\n' +
            '      text-decoration: underline;\n' +
            '    }\n' +
            '    h3, h2, h1 {\n' +
            '      color: white;\n' +
            '      text-shadow: none !important;\n' +
            '    }\n' +
            '    #nextprevlist div span {\n' +
            '      text-shadow: none;\n' +
            '    }\n' +
            '    /* Ads and chat boxes */\n' +
            '    #chatango, .sideright, #leftside {\n' +
            '      display: none;\n' +
            '    }\n' +
            '    a:hover {\n' +
            '      color: black;\n' +
            '      text-decoration: none;\n' +
            '      background-color: white;\n' +
            '    }\n' +
            '');

        function blockAds(interval) {
            if (!doVisualUpdates) return;
            interval = interval || 1000;
            try {
                $(ad_selector).hide().attr('z-index', 0).remove();
            } catch (e) {
                console.log('blockAds', e);
                if (/cross-origin/.test(e.message)) {
                    console.log('blockAds', "stopping");
                    return;
                }
            }
            var stop = 0;
            try {
                $('iframe').each(function() {
                    try {
                        $(ad_selector, this.contentWindow.document).hide().attr('z-index', 0).remove();
                    } catch (e) {
                        console.log('blockAds', e);
                        if (/cross-origin/.test(e.message)) {
                            console.log('blockAds', "stopping");
                            stop = 1;
                            return;
                        }
                    }
                });
                if (stop) return;
            } catch (e) {
                console.log('blockAds', e);
            }
            if (interval > 10000) {
                interval = 10000;
            } else {
                interval += 100;
            }
            setTimeout(blockAds, interval, interval);
        }
        setTimeout(function() {
            $('span.trending a, span.latest a, div#nextprevlist a, div#eplist a, a.ser, a.ep').attr('target', '_blank');
            blockAds(0);
        }, 2000);
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log('anilinkz.user.js ended');

// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
