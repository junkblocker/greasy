// ==UserScript==
// @id             Duckduckgo
// @name           Duckduckgo search enhancements
// @version        7
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Add other search engines to Duckduckgo Interface and does fallback to github on no search results
// @icon           https://duckduckgo.com/favicon.ico
// @include        https://duckduckgo.tld/*
// @include        https://html.duckduckgo.tld/*
// @grant          GM_addStyle
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

// Default fallback: duckduckgo -> bing -> algolia -> github -> google | ZipRecruiterWiki
console.log('duckduckgo.user.js starting');
try {
    (function() {
        var doVisualUpdates = true;
        var qf;

        var hidden;
        if (typeof window.document.hidden !== 'undefined') {
            hidden = 'hidden';
        } else if (typeof window.document.msHidden !== 'undefined') {
            hidden = 'msHidden';
        } else if (typeof window.document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
        }

        if (hidden) {
            doVisualUpdates = !window.document[hidden];

            document.addEventListener('visibilitychange', function() {
                doVisualUpdates = !window.document[hidden];
                if (doVisualUpdates) {
                    killAds(0);
                }
            });
        }

        function killAds(loopy) {
            if (!doVisualUpdates) return;
            loopy = loopy || 0;
            loopy++;
            try {
                // multiple divs with this same id can occur due to infinite scroll
                var ads = document.querySelectorAll('.web-result-sponsored');
                if (!ads) {
                    setTimeout(killAds, 2000, loopy + 1);
                    return;
                }
                for (var i = 0; i < ads.length; i++) {
                    ads[i].style.display = 'none';
                }
                if (loopy < 15) setTimeout(killAds, 2000, loopy);
            } catch (ex) {
                console.log(ex);
            }
        }

        var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
            var f = scope ? function() {
                eventHandler.apply(scope, arguments);
            } : eventHandler;
            if (target.addEventListener) {
                target.addEventListener(eventName, f, true);
            } else if (target.attachEvent) {
                target.attachEvent('on' + eventName, f);
            }
            return f;
        };

        // *************************
        // addStyle
        // *************************
        var addStyle = typeof addStyle != 'undefined' ? addStyle : function(css) {
            if (typeof GM_addStyle !== 'undefined') {
                GM_addStyle(css);
                return;
            }

            var heads = document.getElementsByTagName('head');
            var root = heads ? heads[0] : document.body;
            var style = document.createElement('style');
            try {
                style.textContent = css;
            } catch (x) {
                try {
                    style.innerHTML = css;
                } catch (y) {
                    style.innerText = css;
                }
            }
            style.type = 'text/css';
            root.appendChild(style);
        };

        function query_s() {
            try {
                return qf.value;
            } catch (e) {
                return '';
            }
        }

        function duck_s() {
            return 'https://duckduckgo.com/html/?q=' + query_s() + '&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        }

        function google_s(kind) {
            if (kind && kind === 'pics') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=isch' + '&&tbs=li:1';
            } else if (kind && kind === 'video') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=vid' + '&&tbs=li:1';
            } else {
                return 'https://www.google.com/search?q=' + query_s();
            }
        }

        //function ixquick(what) {
        //return 'https://eu.ixquick.com/do/search?a=1&prfh=lang_homepageEEEs%2Fair%2Feng%2FN1Nconnect_to_serverEEEeuN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Nsearch_engine_sourcesEEE1N1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Npower_refinementEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N&cat=web&language=english&query=' + query_s() + '&pl=chrome';
        //}

        function spage_s() {
            return 'https://startpage.com/do/search/?query=' + query_s() + '&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        }

        function github_s() {
            return 'https://github.com/search?q=' + query_s();
        }

        function algolia_s() {
            return 'https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=' + query_s() + '&sort=byPopularity&type=all';
        }

        function bing_s() {
            return 'https://www.bing.com/search?q=' + query_s() + '&pc=MOZIo';
        }

        function makeButton(label, query_s_func) {
            var button_id = 'my_' + label;
            if (document.getElementById(button_id)) return;
            var button = document.createElement('button');
            button.id = 'my_' + label;
            button.textContent = label;
            addEventHandler(button, 'click', function() {
                window.location.replace(query_s_func());
                return false;
            });
            return button;
        }

        function work() {
            addStyle('#duckbar, #links_wrapper, .results--main, .cw {max-width: 99999px !important;}');
            var query_s = document.querySelector("#search_form_input").value;

            var results = document.getElementsByClassName('no-results')[0];
            if (results && /No\s+results/.test(results.innerHTML)) {
                var did_you_mean = document.getElementById('did_you_mean');
                if (!did_you_mean) window.location.replace(bing_s(query_s));
            }
            var before = document.getElementById('duckbar_dropdowns');
            if (before) {
                var pn = before.parentNode;
                pn.insertBefore(makeButton('Algolia', algolia_s), before);
                pn.insertBefore(makeButton('Bing', bing_s), before);
                pn.insertBefore(makeButton('Github', github_s), before);
                pn.insertBefore(makeButton('Google', google_s), before);
                addStyle('button {padding: 3px; margin: 3px;}');
                return;
            }
        }
        setTimeout(killAds, 500);
        setTimeout(work, 0); // This helps li.zcm__item detection
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("duckduckgo.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
