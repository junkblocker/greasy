// ==UserScript==
// @id             open_links_in_a_new_tab
// @name           Open links in a new tab
// @version        3
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    opens links on specified pages in new tabs
// @icon           data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAA4WlDQ1BzUkdCAAAYlWNgYDzNAARMDgwMuXklRUHuTgoRkVEKDEggMbm4gAE3YGRg+HYNRDIwXNYNLGHlx6MWG+AsAloIpD8AsUg6mM3IAmInQdgSIHZ5SUEJkK0DYicXFIHYQBcz8BSFBDkD2T5AtkI6EjsJiZ2SWpwMZOcA2fEIv+XPZ2Cw+MLAwDwRIZY0jYFhezsDg8QdhJjKQgYG/lYGhm2XEWKf/cH+ZRQ7VJJaUQIS8dN3ZChILEoESzODAjQtjYHh03IGBt5IBgbhCwwMXNEQd4ABazEwoEkMJ0IAAHLYNoSjH0ezAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAH9ElEQVRYhaWXS4wcRxnHf1XV857ZmZ2d8c4+7H14bONNYhvH8cZKDE6UOAmOhEhkBAIRDsgHhJQLEnBAQQghhMQJDsABcYAcEgkOJDwilATFSoQck1iOjdfx2mvH3l3v07szO9PT3VUfh1k7Edq110pJ3aWuavX3q//3qGrFBtpzzxw5ttxo/KTRCorWWi0izok1TkCcQ8QhAog4BSel6b5y/PTpixv5trrd5OHDuzLaZn/trPuac06JCCKCE1k13H5GoH0TRCBybt73w6feP3P+xJ0A9O3mwrp5KfD9r4dBS9kwxEYhURThwggbRVhrcdbhnEWJ44HhCnu3dmOQrlTce2VXtdp/JwCz3sTBfbueV4rvbO7MKmstfhAiTnDOtRVYVQERYgqOPnwPD44MsL2/zFJ9hanFesZ4an8q1/niwsKCvSuA4eHhvBb7p/7OTPqrj+xmqKeTi9fm8MOIKHIoQBCcEzTC/movB+4dQmuNUorhni4uTc1xo9HaYpDG9OzC8XVlXmvQBv7R6bmF4udHBkglYvQVO3h8z1a29RZJeopkXIMIBuGRe7fwyN4qSqnVgBLinmZ05xZEBIv0rGd8XQC/1fpyNuGx4vvtIAN2DlY4sn8H1Z4iuwe60UqxdVMnD++ukkol2prIx9dKw0cEjKLjbgGUdW6k2YqYmJy/5WcF5DMpjhwY4cDIFqq9BR4c2Uzc81DtBFjNiHZW5NMJFEImlTx89Oj6sbYWgIjIWT+MuDS1QBhENwdBhI50gq5CjmcP7mL7YGU1kd2qUnKr7yvlqRSz2Mj2XL+yc/huABB4K7KOZhgyu1j7P7p2n00l0VqDgBMIw5BWq50pIpBNJ9nW24WAbCmU6ncFoBRnYsaggHQqjnWWVhDitwLCMGoXodXLOku93sSJcGVmkdPj15hbXCaMLP39PZRLxflqJfujH3zjyd+vZctba9CY+ClDKOV8WnXkUizVmyQTcRIxj6YfsFwLyBdyOOc48d/LXPhojumlFWxkGeot8e6lGVqiuLESsq3SWSoXM8f+efL8uTUXu54yfeWu6f1be8rdpRwnxq7iB4543LCpkMEPLPPLdRZrPjU/xK36RRmNiXkkUwly2SxGaY7uHWCuXqfSVXjjhd/99dENKfDNw6N/f+vs5czrZy5hrbSr/GpwjV39mFlpTSzmEbXC9rMI4qDpBFB0pNP0lDqYml9iz7a+PWuudK3B0+OTk0EQpsMowolFxLG643Bz0wHBGEO50o1aZWqXZ0sURjTqTZKeJptNoo3CiVvcMIBWxBKepu0h9QnjHzelFJlMklwuQyweb0OodolGHC6ydHekaQURg91FVvzgz2vZWtMFYeRmMokYSnGrEn7CNEopNhdzvHDsCFfna7wes1y4Ms18rUnUrlsoBZVChhtLddKpOB8tLL6yYYCG77/YkU097xmjwsjeWrFWilQ8RqUzzdCmTkr5LN3FDvZW+3DOUW80mZpbYmp+meuLdYa6i2jl2NJbuvbG8Tff2TDA+en5E3uH+17NJuJP34h8PKP53H1DPHVghGuzi7x1apx4zPtECgkijnw2Qz6bYcdADyLCmfFrFDLJqzMLS0/88m8XWhsGANAi392UTx/qyqWyD40M8Mzj+whCyxv/GWNTPsv+e4faPhdBnLC03KCYz4JuY1lxdOXSZHKp9577/h/PrmtnvYl3L02O1RrhswnPayegc5w8N8Hc0grLKwGzi7X2acg6ZuZvtCvkKow4R7MZ0FXIoUStuxHdVgGAC1Mzr+3btnn+7MT10sj4FB+MT+IctAh5++wEoKmUOphdWEYbw9WZJXZt68MY3T49ibvd5+8MAGCUfr8ZhI+9/OYpUI6aH3JtoU4YOSZm6+RzaUpdnXTkcox/eJqDV6/zpUf3YoxGa42z9sqnAoicvB339GN+EGKM4qPZGsV8hj33bOXb33qOmFJEQYugWWNmYoyYdogTYkZjrePy9cV/fCqAmh+8Wswmfmi0UtYJ1UqBTCrOF/cO0hh/FwSsCFFkKXckgXZgRpHj4tQ8r/17bM3ov9lu+19w8537tw28EzNqFNr1wBhNImHIJuJEztFdyDK6c4D+SvFW4Vpe8Vmq+cQ7S43ywPbLfiuctZGdAZlu+M1z42MX3/npb//w3kYA2NFXORqPm5fScQ+tVfsgYhR9xRwHRgbY2l9un4gB6xz1FZ9UMoHWmkDHqewcpVZbIZvLErQCpq/PsLm/h0ajObkhgM/09W03CcZwQjoeQ5u2CofuGyTmGdKpOMVshlIhS7MVkoh7xOMxAKwoXjs1iR8ZtNb09pQ5dHAUrTXpdGZDLuDQ4GDyRszWWqH10jEDSuFHEeJgbmmFwDo6M0n2DFd4Yt8OesoFUAqlQUQxNnmDD66uoJShOtzvmn6gPc9w4IHP3jkIf/Oz7+WrW6sv//wXv/ImpmfwjIcVx9RCHT+IUEpjtKLWCmg0Q8qdOZy1tKyj3gjoKmSpVgpYZYjne9mz+x6dTqW4+R95W4Bjx47FBgcG//Lh+YmDrVZE3BisCEZrhro7mVyoEUSOrmyKJ++v8tB9w4BirtbkX2cu01fuIlnsJttZltEd3SqZTAMKJ0Kr5TM7t3h7gNFq+cfHj588eH78MkFkSXoe1jmMNsQ8w1ClSEc6wZEHdtBTyqOUlkXfuSmbNl848jT9fX0Y48FqtomIhFGozp2/yNiHFyl25vkfbhHvPFQ2rOsAAAAASUVORK5CYII=
// @include        *://*swish.cgi*
// @include        *://chneukirchen.org/*
// @include        *://*trivium*
// @include        *://encrypted.google.tld*
// @include        *://www.dragonflydigest.tld*
// @include        *://gist.github.tld*
// @inclue         *;//www.shiningsilence.tld*
// @include        *://*.livejournal.tld*
// @include        *://*newshelton.tld*
// @include        *://*popurls.tld*
// @include        *://*reddit.tld*
// @include        *://*vim.org*
// @include        *://*vuxml.org*
// @include        *://*wallhaven.cc*
// @include        *://*wallhaven.cc*
// @include        *://*wikimedia.org*
// @include        *://*wikipedia.tld*
// @run-at         document-end
// @grant          none
// ==/UserScript==

try {
    console.log("open_links_in_a_new_tab.user.js starting");
} catch (safe_wrap_top) {};
try {
    (function() {
        function open_in_new_tab_work(doc) {
            try {
                doc = doc || document;
                if (!doc.querySelectorAll || typeof doc.querySelectorAll == 'undefined') return;
                var links = doc.querySelectorAll('a');
                var link;

                // XXX: Does Javascript/DOM have any methods to call to do this without this
                // hack?
                var base = document.baseURI;

                for (var i = 0, l = links.length; i < l; i++) {
                    link = links[i];
                    try {
                        //if (!(/^[Aa]$/).test(link.nodeName)) {
                        //console.log("Skipping non <a> link", link.href);
                        //} else
                        if (link.onClick) { // for example on reddit comment thread collapse/expand [+]/[-] links
                            // console.log("Skipping link with onClick", link.href);
                            // } else if (!link.href) {
                            // console.log("Skipping link with no href");
                        } else if ((/^javascript/).test(link.href)) {
                            // console.log("Skipping javascript link", link.href);
                        } else if (base == link.origin + link.pathname) {
                            // (Internal) link to the same page
                            // console.log("Skipping internal link", link.href);
                        } else {
                            link.target = "_blank";
                        }
                    } catch (ex) {
                        console.log("Error:", ex);
                    }
                }
            } catch (e) {
                console.log("Error", e);
            }
        }
        var mo = window.MutationObserver || window.MozMutationObserver || window.WebKitMutationObserver;
        if (typeof mo !== "undefined") {
            var observer = new mo(function(mutations) {
                mutations.forEach(function(mutation) {
                    if (mutation.addedNodes !== null) {
                        for (var i = 0; i < mutation.addedNodes.length; i++) {
                            open_in_new_tab_work(mutation.addedNodes[i]);
                        }
                    }
                });
            });
            observer.observe(document, {
                childList: true,
                subtree: true,
            });
        } else {
            function open_in_new_tab_boot(e) {
                open_in_new_tab_work(e.target);
            }
            window.addEventListener("DOMNodeInserted", open_in_new_tab_boot, false);
            window.addEventListener("AutoPatchWork.DOMNodeInserted", open_in_new_tab_boot, false);
            window.addEventListener("AutoPagerize_DOMNodeInserted", open_in_new_tab_boot, false);
            window.addEventListener("AutoPagerAfterInsert", open_in_new_tab_boot, false);
        }
        open_in_new_tab_work(document);
    })();
} catch (safe_wrap_bottom_1) {
    try {
        console.log(safe_wrap_bottom_1);
    } catch (safe_wrap_bottom_2) {}
};
try {
    console.log("open_links_in_a_new_tab.user.js ended");
} catch (safe_wrap_bottom_3) {};
