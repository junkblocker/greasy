// ==UserScript==
// @id             Algolia
// @name           Algolia search enhancements
// @version        7
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Add other search engines to Algolia Interface and does fallback to github on no search results
// @icon           https://hn.algolia.com/favicon.ico
// @include        https://hn.algolia.com/*
// @grant          GM_addStyle
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

// Default fallback: algolia -> bing -> github -> duckduckgo -> startpage -> google | ZipRecruiterWiki
console.log("algolia.user.js starting");
try {
    (function() {
        const bings = 'https://www.bing.com/search?q=%s&pc=MOZIo';
        const ducks = 'https://duckduckgo.com/html/?q=%s&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        const googles = 'https://www.google.com/search?q=%s';
        const githubs = 'https://github.com/search?q=%s';
        //            const algolia = 'https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=%s&sort=byPopularity&type=all';
        const spages = 'https://startpage.com/do/search/?query=%s&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        const zrw = 'https://wiki.zr.org/_search?patterns=%s&search=Search';

        function addStyle(content) {
            var style = document.createElement("style");
            style.type = "text/css";
            style.appendChild(document.createTextNode(content));
            var head = document.getElementsByTagName("head");
            head && head[0] ? head = head[0] : head = document.body;
            head.appendChild(style);
        }
        addStyle(
            'body, .Story_comment, .Story_link, .Story, .Story_separator, .Story_meta, .Story_data, .Story_container {' +
            '    font-family: Menlo, Monaco, "Bitstream Vera Sans Mono", "Lucida Console", Consolas, Terminal, "Courier New", Courier, monospace !important;' +
            '    font-size: 10pt !important;' +
            '}' +
            '.Story_meta {' +
            '    font-size: 8pt !important;' +
            '}' +
            'a[rel=nofollow], a span {' +
            '    color: #ff5f00 !important;' +
            '}' +
            '.SearchResults, .SearchFilters, .Settings, .Pagination {' +
            '    background: #fff !important;' +
            '}'
        );
        var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
            var f = scope ? function() {
                eventHandler.apply(scope, arguments);
            } : eventHandler;
            if (target.addEventListener) {
                target.addEventListener(eventName, f, true);
            } else if (target.attachEvent) {
                target.attachEvent('on' + eventName, f);
            }
            return f;
        };

        function makeButton(label, querys) {
            var button = document.createElement('button');
            button.id = 'my_' + label;
            button.className = 'btn';
            button.textContent = label;
            addEventHandler(button, "click", function() {
                var query_s;
                try {
                    query_s = document.getElementsByClassName("SearchInput")[0].value;
                } catch (e) {
                    console.log("Could not figure the query");
                    query_s = window.location.href.match('query=([^&]+)');
                }
                if (query_s) {
                    console.log("Query", query_s);
                    window.location.replace(querys.replace(/%s/g, query_s));
                } else {
                    alert("No query string");
                }
                return false;
            });
            return button;
        }

        function work() {
            try {
                var before = document.querySelectorAll('section.SearchResults');
                if (before) {
                    before = before[0];
                    var pn = before.parentNode;

                    pn.insertBefore(makeButton('Bing', bings), before);
                    pn.insertBefore(makeButton('Github', githubs), before);
                    pn.insertBefore(makeButton('DuckDuckGo', ducks), before);
                    pn.insertBefore(makeButton('Startpage', spages), before);
                    pn.insertBefore(makeButton('Google', googles), before);
                    pn.insertBefore(makeButton('ZRW', zrw), before);
                    return;
                }
            } catch (e) {
                console.log(e);
            }
            setTimeout(work, 1000);
        }
        setTimeout(work, 1000); // This helps li.zcm__item detection
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("algolia.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
