// ==UserScript==
// @name        Click search again for search in topics, and posts
// @namespace   Violentmonkey Scripts
// @match       *://forum.*.it*
// @grant       none
// @version     2
// @author      -
// @description Whatever
// @run-at      document-idle
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log("search_forums.user.js starting");

// titleonly => all => all + terms = any
function repeatLog() {
    try {
        var title = document.title.replace(/[ _-]*[a-zA-Z]+.it[ _-]*/, '');
        title = title.replace(/\[m[a-z]{3}\]/i, '[M]');
        title = title.replace(/\[z[a-z]{4}\]/i, '[Z]');
        document.title = title.replace(/\[g(driv|oogl)e\]/i, '[G]');
        if (document.search_fixed) return;
        if (/\/\/forum\.[a-z]+\.it\/search\..*\bkeywords=/.test(window.location.href)) {
            var results = document.querySelector('h2.searchresults-title');
            if (results && /Search found/.test(results.innerHTML)) {
                console.log('sf', document.forms.search.sf);
                console.log('terms', document.forms.search.terms);
                if (document.forms.search.sf.value == 'titleonly' || /\bsf=titleonly\b/.test(window.location.href)) {
                    console.log('titleonly->all');
                    document.forms.search.sf.value = 'all';
                    document.search_fixed = 1;
                } else if (!document.forms.search.terms && /\bsf=all\b/.test(window.location.href)) {
                    console.log('terms->any');
                    var terms = document.createElement('input');
                    terms.id = 'terms';
                    terms.name = 'terms';
                    terms.hidden = true;
                    document.forms.search.appendChild(terms);
                    document.forms.search.terms.value = 'any';
                    document.search_fixed = 1;
                }
                console.log("Success");
            }
        }
    } catch (e) {
        console.log(e);
    }
}
repeatLog();
console.log("search_forums.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
