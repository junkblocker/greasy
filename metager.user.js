// ==UserScript==
// @id             MetaGer
// @name           MetaGer search enhancements
// @version        5
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Add other search engines to MetaGer Interface and does fallback to duckduckgo on no search results
// @include        https://metager.tld/*
// @icon           https://metager.org/favicon.ico
// @grant          none
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

// Default fallback: MetaGer -> duckduckgo -> bing -> algolia -> github -> google | ZipRecruiterWiki
console.log("metager.user.js starting");
try {
    (function() {
        var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
            var f = scope ? function() {
                eventHandler.apply(scope, arguments);
            } : eventHandler;
            if (target.addEventListener) {
                target.addEventListener(eventName, f, true);
            } else if (target.attachEvent) {
                target.attachEvent('on' + eventName, f);
            }
            return f;
        };

        function query_s() {
            try {
                return encodeURI(document.forms.searchForm.eingabe.value);
            } catch (e) {
                return '';
            }
        }

        function duck_s() {
            return 'https://duckduckgo.com/html/?q=' + query_s() + '&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        }

        function google_s(kind) {
            if (kind && kind === 'pics') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=isch' + '&&tbs=li:1';
            } else if (kind && kind === 'video') {
                return 'https://www.google.com/search?q=' + query_s() + '&tbm=vid' + '&&tbs=li:1';
            } else {
                return 'https://www.google.com/search?q=' + query_s();
            }
        }

        function spage_s() {
            return 'https://startpage.com/do/search/?query=' + query_s() + '&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        }

        function github_s() {
            return 'https://github.com/search?q=' + query_s();
        }

        function github_kind_s(kind) {
            kind = kind || 'Repositories';
            if (isRepoSearch()) {
                return 'https://github.com/' + query_s + '/search?langOverride=&q=' + what + '&type=' + kind;
            } else {
                return 'https://github.com/search?q=' + query_s + '&type=' + kind;
            }
        }

        function zrw_s() {
            return 'https://wiki.zr.org/_search?patterns=' + query_s() + '&search=Search';
        }

        function algolia_s() {
            return 'https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=' + query_s() + '&sort=byPopularity&type=all';
        }

        function bing_s() {
            return 'https://www.bing.com/search?q=' + query_s() + '&pc=MOZIo';
        }

        function makeButton(label, query_s_func) {
            var button_id = 'my_' + label;
            var old = document.getElementById(button_id);
            if (old) return old;
            var linkdiv = document.createElement('div');
            linkdiv.id = label;
            var alink = document.createElement('a');
            alink.href = query_s_func();
            alink.innerText = label;
            alink.target = '_top';
            linkdiv.appendChild(alink);
            return linkdiv;
        }

        function work() {
            try {
                var searchbuttons = document.getElementById('foki-box');
                if (searchbuttons) {
                    searchbuttons.appendChild(makeButton('DuckDuckGo', duck_s));
                    searchbuttons.appendChild(makeButton('Github', github_s));
                    searchbuttons.appendChild(makeButton('Algolia', algolia_s));
                    searchbuttons.appendChild(makeButton('Bing', bing_s));
                    searchbuttons.appendChild(makeButton('Google', google_s));
                    searchbuttons.appendChild(makeButton('ZRW', zrw_s));

                    return;
                } else {
                    console.log('foki-box not found');
                }
            } catch (e) {
                console.log(e);
            }
            setTimeout(work, 2000);
        }
        setTimeout(work, 100);
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("metager.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 nowrap :
