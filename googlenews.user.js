// ==UserScript==
// @id             Improve Google News
// @name           Improve Google News
// @namespace      junkblocker
// @description    Improve Google News
// @version        4
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @match          https://news.google.com/*
// @grant          none
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
console.log("googlenews.user.js starting");

(function() {
    'use strict';

    function hideCrap() {
        // Hide certain news sections
        try {
            document.querySelectorAll('a.wmzpFf').forEach(function(x) {
                var div = x.parentNode.parentNode.parentNode.parentNode;
                console.log(x.innerHTML, "div classname", div.className);

                if (/Sports|Entertainment/.test(x.innerHTML)) {
                    var nextSib = div.nextSibling;
                    while (nextSib && !hasClass(nextSib, 'dSva6b')) {
                        nextSib.style.display = 'none';
                        nextSib = nextSib.nextSibling;
                    }
                    div.style.display = 'none';
                }
            });
        } catch (e) {
            console.log(e);
        }
        // Hide certain news publications
        try {

            document.querySelectorAll('a.wEwyrc').forEach(function(x) {
                if (/^(Fox News|The Washington Post)$/.test(x.innerText)) {
                    x.parentNode.parentNode.parentNode.style.color = '#dddddd';
                }
            });
        } catch (e) {
            console.log(e);
        }
        setTimeout(hideCrap, 5000);
    }

    var hasClass = typeof hasClass !== 'undefined' ? hasClass : function(elem, cls) {
        if ((typeof(elem) == 'undefined') || (elem === null)) {
            console.log("Invalid hasClass elem argument");
            return false;
        } else if ((typeof(cls) == 'undefined') || (cls === null)) {
            console.log("Invalid hasClass cls argument", cls);
            return false;
        }
        return elem.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    };

    try {
        setTimeout(hideCrap, 500);
    } catch (safe_wrap_bottom_1) {
        console.log(safe_wrap_bottom_1);
    }
})();
console.log("googlenews.user.js ended");
// vim: set et fdm=indent fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
