// ==UserScript==
// @id             zoom_us
// @name           zoom_us
// @namespace      junkblocker
// @description    zoom_us site enhancements
// @version        2
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @match          *://zoom.tld/*
// @match          *://*.zoom.tld/*
// @grant          none
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log("zoom_us.user.js starting");
try {
    (function() {
        function addStyle(content) {
            var style = document.createElement("style");
            style.type = "text/css";
            style.appendChild(document.createTextNode(content));
            var head = document.getElementsByTagName("head");
            head && head[0] ? head = head[0] : head = document.body;
            head.appendChild(style);
        }
        // unhide the webclient block
        addStyle("div.webclient.hideme { display: block }");
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("zoom_us.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
