// ==UserScript==
// @name        Dismiss modal on bytebytego.com
// @namespace   junkblocker
// @match       https://blog.bytebytego.com/*
// @grant       none
// @version     1
// @author      junkblocker@yahoo.com
// @description 7/5/2023, 11:06:12 AM
// ==/UserScript==

(function() {
    'use strict';

    var element = document.querySelector('button.close-welcome-modal');
    if (element) {
        element.click();
    } else {
        console.log('modal not found');
    }
})();
