// ==UserScript==
// @id             Redirect zoom app links to browser
// @name           Redirect zoom app links to browser
// @namespace      junkblocker
// @description    Redirect zoom app links to browser
// @version        2
// @author         Manpreet Singh <manpreets@ziprecruiter.com>
// @match          *://zoom.tld/*
// @match          *://*.zoom.tld/*
// @grant          none
// @run-at         document-start
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log("zoom_web_client.user.js starting");
try {
    (function() {
        if (/(^https?:\/\/([^/]*\.)?zoom\..*)\/j\/(.*)$/.test(window.location.href)) {
            window.location.replace(RegExp.$1 + '/wc/join/' + RegExp.$3);
        }
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("zoom_web_client.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
