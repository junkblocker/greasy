// ==UserScript==
// @id             Startpage
// @name           Startpage search enhancements
// @version        5
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Add other search engines to Startpage Interface and does fallback to duckduckgo on no search results
// @icon           https://www.startpage.com/sp/cdn/favicons/favicon-32x32--default.png
// @include        *://startpage.tld/*
// @include        *://*.startpage.tld/*
// @grant          none
// @run-at         document-end
// ==/UserScript==

// Default fallback: startpage -> duckduckgo -> github -> google | ZRW
try {
    console.log('startpage.user.js starting');
} catch (safe_wrap_top) {}
try {
    (function() {
        var debug = true;
        var interval = 500;
        var doVisualUpdates = true;

        var hidden;
        if (typeof window.document.hidden !== 'undefined') {
            hidden = 'hidden';
        } else if (typeof window.document.msHidden !== 'undefined') {
            hidden = 'msHidden';
        } else if (typeof window.document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
        }

        if (hidden) {
            doVisualUpdates = !window.document[hidden];

            document.addEventListener('visibilitychange', function() {
                doVisualUpdates = !window.document[hidden];
                if (doVisualUpdates) {
                    setTimeout(killAds, 100, 100);
                }
            });
        }

        function killAds(interval) {
            if (!doVisualUpdates) return;
            interval = interval || 100;
            try {
                // multiple divs with this same id can occur due to infinite scroll
                var ads = document.querySelectorAll('#adBlock');
                if (ads) {
                    for (var i = 0; i < ads.length; i++) {
                        ads[i].style.display = 'none';
                    }
                }
            } catch (ex) {
                console.log(ex);
            }
            try {
                var adsl = document.querySelectorAll('#spon_links');
                if (adsl) {
                    for (var j = 0; j < adsl.length; j++) {
                        adsl[j].style.display = 'none';
                    }
                }
            } catch (ex) {
                console.log(ex);
            }
            try {
                var adsr = document.querySelectorAll('#spon-results');
                if (adsr) {
                    for (var k = 0; k < adsr.length; k++) {
                        adsr[k].style.display = 'none';
                    }
                }
            } catch (ex) {
                console.log(ex);
            }
            interval = interval < 2000 ? interval + 500 : 2000;
            setTimeout(killAds, interval);
        }
        setTimeout(killAds, 100, 0);

        var addEventHandler = typeof addEventHandler !== 'undefined' ? addEventHandler : function(target, eventName, eventHandler, scope) {
            var f = scope ? function() {
                eventHandler.apply(scope, arguments);
            } : eventHandler;
            if (target.addEventListener) {
                target.addEventListener(eventName, f, true);
            } else if (target.attachEvent) {
                target.attachEvent('on' + eventName, f);
            }
            return f;
        };

        function duck(what) {
            return 'https://duckduckgo.com/html/?q=' + what + '&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        }

        function google(what, kind) {
            if (kind && kind === 'pics') {
                return 'https://www.google.com/search?q=' + what + '&tbm=isch' + '&&tbs=li:1';
            } else if (kind && kind === 'video') {
                return 'https://www.google.com/search?q=' + what + '&tbm=vid' + '&&tbs=li:1';
            } else {
                return 'https://www.google.com/search?q=' + what + '&&tbs=li:1';
            }
        }

        // function spage(what) {
        //     return 'https://startpage.com/do/search/?query=' + what + '&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        // }

        function github(what) {
            return 'https://github.com/search?q=' + what;
        }

        // function cec(what) {
        //     return 'http://wwwin-tools.cisco.com/onesearch/searchpage?queryFilter=' + what;
        // }

        function zrw(what) {
            return 'https://wiki.zr.org/_search?patterns=' + what + '&search=Search';
        }

        function makeButton(label, querys) {
            var button = document.createElement('button');
            button.id = 'my_' + label;
            button.textContent = label;
            button.alt = querys;
            button.title = querys;
            addEventHandler(button, 'click', function() {
                window.location.replace(querys);
                return false;
            });
            return button;
        }

        function makeOtherLink(label, url) {
            var a = document.createElement('a');
            a.className = 'addsettings';
            a.id = label;
            a.href = url;
            a.innerHTML = label;
            return a;
        }
        var each = typeof each !== 'undefined' ? each : function(c, fn) {
            if (c.forEach) {
                c.forEach(fn);
            } else if (typeof c.length === 'number') {
                Array.prototype.forEach.call(c, fn);
            } else {
                Object.keys(c).forEach(function(k) {
                    fn(c[k], k, c);
                });
            }
        };

        function work() {
            var query_s, search_kind, results;
            if (document.body.done) return;
            try {
                if (document.forms[0]) {
                    query_s = document.forms[0].query.value;
                    search_kind = document.forms[0].cat.value;
                } else if (document.forms['search']) {
                    query_s = document.forms['search'].query.value;
                    search_kind = document.forms['search'].cat.value;
                }
                if (query_s) {
                    if (debug) console.log('Query string', query_s);

                    each(['nc-nr-em__title', 'no-results__title'], function(k) {
                        var results = document.getElementsByClassName(k)[0];
                        if (results && /no_(pics|web|video|result)|no Web results|Sorry, there are no results for this search./.test(results.innerText)) {
                            window.location.replace(duck(query_s, search_kind));
                            return;
                        }
                    });
                    results = document.getElementsByClassName('mainline-results--default')[0];
                    if (!results) {
                        results = document.getElementsByClassName('mainline-results--air')[0];
                    }
                    if (results) {
                        var div = document.createElement('div');
                        div.setAttribute('align', 'center');

                        div.appendChild(makeButton('DuckDuckGo', duck(query_s, search_kind)));
                        div.appendChild(makeButton('Github', github(query_s, search_kind)));
                        div.appendChild(makeButton('Google', google(query_s, search_kind)));
                        // div.appendChild(makeButton('CEC', cec(query_s, search_kind)));
                        div.appendChild(makeButton('ZRW', zrw(query_s, search_kind)));

                        results.parentNode.insertBefore(div, results);
                        document.body.done = 1;
                    }
                } else {
                    var links = document.getElementById('search');
                    if (!links) {
                        links = document.querySelectorAll('article.column--main');
                        if (links) links = links[0];
                    } else {
                        links = links.parentNode;
                    }
                    if (links) {
                        links.insertBefore(document.createTextNode(' | '), links.firstChild);
                        links.insertBefore(makeOtherLink('Github', github('')), links.firstChild);
                        links.insertBefore(document.createTextNode(' | '), links.firstChild);
                        links.insertBefore(makeOtherLink('Google', google('')), links.firstChild);
                        links.insertBefore(document.createTextNode(' | '), links.firstChild);
                        links.insertBefore(makeOtherLink('Duckduckgo', duck('')), links.firstChild);
                        links.insertBefore(document.createTextNode(' | '), links.firstChild);
                        // links.insertBefore(makeOtherLink('CEC', cec('')), links.firstChild);
                        links.insertBefore(makeOtherLink('ZRW', zrw('')), links.firstChild);
                        document.body.done = 1;
                    }

                }
            } catch (exc) {
                console.log(exc);
            }
            setTimeout(work, 500);
        }
        setTimeout(work, 500);
    })();
} catch (safe_wrap_bottom_1) {
    try {
        console.log(safe_wrap_bottom_1);
    } catch (safe_wrap_bottom_2) {}
}
try {
    console.log('startpage.user.js ended');
} catch (safe_wrap_bottom_3) {}
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
