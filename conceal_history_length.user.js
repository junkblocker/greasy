// ==UserScript==
// @name        Conceal history.length
// @description Intercepts read access to history.length property
// @namespace   localhost
// @include     *
// @run-at      document-start
// @version     2
// @grant       none
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log('conceal_history_length.user.js starting');
try {
    (function() {
        const _history = {
            length: history.length
        };
        Object.defineProperty(history, 'length', {
            get: function() {
                if (_history.length > 2) {
                    return 2;
                } else {
                    return _history.length;
                }
            }
        });
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("conceal_history_length.user.js ended");
