// ==UserScript==
// @id          fedoramagazine
// @name        fedoramagazine.org
// @namespace   junkblocker
// @author      Manpreet Singh <junkblocker@yahoo.com>
// @description fedoramagazine.org
// @match       https://fedoramagazine.org/*
// @grant       GM_addStyle
// @version     1
// ==/UserScript==
/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

'use strict';

console.log("fedoramagazine.user.js starting");
try {
    (function() {
        GM_addStyle(".section-inner {width: 100% !important ; }");
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("fedoramagazine.user.js ended");
