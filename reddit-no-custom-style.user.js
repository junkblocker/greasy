// ==UserScript==
// @name          Reddit No Custom CSS
// @description   Disable custom subreddit styles
// @author        junkblocker
// @copyright     junkblocker
// @version       1
// @namespace     junkblocker
// @license       GPL: http://www.gnu.org/copyleft/gpl.html
// @include       http://reddit.com/r/*
// @include       https://reddit.com/r/*
// @include       http://*.reddit.com/r/*
// @include       https://*.reddit.com/r/*
// @grant         GM_deleteValue
// @grant         GM_getValue
// @grant         GM_setValue
// @run-at        document-start
// @inject-into   auto
// ==/UserScript==
// Force remove version of chocolateboy's script

console.log("reddit-no-custom-style.user.js starting");
const CUSTOM_CSS = 'link[ref^="applied_subreddit_"]';

const {
    style
} = document.documentElement;

style.display = 'none'; // 1) hide the page
document.addEventListener('DOMContentLoaded', function() {
    const customCss = document.querySelector(CUSTOM_CSS);
    if (customCss) {
        customCss.disabled = true; // 2) disable the stylesheet
    }
    style.removeProperty('display'); // 3) unhide the page
});
console.log("reddit-no-custom-style.user.js ending");
