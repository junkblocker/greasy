// ==UserScript==
// @id             emacswiki_dot_org_improvements
// @name           emacswiki.org Improvements
// @version        2
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    emacswiki.org Improvements
// @include        http://www.emacswiki.org/*
// @include        htts://www.emacswiki.org/*
// @grant          none
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

console.log("emacswiki.user.js starting");
try {
    (function() {
        document.title = document.title.replace(/^EmacsWiki: /, '');
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("emacswiki.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
