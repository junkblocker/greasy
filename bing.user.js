// ==UserScript==
// @id             Bing
// @name           Bing search enhancements
// @version        5
// @namespace      junkblocker
// @author         Manpreet Singh <junkblocker@yahoo.com>
// @description    Add other search engines to Bing Interface and does fallback to github on no search results
// @icon           data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAEUUlEQVR4AWIYrmAUjIJRMApGwSgAEF8OQM4lURi9wdq2VVyjtLZt87dt27Zt27Ztj9I9VvjtvUleBr/eS0VVZ9zd59wexVYOe4Ipd3508g7mKuZG5mbmlgRxc/hMOdthNaL0xK9u1anr77NXrD2weP2WlIXrN6cuWLspZcmG0Ot4sXDd5nNyppwtDuJiJcKQdzK35ebmarfbDaUUtNYoLi5GSkoKEvGQs8Uh7GK3EuBgrmTubjFiCpKJOIRdHFYCnOGru5/e/BFJhR3CLk6zAXbmCuZa5iFjI8fbPyUr4CFxCTvZrQRcxzxibPRihcZwvv1zMgIeEZdoAq5nHrV64N7jZ9B02CQ88Usty7IyoFs+/ad8wKPiYjXgSiPg+g//sCRR+vHsPw1jcQNGwJVRBVz57i/nTclswAsVGic/oPyGfMXxD/iAb/23RqC6PWIfwFgKuPGjP82v/6wCqHI7UIuhoJbDhOQHVOgyEL0nz8E3zbriri8rnL/mnV9B/7UCNR4gwueR9ID7vqmERes3G4RiWvbEvRWa87TbgpoNNmTjFPD+76C3foo6gN79FU1GTMaizTuxaNturNxzCEdT07HqyClDMs4BxkZNB4Ea9AHV7gaq2hFUoQ1ffUvQv6Wo2AaBQCDCC/2nRUReHzoba4+dRn5BAQoLC7Hu2JnEBVhBxP1+f5AXBs3E1W1Gou6ijcjIzQv+JysUFRVhDcdcdr8eE2MQwJOjnpNB7UaZCjDkhQ6rd+K4zobH44H8Sy7y+YVF6LluF+7qMu789a2Hh6RHzgOt3wU6dy4GAQs2IsKcdaBJy0GjFoAGzgT14rCu40Htx/DhI8oE+Hy+IF6vNxIweschPCrDENn2o3jtBFC/6Sw8HzRlBWjeetCmPaCzZ0A6XYhNgGnmr8esFFVGXphzNgPPLNsKmrUaNHstf+2G89eK+JmQ+INa43tdgA7KF4OAlHOgo8dBOw6AVu8wFfLiuj2Yk6ax2pWF1zbuu/jXLt4c2jf1HBws/rzKRgPlxiAFgxgE8MZlyEgFHT4mExMBEbHG4k2yNjQUlYabtQufqjx00j5DOq4B5yPfr/uPgDbvBa3dCVq5DbRkC2gRiy7l16u2h4T3HAKdOh2Uvk8rfKDzUI+nPVAFDNmYBhhPaB5+Ly3T85LOxlM6M3jwTTwxx+WiGDtzK3/t47zuFV7/tcpHLe1GD+UXMVP0T3V7xEFcon1Gdm/d2Ssnnb95ICjSVnmF4A9bZ6Ybf6wn00cFLjtZM8jZ4hBNgJO5hrmF+V/yUlJSRsrq6taKqqp2Cioq9gBezAADYBgGgKbLtBSFjsJs///kGg5TmCDBAZZLrOA8UYe61Kludijs9H/AUuQqXWZMLn7n48yNa+Cu7JKsYUsmmQGNYX1yOtNxNdz583w2axcVri+8wxoBroJbbH10KdJ8vDPoCEJwprVUO6f1+MRuOyied4McjIJRMApGAQA+/oE+C3EHYgAAAABJRU5ErkJggg==
// @include        *://www.bing.tld/*
// @include        *://bing.tld/*
// @grant          GM_addStyle
// @run-at         document-end
// ==/UserScript==

/* eslint-extends 'eslint:recommended' */
/* eslint-env es6, browser, greasemonkey, jquery */
/* eslint prefer-const: 'error'*/
/* eslint-disable quotes */
/* eslint indent: ["error", 4, { "SwitchCase": 1 }] */

/* eslint-disable no-prototype-builtins */
'use strict';

// Default fallback: algolia -> bing -> github -> duckduckgo -> startpage -> google | ZipRecruiterWiki
console.log('bing.user.js starting');
try {
    (function() {
        var doVisualUpdates = true;

        var hidden;
        if (typeof window.document.hidden !== 'undefined') {
            hidden = 'hidden';
        } else if (typeof window.document.msHidden !== 'undefined') {
            hidden = 'msHidden';
        } else if (typeof window.document.webkitHidden !== 'undefined') {
            hidden = 'webkitHidden';
        }

        if (hidden) {
            doVisualUpdates = !window.document[hidden];

            document.addEventListener('visibilitychange', function() {
                doVisualUpdates = !window.document[hidden];
                if (doVisualUpdates) {
                    killAds(0);
                }
            });
        }

        function killAds(loopy) {
            if (!doVisualUpdates) return;
            loopy = loopy || 0;
            loopy++;
            try {
                // multiple divs with this same id can occur due to infinite scroll
                var ads = document.querySelectorAll('.web-result-sponsored');
                if (!ads) {
                    setTimeout(killAds, 2000, loopy + 1);
                    return;
                }
                for (var i = 0; i < ads.length; i++) {
                    ads[i].style.display = 'none';
                }
                if (loopy < 15) setTimeout(killAds, 2000, loopy);
            } catch (ex) {
                console.log(ex);
            }
        }

        function duck(what) {
            return 'https://duckduckgo.com/html/?q=' + what + '&k1=-1&k5=-1&kac=1&kaf=1&kaj=-1&kam=osm&kav=1&kc=-1&kd=-1&kf=-1&kg=p&kh=1&kj=w&kl=wt-wt&kn=1&ko=-2&kp=-1&kx=g&kz=1';
        }

        function google(what, kind) {
            if (kind && kind === 'pics') {
                return 'https://www.google.com/search?q=' + what + '&tbm=isch' + '&&tbs=li:1';
            } else if (kind && kind === 'video') {
                return 'https://www.google.com/search?q=' + what + '&tbm=vid' + '&&tbs=li:1';
            } else {
                return 'https://www.google.com/search?q=' + what;
            }
        }

        function spage(what) {
            return 'https://startpage.com/do/search/?query=' + what + '&cat=web&pl=ff&language=english&prfh=lang_homepageEEEs/white/eng/N1Nconnect_to_serverEEEusN1Nfont_sizeEEEmediumN1Nrecent_results_filterEEE1N1Nlanguage_uiEEEenglishN1Ndisable_open_in_new_windowEEE0N1NsslEEE1N1Ndisable_family_filterEEE1N1Nnum_of_resultsEEE100N1Ngeo_mapEEE1N1Npicture_privacyEEEonN1Ndisable_video_family_filterEEE1N1NsuggestionsEEE1N1N';
        }

        function github(what) {
            return 'https://github.com/search?q=' + what;
        }

        function zrw(what) {
            return 'https://wiki.zr.org/_search?patterns=' + what + '&search=Search';
        }

        function algolia(what) {
            return 'https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=' + what + '&sort=byPopularity&type=all';
        }

        // function bing(what) {
        //     return 'https://www.bing.com/search?q=' + what + '&pc=MOZIo';
        // }

        function makeLink(label, querys) {
            var li = document.createElement('li');
            li.style.display = 'inline-block';
            var a = document.createElement('a');
            a.className = 'btn';
            a.href = querys;
            a.innerHTML = label;
            li.appendChild(a);
            return li;
        }

        function work() {
            var query_s;
            try {
                query_s = document.querySelector('input.sb_form_q');
            } catch (e) {
                console.log("Could not figure the query");
            }
            if (!query_s) {
                query_s = window.location.href.match('q=([^&]+)')[1];
            }
            console.log('Query string', query_s);

            var results = document.querySelector('#b_results > li > h1');
            if (results && /There\s+are\s+no\s+results/.test(results.innerHTML)) {
                var did_you_mean = document.getElementById('did_you_mean');
                if (!did_you_mean) window.location.replace(github(query_s));
            }
            var appendto;
            appendto = document.querySelector('#b_header > nav > ul');
            if (appendto) {
                var xpn = appendto;
                xpn.appendChild(makeLink('algolia', algolia(query_s)));
                xpn.appendChild(makeLink('DuckDuckGo', duck(query_s)));
                xpn.appendChild(makeLink('Github', github(query_s)));
                xpn.appendChild(makeLink('Startpage', spage(query_s)));
                xpn.appendChild(makeLink('Google', google(query_s)));
                xpn.appendChild(makeLink('ZRW', zrw(query_s)));
                return;
            }
        }
        setTimeout(killAds, 500);
        setTimeout(work, 0); // This helps li.zcm__item detection
    })();
} catch (safe_wrap_bottom_1) {
    console.log(safe_wrap_bottom_1);
}
console.log("bing.user.js ended");
// vim: set et fdm=syntax fenc=utf-8 ff=unix ft=javascript sts=0 sw=4 ts=4 tw=79 nowrap :
